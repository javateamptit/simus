/****************************************************************************
** Project name:    US Blink Simulator
** File name:       lava.h
** Description:     N/A
** Specification:   LavaFlow Binary Order Entry (LFBOE) Specification 
**                  Version 1.1
**                  February 3, 2014
**
** Author:          Thien Nguyen-Tai
** First created:   May 13, 2014
** Last updated:    May 14, 2014
****************************************************************************/

#ifndef __LFBOE__
#define __LFBOE__

#include "global.h"
#include "soupbintcp.h"


// LFBOE protocol
#define LFBOE_PAYLOAD_OFFSET                         SOUPBINTCP_PAYLOAD_OFFSET
#define LFBOE_HEARDER_PACKET_LEN                     SOUPBINTCP_HEARDER_PACKET_LEN

#define LFBOE_LOGIN_REQUEST_PACKET_TYPE              SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE
#define LFBOE_CLIENT_HEARTBEAT_PACKET_TYPE           SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE
#define LFBOE_LOGOUT_REQUEST_PACKET_TYPE             SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE
#define LFBOE_UNSEQUENCED_DATA_PACKET_TYPE           SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE

#define LFBOE_HeartbeatThread                        SOUPBINTCP_HeartbeatThread
#define LFBOE_Handle_LogonPacket                     SOUPBINTCP_Handle_LogonPacket
#define LFBOE_Handle_LogoutPacket                    SOUPBINTCP_Handle_LogoutPacket

#define LFBOE_BuildAndSend_SequencedDataPacket       SOUPBINTCP_BuildAndSend_SequencedDataPacket

#define LFBOE_PROTOCOL_CODE                   0x0101

#define LFBOE_ENTER_ORDER_MSG_LEN             62
#define LFBOE_REPLACE_MSG_LEN                 48
#define LFBOE_CANCEL_MSG_LEN                  17
#define LFBOE_ACCEPTED_MSG_LEN                78
#define LFBOE_EXECUTED_MSG_LEN                51
#define LFBOE_REJECTED_MSG_LEN                27
#define LFBOE_CANCELED_MSG_LEN                27
#define LFBOE_CANCEL_REJECT_MSG_LEN           27
#define LFBOE_REPLACED_MSG_LEN                86

#define LFBOE_ENTER_ORDER_MSG_TYPE            'O'
#define LFBOE_REPLACE_MSG_TYPE                'U'
#define LFBOE_CANCEL_MSG_TYPE                 'X'
#define LFBOE_ACCEPTED_MSG_TYPE               'A'
#define LFBOE_EXECUTED_MSG_TYPE               'E'
#define LFBOE_REJECTED_MSG_TYPE               'J'
#define LFBOE_CANCELED_MSG_TYPE               'C'
#define LFBOE_CANCEL_REJECT_MSG_TYPE          'I'
#define LFBOE_REPLACED_MSG_TYPE               'R'

#define LFBOE_TIF_IOC                          0
#define LFBOE_TIF_DAY                          0xFFFFFFFD
#define LFBOE_TIF_GTX                          0xFFFFFFFE

#define LFBOE_SIDE_BUY                         'B'

#define LFBOE_EXECUTION_FLAGS_PARTIAL              0x00
#define LFBOE_EXECUTION_FLAGS_FILL                 0x01

#define LFBOE_LIQUIDITY_FLAG_ADDED                 'A'
#define LFBOE_LIQUIDITY_FLAG_REMOVED               'R'
#define LFBOE_LIQUIDITY_FLAG_HIDDEN                'H'

/****************************************************************************
- Function name:    HandleMsg_LFBOE
- Input:            'connection': the connection; 'dataBlock': the receive data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Handle LavaFlow Binary Order Entry Specification 
- Usage:            N/A
****************************************************************************/
int HandleMsg_LFBOE(t_Connection* client, t_DataBlock *dataBlock);


#endif //__LFBOE__
