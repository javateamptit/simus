#include "config.h"
#include "utility.h"
#include "soupbintcp.h"
#include "ufo.h"
#include "ouch.h"
#include "nasdaq_psx.h"

int TEST_PSX_logon (t_Connection* connection, const char* session, const char* user, const char* password)
{
#if NASDAQ_PSX_USE_UDP
  return UFO_BuildAndSend_LoginRequestMsg (connection, user, password, session);
#else // NASDAQ_PSX_USE_UDP
  return SOUPBINTCP_BuildAndSend_LoginRequestPacket (connection, user, password, session, 0);
#endif //NASDAQ_PSX_USE_UDP
}

int TEST_PSX_logout (t_Connection* connection)
{
#if NASDAQ_PSX_USE_UDP
  return UFO_BuildAndSend_LogoutRequestMsg (connection);
#else // NASDAQ_PSX_USE_UDP
  return SOUPBINTCP_BuildAndSend_LogoutRequestPacket (connection);
#endif //NASDAQ_PSX_USE_UDP
}

int TEST_PSX_heartbeat (t_Connection* connection)
{
#if NASDAQ_PSX_USE_UDP
  return UFO_BuildAndSend_ClientHeartbeatMsg (connection);
#else // NASDAQ_PSX_USE_UDP
  return SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket (connection);
#endif //NASDAQ_PSX_USE_UDP
}

int TEST_PSX_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
  char side = (strcmp(sd, "b")==0 || strcmp(sd, "B")==0) ? PSX_SIDE_BUY :
              (strcmp(sd, "s")==0 || strcmp(sd, "S")==0) ? PSX_SIDE_SELL : PSX_SIDE_SELL_SHORT;
  int quantity = atoi(qty);
  long price = (long)(atof(pr) * 10000.0);
  int tif = (strcmp(tf, "ioc")==0 || strcmp(tf, "IOC")==0) ? PSX_TIF_IOC: PSX_TIF_DAY;
 
  t_IntConverter intUnion;
  
  char message[48];
  memset(message, 0, 48);
  
  // Type "O"
  message[0] = 'O';
  // Token
  __StrWithSpaceRightPad(token, 14, &message[1]);
  // Side
  message[15] = side;
  // Quantity
  intUnion.value = quantity;
  message[16] = intUnion.c[3];
  message[17] = intUnion.c[2];
  message[18] = intUnion.c[1];
  message[19] = intUnion.c[0];
  // Stock
  __StrWithSpaceRightPad(symbol, 8, &message[20]);
  // Price
  intUnion.value = price;
  message[28] = intUnion.c[3];
  message[29] = intUnion.c[2];
  message[30] = intUnion.c[1];
  message[31] = intUnion.c[0];
  // Time in force
  intUnion.value = tif;
  message[32] = intUnion.c[3];
  message[33] = intUnion.c[2];
  message[34] = intUnion.c[1];
  message[35] = intUnion.c[0];
  // TODO:
  
#if NASDAQ_PSX_USE_UDP
  return UFO_BuildAndSend_UnsequencedMsg(connection, message, 48);
#else // NASDAQ_PSX_USE_UDP
  return SOUPBINTCP_BuildAndSend_UnsequencedDataPacket(connection, message, 48);
#endif
}

int TEST_PSX_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{
  int quantity = atoi(newQty);

  char message[19];
  memset(message, 0, 19);
  
  // Type "X"
  message[0] = 'X';
  // Token
  __StrWithSpaceRightPad(token, 14, &message[1]);
  // New size
  t_IntConverter intUnion;
  intUnion.value = quantity;
  message[15] = intUnion.c[3];
  message[16] = intUnion.c[2];
  message[17] = intUnion.c[1];
  message[18] = intUnion.c[0];

#if NASDAQ_PSX_USE_UDP
  return UFO_BuildAndSend_UnsequencedMsg(connection, message, 19);
#else // NASDAQ_PSX_USE_UDP
  return SOUPBINTCP_BuildAndSend_UnsequencedDataPacket(connection, message, 19);
#endif
}

void TEST_PSX_printInfo (t_Connection* connection, t_DataBlock* data)
{
  int contentOff = 3;
  if (connection->configInfo->type == SOCK_DGRAM)
    contentOff = 9;
    
  char type = 0;
  if (data->msgLen > contentOff)
     type = data->msgContent[contentOff];

  if (type == 'E')
  {
    char token[14] = "\0";
    memcpy(token, &data->msgContent[contentOff+9], 14);
    int share = uintAt((unsigned char*)data->msgContent, contentOff+23);
    int price = uintAt((unsigned char*)data->msgContent, contentOff+27);
    long match = ulongAt((unsigned char*)data->msgContent, contentOff+32);
    
    TraceLog(DEBUG_LEVEL, "\n%ld.PSX FILL %s: NUMBER %d OF %s IS %s AT %f\n", match, token, share, "Sym", "s/b?", price / 10000.0f);
    return;
  }
  
  if (type == 'C')
  {
    char token[14] = "\0";
    memcpy(token, &data->msgContent[contentOff+9], 14);
    
    TraceLog(DEBUG_LEVEL, "ARCA CANCEL %s\n", token);
    return;
  }
}
