INCLUDES=-I./include
BINARY=../bin/oec_sim
FLAGS=-Wall -g -mcmodel=medium

objs=sim.o global.o utility.o order_token_queue.o network.o fix_protocol.o soupbintcp.o souptcp.o ufo.o ouch.o arca.o bats_boe.o nasdaq_rash.o nyse.o edge.o nsx.o chx.o lava.o nasdaq_psx.o test.o test_bats.o test_chx.o test_lfboe.o test_psx.o test_arca.o test_edge.o test_rash.o test_nyse.o

all:clean oec_sim

oec_sim:$(objs)
	cd obj;gcc $(FLAGS) -o $(BINARY) $^ $(INCLUDES) -lpthread -lm

sim.o:./src/sim.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
  
global.o:./src/global.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
utility.o:./src/utility.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

order_token_queue.o:./src/order_token_queue.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
  
network.o:./src/network.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

fix_protocol.o:./src/fix_protocol.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

soupbintcp.o:./src/soupbintcp.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

souptcp.o:./src/souptcp.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

ufo.o:./src/ufo.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

arca.o:./src/arca.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
ouch.o:./src/ouch.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

bats_boe.o:./src/bats_boe.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
nasdaq_rash.o:./src/nasdaq_rash.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
nyse.o:./src/nyse.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
edge.o:./src/edge.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
nsx.o:./src/nsx.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
chx.o:./src/chx.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
lava.o:./src/lava.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
nasdaq_psx.o:./src/nasdaq_psx.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
test.o:./src/test.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
test_bats.o:./src/test/test_bats.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
  
test_psx.o:./src/test/test_psx.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
test_chx.o:./src/test/test_chx.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
test_lfboe.o:./src/test/test_lfboe.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

test_arca.o:./src/test/test_arca.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

test_edge.o:./src/test/test_edge.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
	
test_rash.o:./src/test/test_rash.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)
    
test_nyse.o:./src/test/test_nyse.c
	gcc $(FLAGS) -c $^ -o ./obj/$@ $(INCLUDES)

clean:
	mkdir -p ./obj
	mkdir -p ./bin
	rm -f ./obj/*.o ./bin/oec_sim