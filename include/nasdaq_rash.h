/****************************************************************************
** Project name:    US Blink Simulator
** File name:       nasdaq_rash.h
** Description:     N/A
** Specification:   RASHport
**                  Version 1.1
**                  February 6th, 2014
**
** Author:          Thien Nguyen-Tai
** First created:   April 17, 2014
** Last updated:    May 12, 2014
****************************************************************************/

#ifndef __NASDAQ_RASH__
#define __NASDAQ_RASH__

#include "global.h"
#include "souptcp.h"


// SOUPTCP protocol
#define RASH_PAYLOAD_OFFSET                         1
#define RASH_ADDITIONAL_DATA_PACKET_LEN             SOUPTCP_ADDITIONAL_DATA_PACKET_LEN

#define RASH_LOGIN_REQUEST_PACKET_TYPE              SOUPTCP_LOGIN_REQUEST_PACKET_TYPE
#define RASH_CLIENT_HEARTBEAT_PACKET_TYPE           SOUPTCP_CLIENT_HEARTBEAT_PACKET_TYPE
#define RASH_LOGOUT_REQUEST_PACKET_TYPE             SOUPTCP_LOGOUT_REQUEST_PACKET_TYPE
#define RASH_UNSEQUENCED_DATA_PACKET_TYPE           SOUPTCP_UNSEQUENCED_DATA_PACKET_TYPE

#define RASH_HeartbeatThread                        SOUPTCP_HeartbeatThread
#define RASH_Handle_LogonPacket                     SOUPTCP_Handle_LogonPacket
#define RASH_Handle_LogoutPacket                    SOUPTCP_Handle_LogoutPacket

#define RASH_BuildAndSend_SequencedDataPacket       SOUPTCP_BuildAndSend_SequencedDataPacket
#define RASH_BuildAndSend_LoginRequestPacket        SOUPTCP_BuildAndSend_LoginRequestPacket
#define RASH_BuildAndSend_ClientHeartbeatPacket     SOUPTCP_BuildAndSend_ClientHeartbeatPacket
#define RASH_BuildAndSend_LogoutRequestPacket       SOUPTCP_BuildAndSend_LogoutRequestPacket

// RASH protocol
#define RASH_ENTER_ORDER_MSG_LEN                    140
#define RASH_CANCEL_MSG_LEN                         21
#define RASH_ACCEPTED_MSG_LEN                       157
#define RASH_EXECUTED_MSG_LEN                       49
#define RASH_REJECTED_MSG_LEN                       24
#define RASH_CANCELLED_MSG_LEN                      30

#define RASH_ENTER_ORDER_MSG_TYPE                   'O'
#define RASH_CANCEL_MSG_TYPE                        'X'
#define RASH_ACCEPTED_MSG_TYPE                      'A'
#define RASH_EXECUTED_MSG_TYPE                      'E'
#define RASH_REJECTED_MSG_TYPE                      'J'
#define RASH_CANCELLED_MSG_TYPE                     'C'

#define RASH_TIF_IOC                                0
#define RASH_TIF_DAY                                99999
#define RASH_SIDE_BUY                               'B'
#define RASH_SIDE_SELL                              'S'
#define RASH_SIDE_SELL_SHORT                        'T'

#define RASH_CANCEL_REASON_USER                     'U'
#define RASH_CANCEL_REASON_IOC                      'I'
#define RASH_CANCEL_REASON_SYSTEM                   'S'

#define RASH_LIQUIDITY_FLAG_ADDED                   'A'
#define RASH_LIQUIDITY_FLAG_REMOVED                 'R'

int HandleMsg_RASH(t_Connection* client, t_DataBlock *dataBlock);

#endif // __NASDAQ_RASH__
