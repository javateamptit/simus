#include "bats_boe.h"
#include "network.h"

static long timeStamp()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  struct tm local;
  localtime_r(&tv.tv_sec, &local);
      
  long l1 = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  long l2 = l1 * 1000000000;
  long l3 = l2 + (tv.tv_usec * 1000);
  return l3;
}

static int GetOptionFieldsEnterOrder (t_OrderEntry* orderEntry, const char* data, const char* bitFields)
{
  int result = SUCCESS;
  int offset = 0;
  // for bit fields 1
  if (bitFields[0] & 0x01)
  {
    memcpy(orderEntry->clearingFirm, &data[offset], 4);
    offset += 4;
  }
  if (bitFields[0] & 0x02)
  {
    memcpy(orderEntry->clearingAccount, &data[offset], 4);
    offset += 4;
  }
  if (bitFields[0] & 0x04)
  {
    memcpy(&orderEntry->price, &data[offset], 8);
    offset += 8;
  }
  if (bitFields[0] & 0x08)
  {
    orderEntry->execInst = data[offset];
    offset++;
  }
  if (bitFields[0] & 0x10)
  {
    orderEntry->orderType = data[offset];
    offset++;
  }
  if (bitFields[0] & 0x20)
  {
    orderEntry->tif = data[offset];
    offset++;
  }
  if (bitFields[0] & 0x40)
  {
    memcpy(&orderEntry->minQty, &data[offset], 4);
    offset += 4;
  }
  if (bitFields[0] & 0x80)
  {
    memcpy(&orderEntry->maxFloor, &data[offset], 4);
    offset += 4;
  }
  
  // for bit fields 2
  if (bitFields[1] & 0x01)
  {
    memcpy(orderEntry->symbol, &data[offset], 8);
    offset += 8;
  }
  if (bitFields[1] & 0x02)
  {
    memcpy(orderEntry->symbolSuffix, &data[offset], 6);
    offset += 6;
  }
  // if (bitFields[1] & 0x04)
  // {
  // }
  // if (bitFields[1] & 0x08)
  // {
  // }
  // if (bitFields[1] & 0x10)
  // {
  // }
  // if (bitFields[1] & 0x20)
  // {
  // }
  if (bitFields[1] & 0x40)
  {
    orderEntry->capacity = data[offset];
    offset++;
  }
  if (bitFields[1] & 0x80)
  {
    memcpy(&orderEntry->RoutingInst, &data[offset], 4);
    offset += 4;
  }

  // for bit fields 3
  if (bitFields[2] & 0x01)
  {
    memcpy(orderEntry->clientId, &data[offset], 16);
    offset += 16;
  }
  if (bitFields[2] & 0x02)
  {
    orderEntry->displayIndicator = data[offset];
    offset++;
  }
  if (bitFields[2] & 0x04)
  {
    orderEntry->maxRemovePct = data[offset];
    offset++;
  }
  if (bitFields[2] & 0x08)
  {
    memcpy(&orderEntry->discretionAmount, &data[offset], 2);
    offset += 2;
  }
  if (bitFields[2] & 0x10)
  {
    memcpy(&orderEntry->pegDifference, &data[offset], 8);
    offset += 8;
  }
  if (bitFields[2] & 0x20)
  {
    memcpy(orderEntry->preventMemberMatch, &data[offset], 3);
    offset += 3;
  }
  if (bitFields[2] & 0x40)
  {
    orderEntry->locateReqd = data[offset];
    offset++;
  }
  if (bitFields[2] & 0x80)
  {
    memcpy(&orderEntry->expireTime, &data[offset], 8);
    offset += 8;
  }
  
  // for bit fields 4
  if (bitFields[3] & 0x80)
  {
    TraceLog(ERROR_LEVEL, "Invalid bit fields 4 \n");
    result = ERROR;
  }

  // for bit fields 5
  if (bitFields[4] & 0x02)
  {
    orderEntry->attributedQuote = data[offset];
    offset++;
  }
  if (bitFields[4] & 0x08)
  {
    orderEntry->extExecInst = data[offset];
    offset++;
  }
  if (bitFields[4] & 0xF0)
  {
    TraceLog(ERROR_LEVEL, "Invalid bit fields 5 \n");
    result = ERROR;
  }
  
  // for bit fields 6
  if (bitFields[5] & 0xFF)
  {
    TraceLog(ERROR_LEVEL, "Invalid bit fields 6 \n");
    result = ERROR;
  }
  
  return result;
}

static int SetReturnOptionalField1 (t_OrderEntry* orderEntry, char* buffer, const char bitFields)
{
  int offset = 0; // maximum = 25
  
  if (bitFields & 0x01)
  {
    buffer[offset] = orderEntry->side;
    offset++;
  }
  if (bitFields & 0x02)
  {
    memcpy(&buffer[offset], &orderEntry->pegDifference, 8);
    offset += 8;
  }
  if (bitFields & 0x04)
  {
    memcpy(&buffer[offset], &orderEntry->price, 8);
    offset += 8;
  }
  if (bitFields & 0x08)
  {
    buffer[offset] = orderEntry->execInst;
    offset++;
  }
  if (bitFields & 0x10)
  {
    buffer[offset] = orderEntry->orderType;
    offset++;
  }
  if (bitFields & 0x20)
  {
    buffer[offset] = orderEntry->tif;
    offset++;
  }
  if (bitFields & 0x40)
  {
    memcpy(&buffer[offset], &orderEntry->minQty, 4);
    offset += 4;
  }
  if (bitFields & 0x80)
  {
    buffer[offset] = orderEntry->maxRemovePct;
    offset++;
  }
  
  return offset;
}

static int SetReturnOptionalField2 (t_OrderEntry* orderEntry, char* buffer, const char bitFields)
{
  int offset = 0; // maximum = 15
  
  if (bitFields & 0x01)
  {
    memcpy(&buffer[offset], orderEntry->symbol, 8);
    offset += 8;
  }
  if (bitFields & 0x02)
  {
    memcpy(&buffer[offset], orderEntry->symbolSuffix, 6);
    offset += 6;
  }
  // if (bitFields & 0x04)
  // {
  // }
  // if (bitFields & 0x08)
  // {
  // }
  // if (bitFields & 0x10)
  // {
  // }
  // if (bitFields & 0x20)
  // {
  // }
  if (bitFields & 0x40)
  {
    buffer[offset] = orderEntry->capacity;
    offset++;
  }
  // if (bitFields & 0x80)
  // {
  // }
  
  return offset;
}

static int SetReturnOptionalField3 (t_OrderEntry* orderEntry, char* buffer, const char bitFields)
{
  int offset = 0; // maximum = 38
  
  if (bitFields & 0x01)
  {
    memcpy(&buffer[offset], orderEntry->clientId, 16);
    offset += 16;
  }
  if (bitFields & 0x02)
  {
    memcpy(&buffer[offset], orderEntry->clearingFirm, 4);
    offset += 4;
  }
  if (bitFields & 0x04)
  {
    memcpy(&buffer[offset], orderEntry->clearingAccount, 4);
    offset += 4;
  }
  if (bitFields & 0x08)
  {
    buffer[offset] = orderEntry->displayIndicator;
    offset++;
  }
  if (bitFields & 0x10)
  {
    memcpy(&buffer[offset], &orderEntry->maxFloor, 4);
    offset += 4;
  }
  if (bitFields & 0x20)
  {
    memcpy(&buffer[offset], &orderEntry->discretionAmount, 2);
    offset += 2;
  }
  if (bitFields & 0x40)
  {
    memcpy(&buffer[offset], &orderEntry->quantity, 4);
    offset += 4;
  }
  if (bitFields & 0x80)
  {
    memcpy(&buffer[offset], &orderEntry->preventMemberMatch, 3);
    offset += 3;
  }
  
  return offset;
}

static int SetReturnOptionalField4 (t_OrderEntry* orderEntry, char* buffer, const char bitFields)
{
  // RESERVED
  return 0;
}

static int SetReturnOptionalField5 (t_OrderEntry* orderEntry, char* buffer, const char bitFields,
      const char* orgClOrdId, const int leavesQty, const int lastShares, const long lastPx,
      const long displayPx, const long workingPx, const char baseLiquidityIndicator)
{
  int offset = 0; //maximum 61

  if (bitFields & 0x01)
  {
    strncpy(&buffer[offset], orgClOrdId, 20);
    offset += 20;
  }
  if (bitFields & 0x02)
  {
    memcpy(&buffer[offset], &leavesQty, 4);
    offset += 4;
  }
  if (bitFields & 0x04)
  {
    memcpy(&buffer[offset], &lastShares, 4);
    offset += 4;
  }
  if (bitFields & 0x08)
  {
    memcpy(&buffer[offset], &lastPx, 8);
    offset += 8;
  }
  if (bitFields & 0x10)
  {
    memcpy(&buffer[offset], &displayPx, 8);
    offset += 8;
  }
  if (bitFields & 0x20)
  {
    memcpy(&buffer[offset], &workingPx, 8);
    offset += 8;
  }
  if (bitFields & 0x40)
  {
    buffer[offset] = baseLiquidityIndicator;
    offset++;
  }
  if (bitFields & 0x80)
  {
    memcpy(&buffer[offset], &orderEntry->expireTime, 8);
    offset += 8;
  }
  
  return offset;
}

static int SetReturnOptionalField6 (t_OrderEntry* orderEntry, char* buffer, const char bitFields,
      const long secondaryOrderId, const char attributedQuote)
{
  int offset = 0; // maximum 9
  
  if (bitFields & 0x01)
  {
    memcpy(&buffer[offset], &secondaryOrderId, 8);
    offset += 8;
  }
  if (bitFields & 0x08)
  {
    buffer[offset] = attributedQuote;
    offset++;
  }
  
  return offset;
}

static int SetReturnOptionalField7 (t_OrderEntry* orderEntry, char* buffer, const char bitFields, const char subLiquidityIndicator)
{
  int offset = 0; //maximum 1
  
  if (bitFields & 0x01)
  {
    buffer[offset] = subLiquidityIndicator;
    offset++;
  }
  
  return offset;
}

static void BuildCommonAppMsgHeader (char* message, const int msgLen, const char type,
      const char matching, const int seqNum, const char* clOrdId)
{
  message[0] = 0xBA;
  message[1] = 0xBA;
  
  unsigned short mgsLength = msgLen - 2; // Not including 2 bytes of start of message
  // Message length: 2 bytes(2-3)
  memcpy(&message[2], &mgsLength, 2);
  
  // Message type: 1 byte
  message[4] = type;
  
  // Matching Unit
  message[5] = matching;
  
  // Sequence Number
  memcpy(&message[6], &seqNum, 4);
  
  // Transaction time
  long timeval = timeStamp();
  memcpy(&message[10], &timeval, 8);
  
  // ClOrderId
  strncpy(&message[18], clOrdId, 20);  
}

static int BuildAndSendServerHeartbeat(t_Connection* connection)
{
  char message[BATS_BOE_SERVER_HEARTBEAT_LEN];
  memset(message, 0, BATS_BOE_SERVER_HEARTBEAT_LEN);
  
  // Start of Message
  message[0] = BATS_BOE_START_OF_MESSAGE;
  message[1] = BATS_BOE_START_OF_MESSAGE;
  // Message length
  message[2] = 0x08;
  // Message type
  message[4] = BATS_BOE_SERVER_HEARTBEAT_TYPE;
  // Matching Unit => 0
  // Sequence number => 0
  
  return SendMsg(connection->socket, &connection->lock, message, BATS_BOE_SERVER_HEARTBEAT_LEN);
}

static int BuildAndSendLogonResponse(t_Connection* connection, const char status, const char* text,
      const char numberUnit, const char* unit)
{
  const short msgLen = BATS_BOE_LOGON_RESPONSE_SHORT_LEN + numberUnit * 5;
  char message[msgLen];
  memset(message, 0, msgLen);
  
  // Start of Message
  message[0] = BATS_BOE_START_OF_MESSAGE;
  message[1] = BATS_BOE_START_OF_MESSAGE;
  // Message length
  unsigned short mgsLength = msgLen - 2; // Not including 2 bytes of start of message
  memcpy(&message[2], &mgsLength, 2);
  // Message type
  message[4] = BATS_BOE_LOGON_RESPONSE_TYPE;
  // Matching Unit => 0
  // Sequence number => 0
  // Login response status
  message[10] = status;
  // Login response text
  if (text != NULL)
    strncpy(&message[11], text, 60);
  //NoUnspecified Unit Replay
  message[71] = connection->noUnspecifiedUnitReplay;
  //*********** Order Acknowledgement Bitfield (72-78) ***********
  memcpy(&message[72], connection->bitFields.ACK, 7);
  //*********** Order Rejected Bitfields (80-86) ***********
  memcpy(&message[80], connection->bitFields.Rejected, 7);
  //*********** Order Modified Bitfields (88-94) ***********
  memcpy(&message[88], connection->bitFields.Modified, 7);
  //*********** Order Restated Bitfields (96-102) ***********
  memcpy(&message[96], connection->bitFields.Restates, 7);
  //*********** User Modify Rejected Bitfields(104-110) ***********
  memcpy(&message[104], connection->bitFields.ModifyReject, 7);
  //*********** Order Cancelled Bitfields (112-118***********
  memcpy(&message[112], connection->bitFields.Cancelled, 7);
  //*********** Order Cancel Reject Bitfields (120-126) ***********
  memcpy(&message[120], connection->bitFields.CancelReject, 7);
  //*********** Execution Bitfields (128-134) ***************/
  memcpy(&message[128], connection->bitFields.Execution, 7);
  //*********** Trade Cancel or Correct Bitfields (136-142) ***********
  memcpy(&message[136], connection->bitFields.Correct, 7);  
  // Reserved message (144 -> 159)  
  // Last received sequence number 
  memcpy(&message[160], &connection->userInfo->outgoingSeq, 4);
  // Number of Units
  message[164] = numberUnit; // just use one byte
  // Unit Number and Unit Sequence
  if (numberUnit > 0 && unit != NULL)
    memcpy(&message[165], unit, numberUnit * 5);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int BuildAndSendLogoutResponse (t_Connection* connection, const char reason, const char* text,
      const char numberUnit, const char* unit)
{
  const short msgLen = BATS_BOE_LOGOUT_RESPONSE_SHORT_LEN + numberUnit * 5;
  char message[msgLen];
  memset(message, 0, msgLen);
  
  // Start of message
  message[0] = BATS_BOE_START_OF_MESSAGE;
  message[1] = BATS_BOE_START_OF_MESSAGE;
  // Message length
  unsigned short mgsLength = msgLen - 2; // Not including 2 bytes of start of message
  memcpy(&message[2], &mgsLength, 2);
  // Message type
  message[4] = BATS_BOE_LOGOUT_RESPONSE_TYPE;
  // Matching unit => 0
  // Sequence number => 0
  // Reason
  message[10] = reason;
  // Reason text
  if (text != NULL)
    strncpy(&message[11], text, 60);
  // Last sequence number
  memcpy(&message[71], &connection->userInfo->outgoingSeq, 4);
  // Number of units
  message[75] = numberUnit;
  // Units
  if (numberUnit > 0 && unit != NULL)
    memcpy(&message[76], unit, numberUnit * 5);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int BuildAndSendReplayComplete(t_Connection* connection)
{
  char message[BATS_BOE_REPLAY_COMPLETE_LEN];
  memset(message, 0, BATS_BOE_REPLAY_COMPLETE_LEN);
  
  // Start of Message
  message[0] = BATS_BOE_START_OF_MESSAGE;
  message[1] = BATS_BOE_START_OF_MESSAGE;
  // Message length
  message[2] = 0x08;
  // Message type
  message[4] = BATS_BOE_REPLAY_COMPLETE_TYPE;
  // Matching Unit => 0
  // Sequence number => 0
  
  return SendMsg(connection->socket, &connection->lock, message, BATS_BOE_REPLAY_COMPLETE_LEN);
}

static int BuildAndSendACK(t_Connection* connection, t_OrderEntry *orderEntry)
{
  char message[BATS_BOE_ACK_MAX_LEN];
  memset(message, 0, BATS_BOE_ACK_MAX_LEN); 
 
  //OrderID
  orderEntry->orderID = __sync_add_and_fetch(&connection->orderID, 1);
  memcpy(&message[38], &orderEntry->orderID, 8);

  // Order ACK Bitfield
  memcpy(&message[46], connection->bitFields.ACK, 7);
  
  // Optional Fields
  int msgLen = 54;
  msgLen += SetReturnOptionalField1(orderEntry, &message[msgLen], connection->bitFields.ACK[0]);
  msgLen += SetReturnOptionalField2(orderEntry, &message[msgLen], connection->bitFields.ACK[1]);
  msgLen += SetReturnOptionalField3(orderEntry, &message[msgLen], connection->bitFields.ACK[2]);
  msgLen += SetReturnOptionalField4(orderEntry, &message[msgLen], connection->bitFields.ACK[3]);
  msgLen += SetReturnOptionalField5(orderEntry, &message[msgLen], connection->bitFields.ACK[4], orderEntry->clOrdID, 0, 0, 0, 0, 0, 0);
  msgLen += SetReturnOptionalField6 (orderEntry, &message[msgLen], connection->bitFields.ACK[5] & 0x08, 0, 'N'); // use attributedQuote
  msgLen += SetReturnOptionalField7 (orderEntry, &message[msgLen], connection->bitFields.ACK[6], 0);
  
  BuildCommonAppMsgHeader(message, msgLen, BATS_BOE_ACK_TYPE, 1, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1), orderEntry->clOrdID);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int BuildAndSendCancelled(t_Connection* connection, t_QueueItem *item)
{
  char message[BATS_BOE_CANCELLED_MAX_LEN];
  memset(message, 0, BATS_BOE_CANCELLED_MAX_LEN); 
  
  char reason = 'A';
  // reason bit 38
  message[38] = reason;
  
  // Order Cancelled Bitfield
  memcpy(&message[39], connection->bitFields.Cancelled, 7);
  
  // orderEntry
  t_OrderEntry orderEntry;
  memcpy(&orderEntry, &item->orderEntry, sizeof(t_OrderEntry));
  orderEntry.quantity = item->orderEntry.quantity - item->cumShare;

  // Optional Fields
  int msgLen = 47;
  msgLen += SetReturnOptionalField1(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[0] & 0x01); // only side value
  msgLen += SetReturnOptionalField2(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[1] & 0x01); // only symbol value
  msgLen += SetReturnOptionalField3(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[2]);
  msgLen += SetReturnOptionalField4(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[3]);
  msgLen += SetReturnOptionalField5(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[4], orderEntry.clOrdID, 0, 0, 0, 0, 0, 0);
  msgLen += SetReturnOptionalField6(&orderEntry, &message[msgLen], connection->bitFields.Cancelled[5] & 0x01, 0, 'N'); // use secondaryOrderId
  
  BuildCommonAppMsgHeader(message, msgLen, BATS_BOE_CANCELLED_TYPE, 1, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1), orderEntry.clOrdID);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int BuildAndSendCancelReject(t_Connection* connection, t_QueueItem *item, const char reason, const char* text)
{
  char message[BATS_BOE_CANCEL_REJECT_MAX_LEN];
  memset(message, 0, BATS_BOE_CANCEL_REJECT_MAX_LEN); 
  
  // reason bit 38
  message[38] = reason;
  // reason description
  if (text != NULL)
    strncpy(&message[39], text, 60);
  // Order Cancel Reject Bitfield => 0
  // Optional Fields => NOT USE
  
  BuildCommonAppMsgHeader(message, BATS_BOE_CANCEL_REJECT_SHORT_LEN, BATS_BOE_CANCEL_REJECT_TYPE, 0, 0, item->orderEntry.clOrdID);
  
  return SendMsg(connection->socket, &connection->lock, message, BATS_BOE_CANCEL_REJECT_SHORT_LEN);
}

static int BuildAndSendFilled(t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  char message[BATS_BOE_EXECUTION_MAX_LEN];
  memset(message, 0, BATS_BOE_EXECUTION_MAX_LEN);
  
  // Execution ID
  __sync_add_and_fetch(&connection->executionNumber, 1);
  memcpy(&message[38], &connection->executionNumber, 8);
  // Last shares
  memcpy(&message[46], &quantity, 4);
  // Last price
  memcpy(&message[50], &price, 8);
  // Leaves quantity
  int leavesQty = item->orderEntry.quantity - item->cumShare;
  memcpy(&message[58], &leavesQty, 4);
  // Base Liquidity Indicator
  message[62] = 'R';
  // Sub Liquidity Indicator => 0
  // Access fee => 0 <free :D>
  // Contra broker
  char contraBroker[4] = "BATS"; // TODO:
  strncpy(&message[72], contraBroker, 4);
  
  // Order Execution Bitfield
  memcpy(&message[76], connection->bitFields.Execution, 7);
  
  t_OrderEntry orderEntry;
  memcpy(&orderEntry, &item->orderEntry, sizeof(t_OrderEntry));
  orderEntry.price = price;
  orderEntry.quantity = quantity;
  
  // Optional Fields
  int msgLen = 84;
  msgLen += SetReturnOptionalField1(&orderEntry, &message[msgLen], connection->bitFields.Execution[0]);
  msgLen += SetReturnOptionalField2(&orderEntry, &message[msgLen], connection->bitFields.Execution[1]);
  msgLen += SetReturnOptionalField3(&orderEntry, &message[msgLen], connection->bitFields.Execution[2]);
  msgLen += SetReturnOptionalField4(&orderEntry, &message[msgLen], connection->bitFields.Execution[3]);
  
  BuildCommonAppMsgHeader(message, msgLen, BATS_BOE_EXECUTION_TYPE, 0, 0, orderEntry.clOrdID);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int BuildAndSendRejected(t_Connection* connection, t_OrderEntry *orderEntry, const char reason, const char* text)
{
  char message[BATS_BOE_REJECTED_MAX_LEN];
  memset(message, 0, BATS_BOE_REJECTED_MAX_LEN);
  
  // reason bit 38
  message[38] = reason;
  
  // text 39 -> 98  
  strncpy(&message[39], text, 60);
  
  // Order Rejected Bitfield
  memcpy(&message[99], connection->bitFields.Rejected, 7);
  
  // Optional Fields
  int msgLen = 107;
  msgLen += SetReturnOptionalField1(orderEntry, &message[msgLen], connection->bitFields.Rejected[0]);
  msgLen += SetReturnOptionalField2(orderEntry, &message[msgLen], connection->bitFields.Rejected[1]);
  msgLen += SetReturnOptionalField3(orderEntry, &message[msgLen], connection->bitFields.Rejected[2]);
  msgLen += SetReturnOptionalField4(orderEntry, &message[msgLen], connection->bitFields.Rejected[3]);
  
  BuildCommonAppMsgHeader(message, msgLen, BATS_BOE_REJECTED_TYPE, 0, 0, orderEntry->clOrdID);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen);
}

static int HandleMsgLogon(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  int seqNum = *(int*)(&dataBlock->msgContent[6]);
  if (seqNum != 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid sequence number logon \n");
    return BuildAndSendLogonResponse(connection, 'Q', "Invalid sequence number logon", 0, NULL);
  }
  
  // char session[5] = "\0";
  // memcpy(&session, &dataBlock->msgContent[10], 4);
  // TODO: validate session
  
  memcpy(connection->userInfo->userName, &dataBlock->msgContent[14], 4);
  memcpy(connection->userInfo->password, &dataBlock->msgContent[18], 10);
  // TODO: check userName and password
  
  connection->noUnspecifiedUnitReplay = dataBlock->msgContent[28];

  memcpy(connection->bitFields.ACK, &dataBlock->msgContent[29], 7);
  memcpy(connection->bitFields.Rejected, &dataBlock->msgContent[37], 7);
  memcpy(connection->bitFields.Modified, &dataBlock->msgContent[45], 7);
  memcpy(connection->bitFields.Restates, &dataBlock->msgContent[53], 7);
  memcpy(connection->bitFields.ModifyReject, &dataBlock->msgContent[61], 7);
  memcpy(connection->bitFields.Cancelled, &dataBlock->msgContent[69], 7);
  memcpy(connection->bitFields.CancelReject, &dataBlock->msgContent[77], 7);
  memcpy(connection->bitFields.Execution, &dataBlock->msgContent[85], 7);
  memcpy(connection->bitFields.Correct, &dataBlock->msgContent[93], 7);
  // TODO: check return bit fields
  
  BuildAndSendLogonResponse(connection, 'A', NULL, 0, NULL);
  BuildAndSendReplayComplete(connection);
  
  return SUCCESS;
}

static int HandleMsgLogout(t_Connection* connection, t_DataBlock *dataBlock)
{
  int seqNum = *(int*)(&dataBlock->msgContent[6]);
  if (seqNum != 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid sequence number \n");
  }
  
  BuildAndSendLogoutResponse(connection, 'U', "User request", 0, NULL);
  CloseConnection(connection);
  
  return SUCCESS;
}

static int HandleMsgNewOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  // int seqNum = *(int*)(&dataBlock->msgContent[6]);
  // if (seqNum != connection->userInfo->incomingSeq)
  // {
    // TraceLog(ERROR_LEVEL, "Sequence number doesn't match\n");
  // }
  
  memcpy(orderEntry.clOrdID, &dataBlock->msgContent[10], 20);  
  orderEntry.side = dataBlock->msgContent[30];
  memcpy(&orderEntry.quantity, &dataBlock->msgContent[31], 4);
  
  GetOptionFieldsEnterOrder(&orderEntry, &dataBlock->msgContent[41], &dataBlock->msgContent[35]);
  
  TraceLog(DEBUG_LEVEL, "BATSZ New order: symbol(%.8s), clOrdID(%s), quantity(%d), price(%lf) side(%c) tif(%d)\n", 
       orderEntry.symbol, orderEntry.clOrdID, 
       orderEntry.quantity, orderEntry.price * 0.0001, 
       orderEntry.side, orderEntry.tif);
  
  if(orderEntry.tif != '0' && orderEntry.tif != '3')
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    BuildAndSendRejected(connection, &orderEntry, 'I', "Reject by TIF\n");
  }
  else if(orderEntry.side != '1' && orderEntry.side != '2' && orderEntry.side != '5' && orderEntry.side != '6')
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    BuildAndSendRejected(connection, &orderEntry, 'I', "Reject by Side\n");
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    BuildAndSendRejected(connection, &orderEntry, 'I', "Reject by Quantity\n");
  }
  else
  {
    BuildAndSendACK(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;

    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, BATS_BOE_SIDE_BUY, &BuildAndSendFilled);
    //IOC Random
    if (orderEntry.tif == BATS_BOE_TIMEINFORCE_IOC)
      if (hasCross)
      {
        if (item.orderEntry.quantity > item.cumShare)
          BuildAndSendCancelled(connection, &item);
      }
      else
        Simulator_Handle_RandomIOC(connection, &item, &BuildAndSendFilled, &BuildAndSendCancelled);
    else // DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static int HandleMsgCancelOrder (t_Connection* connection, t_DataBlock* dataBlock)
{
  t_QueueItem item;
  memset(&item, 0, sizeof(t_QueueItem));
  
  memcpy(item.orderEntry.clOrdID, &dataBlock->msgContent[10], 20);
  
  char bitFields = dataBlock->msgContent[30];
  if (bitFields & 0xF0)
  {
    TraceLog(ERROR_LEVEL, "Invalid Cancel Order Bitfield1 \n");
    BuildAndSendCancelReject(connection, &item, 'I', "Invalid Cancel Order Bitfield1");
  }
  
  bitFields = dataBlock->msgContent[31];
  if (bitFields & 0xFF)
  {
    TraceLog(ERROR_LEVEL, "Invalid Cancel Order Bitfield2 \n");
    BuildAndSendCancelReject(connection, &item, 'I', "Invalid Cancel Order Bitfield2");
  }
  
  // char clearingFirm[4];
  // memcpy(clearingFirm, &dataBlock->msgContent[32], 4);
  pthread_mutex_lock(&connection->orderQueue.mutex);
    int res = SearchAndRemoveToken(&connection->orderQueue, &item);
    if(res == SUCCESS)
    {
      TraceLog(DEBUG_LEVEL, "Cancelled token (%s) \n", item.orderEntry.clOrdID);
      BuildAndSendCancelled(connection, &item);
    }
    else
    {
      TraceLog(DEBUG_LEVEL, "Not found order (%s), errorCode (%d)\n", item.orderEntry.clOrdID, res);
      BuildAndSendCancelReject(connection, &item, 'O', "clOrdId doesn't match");
    }
  pthread_mutex_unlock(&connection->orderQueue.mutex);
   
  return SUCCESS;
}

static void* DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    Simulator_Handle_RandomDAY(connection, &BuildAndSendFilled, &BuildAndSendCancelled);
    
    sleep(15);
  }
  
  return NULL;
}

static void* HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    BuildAndSendServerHeartbeat(connection);
    sleep(1);
  }
  
  TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
  return NULL;
} 

int HandleMsg_BATS(t_Connection* connection, t_DataBlock *dataBlock)
{
  unsigned short startOfMsg = *(unsigned short*)(dataBlock->msgContent);
  if (startOfMsg != 0xBABA)
  {
    TraceLog(ERROR_LEVEL, "Invalid start of message \n");
    return SUCCESS; // don't close the connection, continue next message
  }
  
  char matching = dataBlock->msgContent[5];
  if (matching != 0)
  {
    TraceLog(ERROR_LEVEL, "Invalid matching unit \n");
    return SUCCESS; // don't close the connection, continue next message
  }
  
  char msgType = dataBlock->msgContent[4];
  switch(msgType)
  {
    case 0x01:
      TraceLog(DEBUG_LEVEL, "Received login message from connection\n");
      if(HandleMsgLogon(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, DAYThread, (void*)connection);

        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, HeartbeatThread, (void*)connection);
      }
      break;
      
    case 0x02:
      TraceLog(DEBUG_LEVEL, "BATS: Received logout message from connection\n");
      HandleMsgLogout(connection, dataBlock);
      break;
      
    case 0x03:
      TraceLog(DEBUG_LEVEL, "Received heartbeat from connection\n");
      break;
    
    case 0x04:
      TraceLog(DEBUG_LEVEL, "Received new order from connection\n");
      HandleMsgNewOrder(connection, dataBlock);
      break;
      
    case 0x05:
      TraceLog(DEBUG_LEVEL, "Received cancel order from connection\n");
      HandleMsgCancelOrder(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown MsgType (%02X)\n", msgType);
      break;
  }

  return SUCCESS;
}
