#include "nasdaq_psx.h"


static int PSX_Handle_MsgCancelOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  PSX_GetCancelOrderEntry(&orderEntry, msg, msgLen);
  
  return Simulator_Handle_CancelOrder(connection, &orderEntry, &PSX_Callback_UserCancel, &PSX_Callback_CancelReject);
}

static int PSX_Handle_MsgNewOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  PSX_GetNewOrderEntry(&orderEntry, msg, msgLen);
  
  if(orderEntry.tif != PSX_TIF_IOC && orderEntry.tif != PSX_TIF_DAY)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    PSX_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Invalid time in force 
  }
  else if(orderEntry.side != PSX_SIDE_BUY && orderEntry.side != PSX_SIDE_SELL && orderEntry.side != PSX_SIDE_SELL_SHORT)
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    PSX_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Invalid Side
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    PSX_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Order quantity missing/invalid
  }
  else
  {
    PSX_BuildAndSend_MsgAccepted(connection, &orderEntry, PSX_ORDER_STATE_LIVE, NULL);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;

    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, PSX_SIDE_BUY, &PSX_Callback_Filled);
    //IOC Random
    if (orderEntry.tif == PSX_TIF_IOC) {
      if (hasCross) {
        if (item.orderEntry.quantity > item.cumShare)
          PSX_BuildAndSend_MsgCancelled(connection, &item.orderEntry, item.orderEntry.quantity - item.cumShare, PSX_CANCEL_REASON_IOC);
      } else {
        Simulator_Handle_RandomIOC(connection, &item, &PSX_Callback_Filled, &PSX_Callback_SystemCancel);
      }
    } else {// DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
    }
  }
  
  return SUCCESS;
}

static int PSX_Handle_UnsequencedDataPacket(t_Connection* connection, const char* msg, const int msgLen)
{
  switch(msg[0])
  {
    case PSX_ENTER_ORDER_MSG_TYPE:
      return PSX_Handle_MsgNewOrder(connection, msg, msgLen);
      break;
    
    case PSX_CANCEL_MSG_TYPE:
      return PSX_Handle_MsgCancelOrder(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "Unknown message type '%c'\n", msg[0]);
      break;
  }
  
  return SUCCESS;
}

static void* PSX_DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;

  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &PSX_Callback_Filled, &PSX_Callback_SystemCancel);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_PSX(t_Connection* connection, t_DataBlock *dataBlock)
{
#if NASDAQ_PSX_USE_UDP
  if (connection->configInfo->type != SOCK_DGRAM)
  {
    TraceLog(ERROR_LEVEL, "HandleMsg_OUCH: Socket type isn't yet supported\n");
    return ERROR;
  }
#else // NASDAQ_PSX_USE_UDP
  if (connection->configInfo->type != SOCK_STREAM)
  {
    TraceLog(ERROR_LEVEL, "HandleMsg_OUCH: Socket type isn't yet supported\n");
    return ERROR;
  }
#endif // NASDAQ_PSX_USE_UDP
  
  switch(dataBlock->msgContent[2])
  {
    case PSX_LOGIN_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received login packet from client\n");
      if(PSX_Handle_LogonPacket(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, PSX_DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, PSX_HeartbeatThread, (void*)connection);
      }
      break;
    
    case PSX_CLIENT_HEARTBEAT_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received heartbeat packet from client\n");
      break;
    
    case PSX_LOGOUT_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logout packet from client\n");
      PSX_Handle_LogoutPacket(connection, dataBlock);
      break;
    
    case PSX_UNSEQUENCED_DATA_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received unsequenced data packet from client\n");
      int msgLen;
      char* msg = PSX_GetContent_UnsequencedPacket(dataBlock, &msgLen);
      PSX_Handle_UnsequencedDataPacket(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown packet type '%c'\n", dataBlock->msgContent[2]);
      break;
  }
  return SUCCESS;
}

