#include "edge.h"


static int BuildAndSendMsgReject(t_Connection* connection, t_OrderEntry* orderEntry,
      const char reason, const char responseTo, const int isExtended)
{
  const int msgLen = isExtended ? EDGE_EXTENDED_REJECTED_MSG_LEN : EDGE_REJECTED_MSG_LEN;
  char message[msgLen];
  
  // Message Type "J" or "L" Rejected message 
  message[0] = isExtended ? EDGE_EXTENDED_REJECTED_MSG_TYPE : EDGE_REJECTED_MSG_TYPE;
  // Timestamp
  long m_time = __builtin_bswap64(GetLocalTime_usec());
  memcpy(&message[1], &m_time, 8);
  // Order Token
  strncpy(&message[9], orderEntry->clOrdID, 14);
  // Reject reason
  message[23] = reason;
  // In Response To
  if (isExtended)
    message[24] = responseTo;
    
  return EDGE_MEP_BuildAndSend_SequencedDataPacket(connection, message, msgLen);
}

static int BuildAndSendMsgAccepted(t_Connection* connection, t_OrderEntry* orderEntry, const int isExtended)
{
  t_IntConverter intUnion;
  t_LongConverter longUnion;
  const int msgLen = isExtended ? EDGE_EXTENDED_ACCEPTED_MSG_LEN : EDGE_ACCEPTED_MSG_LEN;
  char message[msgLen];
  
  // Message Type "A" or "P"
  message[0] = isExtended ? EDGE_EXTENDED_ACCEPTED_MSG_TYPE : EDGE_ACCEPTED_MSG_TYPE;
  // Timestamp
  longUnion.value = GetLocalTime_usec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  strncpy(&message[9], orderEntry->clOrdID, 14);
  // Buy/Sell Indicator
  message[23] = orderEntry->side;
  // Quantity
  intUnion.value = orderEntry->quantity;
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  // Symbol
  strncpy(&message[28], orderEntry->symbol, 6);
  // Price
  intUnion.value = orderEntry->price;
  message[34] = intUnion.c[3];
  message[35] = intUnion.c[2];
  message[36] = intUnion.c[1];
  message[37] = intUnion.c[0];
  // Time in Force
  message[38] = orderEntry->tif;
  // Display
  message[39] = orderEntry->displayIndicator;
  // Special Order Type 
  message[40] = orderEntry->orderType;
  // Extended Hrs Eligible 
  message[41] = orderEntry->extendedHrsEligible;
  // Order Reference Number
  longUnion.value = __sync_add_and_fetch(&connection->orderID, 1);;
  message[42] = longUnion.c[7];
  message[43] = longUnion.c[6];
  message[44] = longUnion.c[5];
  message[45] = longUnion.c[4];
  message[46] = longUnion.c[3];
  message[47] = longUnion.c[2];
  message[48] = longUnion.c[1];
  message[49] = longUnion.c[0];
  // Capacity
  message[50] = orderEntry->capacity;
  // Route Out Eligibility
  message[51] = orderEntry->routeOutEligibility;
  // Inter-market Sweep (ISO) Eligibility 
  message[52] = orderEntry->interMarketSweep;
  
  if (isExtended)
  {
    t_ShortConverter shortUnion;
    
    // Routing Delivery Method 
    message[53] = orderEntry->routingDeliveryMethod;
    // Route Strategy
    shortUnion.value = orderEntry->routeStrategy;
    message[54] = shortUnion.c[1];
    message[55] = shortUnion.c[0];
    // Minimum Quantity 
    intUnion.value = orderEntry->minQty;
    message[56] = intUnion.c[3];
    message[57] = intUnion.c[2];
    message[58] = intUnion.c[1];
    message[59] = intUnion.c[0];
    // Max Floor
    intUnion.value = orderEntry->maxFloor;
    message[60] = intUnion.c[3];
    message[61] = intUnion.c[2];
    message[62] = intUnion.c[1];
    message[63] = intUnion.c[0];
    // Peg Difference
    message[64] = orderEntry->pegDifference;
    // Discretionary Offset 
    message[65] = orderEntry->discretionAmount;
    // Expire Time 
    longUnion.value = orderEntry->expireTime;
    message[66] = longUnion.c[7];
    message[67] = longUnion.c[6];
    message[68] = longUnion.c[5];
    message[69] = longUnion.c[4];
    message[70] = longUnion.c[3];
    message[71] = longUnion.c[2];
    message[72] = longUnion.c[1];
    message[73] = longUnion.c[0];
    // Symbol Suffix
    memcpy(&message[74], orderEntry->symbolSuffix, 6);
  }

  return EDGE_MEP_BuildAndSend_SequencedDataPacket(connection, message, msgLen);
}

static int BuildAndSendMsgExecuted(t_Connection* connection, t_QueueItem* item,
      const int quantity, const int price, const char* liqidFlag, const long matchNum)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  t_IntConverter intUnion;
  t_LongConverter longUnion;
  char message[EDGE_EXECUTED_MSG_LEN];  
  
  // Message Type "E"
  message[0] = EDGE_EXECUTED_MSG_TYPE;
  // Timestamp
  longUnion.value = GetLocalTime_usec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  strncpy(&message[9], item->orderEntry.clOrdID, 14);
  // Executed Quantity
  intUnion.value = quantity;
  message[23] = intUnion.c[3];
  message[24] = intUnion.c[2];
  message[25] = intUnion.c[1];
  message[26] = intUnion.c[0];
  // Execution Price
  intUnion.value = price;
  message[27] = intUnion.c[3];
  message[28] = intUnion.c[2];
  message[29] = intUnion.c[1];
  message[30] = intUnion.c[0];
  // Liquidity Flag
  strncpy(&message[31], liqidFlag, 5);
  // Match Number
  longUnion.value = matchNum;
  message[36] = longUnion.c[7];
  message[37] = longUnion.c[6];
  message[38] = longUnion.c[5];
  message[39] = longUnion.c[4];
  message[40] = longUnion.c[3];
  message[41] = longUnion.c[2];
  message[42] = longUnion.c[1];
  message[43] = longUnion.c[0];

  return EDGE_MEP_BuildAndSend_SequencedDataPacket(connection, message, EDGE_EXECUTED_MSG_LEN);
}

static int BuildAndSendFilled(t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  // TODO: need to modify
  return BuildAndSendMsgExecuted(connection, item, quantity, (int)price, "R", matchNum);
}

static int BuildAndSendMsgCancelled(t_Connection* connection, t_QueueItem* item, const int reason)
{
  char message[EDGE_CANCELLED_MSG_LEN];
  
  // Message Type "C"
  message[0] = EDGE_CANCELLED_MSG_TYPE;
  // Timestamp
  long m_time = __builtin_bswap64(GetLocalTime_usec());
  memcpy(&message[1], &m_time, 8);
  // Order Token
  strncpy(&message[9], item->orderEntry.clOrdID, 14);
  // Decremented Quantity
  int m_cumShare = __builtin_bswap32(item->cumShare);
  memcpy(&message[23], &m_cumShare, 4);
  // Cancelled reason
  message[27] = (char) reason;
  return EDGE_MEP_BuildAndSend_SequencedDataPacket(connection, message, EDGE_CANCELLED_MSG_LEN);
}

static int BuildAndSendCancelled(t_Connection* connection, t_QueueItem* item)
{
  return BuildAndSendMsgCancelled(connection, item, 'S');
}

static int HandleMsgCancelOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_QueueItem item;
  memset(&item, 0, sizeof(t_QueueItem));
  memcpy(item.orderEntry.clOrdID, &msg[1], 14);
  
  int res = SearchAndRemoveToken(&connection->orderQueue, &item);
  if(res == SUCCESS)
  {
    int quantity = *(unsigned int*)(&msg[15]);
    if (quantity == 0)
      BuildAndSendMsgCancelled(connection, &item, 'U'); // User requested cancel
    else // TODO: should be updated instead of removing
      BuildAndSendMsgCancelled(connection, &item, 'U');
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "HandleMsgCancelOrder: Not found order(%s), errorCode(%d)\n", item.orderEntry.clOrdID, res);
    BuildAndSendMsgReject(connection, &item.orderEntry, 'I', 'X', 1); // Order Not Found
  }
  
  return SUCCESS;
}

static int HandleMsgNewOrder(t_Connection* connection, const char* msg, const int msgLen, const int isExtended)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
   
  // Order Token
  memcpy(orderEntry.clOrdID, &msg[1], 14);
  // Buy/Sell Indicator
  orderEntry.side =  msg[15];
  // Shares
  orderEntry.quantity = __builtin_bswap32(*(unsigned int*)(&msg[16]));
  // Stock Symbol
  memcpy(orderEntry.symbol, &msg[20], 6);
  // The price of the order
  orderEntry.price = __builtin_bswap32(*(unsigned int*)(&msg[26]));
  // Time In Force
  orderEntry.tif = msg[30];
  // Display
  orderEntry.displayIndicator = msg[31];
  // Special Order Type 
  orderEntry.orderType = msg[32];
  // Extended Hrs Eligible
  orderEntry.extendedHrsEligible = msg[33];
  // Capacity
  orderEntry.capacity = msg[34];
  // Route out Eligibility 
  orderEntry.routeOutEligibility = msg[35];
  // Inter-market Sweep (ISO) Eligibility 
  orderEntry.interMarketSweep = msg[36];
  
  if (isExtended)
  {
    // Routing Delivery Method 
    orderEntry.routingDeliveryMethod = msg[37];
    // Route Strategy
    orderEntry.routeStrategy = ushortAt((unsigned char*)msg, 38);
    // Minimum Quantity 
    orderEntry.minQty = __builtin_bswap32(*(unsigned int*)(&msg[40]));
    // Max Floor
    orderEntry.maxFloor = __builtin_bswap32(*(unsigned int*)(&msg[44]));
    // Peg Difference
    orderEntry.pegDifference = msg[48];
    // Discretionary Offset 
    orderEntry.discretionAmount = msg[49];
    // Expire Time 
    orderEntry.expireTime = __builtin_bswap64(*(unsigned long*)(&msg[50]));
    // Symbol Suffix
    memcpy(orderEntry.symbolSuffix, &msg[58], 6);
  }

  if(orderEntry.tif != EDGE_TIF_IOC && orderEntry.tif != EDGE_TIF_DAY)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    BuildAndSendMsgReject(connection, &orderEntry, 'E', 'O', isExtended); // Exchange option
  }
  else if(orderEntry.side != EDGE_SIDE_BUY && orderEntry.side != EDGE_SIDE_SELL && orderEntry.side != EDGE_SIDE_SELL_SHORT)
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    BuildAndSendMsgReject(connection, &orderEntry, 'E', 'O', isExtended); // Exchange option
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    BuildAndSendMsgReject(connection, &orderEntry, 'Q', 'O', isExtended); // Invalid quantity
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "EDGE New order: symbol(%.6s), clOrdID(%.8s), quantity(%d), price(%.5f) side(%c) tif(%d)\n", 
                    orderEntry.symbol, orderEntry.clOrdID, orderEntry.quantity, 
                    orderEntry.price / 100000.0, orderEntry.side, orderEntry.tif);
               
    BuildAndSendMsgAccepted(connection, &orderEntry, isExtended);
    
    // Token
    t_QueueItem item;
    memset(&item, 0, sizeof(t_QueueItem));
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '5';
    item.cumShare = 0;
    item.totalPx = 0;
    
    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, EDGE_SIDE_BUY, &BuildAndSendFilled);
    //IOC Random
    if (orderEntry.tif == EDGE_TIF_IOC)
      if (hasCross) {
        if (item.orderEntry.quantity > item.cumShare)
          BuildAndSendCancelled(connection, &item); 
      } else {
        Simulator_Handle_RandomIOC(connection, &item, &BuildAndSendFilled, &BuildAndSendCancelled);
      }
    else {// DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
    }
  }
  return SUCCESS;
}

static int HandleUnsequencedDataPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  char* msg = &dataBlock->msgContent[EDGE_MEP_PAYLOAD_OFFSET];
  int msgLen = dataBlock->msgLen - EDGE_MEP_PAYLOAD_OFFSET;
  
  switch(msg[0])
  {
    case EDGE_ENTER_ORDER_MSG_TYPE:
      return HandleMsgNewOrder(connection, msg, msgLen, 0);
      break;
    
    case EDGE_EXTENDED_ENTER_ORDER_MSG_TYPE:
      return HandleMsgNewOrder(connection, msg, msgLen, 1);
      break;
    
    case EDGE_CANCEL_MSG_TYPE:
      return HandleMsgCancelOrder(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "Unknown message type '%c'\n", msg[0]);
      break;
  }
  
  return SUCCESS;
}

static void* DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &BuildAndSendFilled, &BuildAndSendCancelled);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_EDGE(t_Connection* connection, t_DataBlock *dataBlock)
{
  switch(dataBlock->msgContent[2])
  {
    case EDGE_MEP_LOGIN_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received login packet from client\n");
      if(EDGE_MEP_Handle_LogonPacket(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, EDGE_MEP_HeartbeatThread, (void*)connection);
      }
      break;
    
    case EDGE_MEP_CLIENT_HEARTBEAT_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received heartbeat packet from client\n");
      break;
    
    case EDGE_MEP_LOGOUT_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logout packet from client\n");
      EDGE_MEP_Handle_LogoutPacket(connection, dataBlock);
      break;
    
    case EDGE_MEP_UNSEQUENCED_DATA_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received unsequenced data packet from client\n");
      HandleUnsequencedDataPacket(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown packet type '%c'\n", dataBlock->msgContent[2]);
      break;
  }
  return SUCCESS;
}

