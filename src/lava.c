#include "lava.h"


static int LFBOE_GetNewOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
   // Order Token
  memcpy(orderEntry->clOrdID, &msg[3], 14);  
  // Buy/Sell Indicator
  orderEntry->side =  msg[17];  
  // Shares
  memcpy(&orderEntry->quantity, &msg[18], 4);  
  // Stock Symbol
  memcpy(orderEntry->symbol, &msg[22], 8);
  // The price of the order
  memcpy(&orderEntry->price, &msg[30], 4);
  // Time In Force
  memcpy(&orderEntry->tif, &msg[34], 4);  
  // Firm (Account)
  memcpy(orderEntry->clientId, &msg[38], 8);  
  // Order Capacity
  orderEntry->capacity = msg[46];  
  // Order Flags Integer Bit Mask
  memcpy(&orderEntry->randomReserve, &msg[47], 4);  
  // Min Qty
  memcpy(&orderEntry->minQty, &msg[51], 4);  
  // Max Floor
  memcpy(&orderEntry->maxFloor, &msg[55], 4);
  // Difference A peg offset as entered. 
  memcpy(&orderEntry->pegDifference, &msg[59], 2);  
  // Short Sale Exemption Reason as entered. 
  orderEntry->orderType = msg[61];
  
  return SUCCESS;
}

static int LFBOE_BuildAndSend_MsgReject(t_Connection* connection, t_OrderEntry* orderEntry, const short reason)
{
  char message[LFBOE_REJECTED_MSG_LEN];
  short m_protocol = LFBOE_PROTOCOL_CODE;
  long m_time = GetLocalTime_usec();
  
  // Protocol
  memcpy(&message[0], &m_protocol, 2);  
  // Message Type "J" Rejected message 
  message[2] = LFBOE_REJECTED_MSG_TYPE;  
  // Timestamp
  memcpy(&message[3], &m_time, 8);  
  // Order Token
  memcpy(&message[11], orderEntry->clOrdID, 14);  
  // Reject reason
  memcpy(&message[25], &reason, 2);
  
  return LFBOE_BuildAndSend_SequencedDataPacket(connection, message, LFBOE_REJECTED_MSG_LEN);
}

static int LFBOE_BuildAndSend_MsgAccepted(t_Connection* connection, t_OrderEntry* orderEntry)
{
  char message[LFBOE_ACCEPTED_MSG_LEN];
  short m_protocol = LFBOE_PROTOCOL_CODE;
  long m_time = GetLocalTime_usec();
  
  // Protocol  
  memcpy(&message[0], &m_protocol, 2);  
  // Message Type "A" Accepted message
  message[2] = LFBOE_ACCEPTED_MSG_TYPE;  
  // Timestamp
  memcpy(&message[3], &m_time, 8);  
  // The order Token field as entered.
  memcpy(&message[11], orderEntry->clOrdID, 14);
  // Buy/Sell Indicator as entered.
  message[25] = orderEntry->side;
  // Total number of shares accepted.
  memcpy(&message[26], &orderEntry->quantity, 4);
  // Stock Symbol 30 8 Alpha Stock Symbol as entered.
  memcpy(&message[30], orderEntry->symbol, 8);
  // Price
  memcpy(&message[38], &orderEntry->price, 4);
  // Time In Force
  memcpy(&message[42], &orderEntry->tif, 4);
  // Firm (Account)
  memcpy(&message[46], orderEntry->clientId, 8);
  // Order Capacity
  message[54] = orderEntry->capacity;  
  // Order Flags Integer Bit Mask  
  memcpy(&message[55], &orderEntry->randomReserve, 4);  
  // Min Qty
  memcpy(&message[59], &orderEntry->minQty, 4);  
  // Max Floor
  memcpy(&message[63], &orderEntry->maxFloor, 4);
  // Difference A peg offset as entered.
  memcpy(&message[67], &orderEntry->pegDifference, 2);  
  // Short Sale Exemption Reason as entered.
  message[69] = orderEntry->orderType;  
  // Order Reference
  memcpy(&message[70], &orderEntry->orderID, 8);

  return LFBOE_BuildAndSend_SequencedDataPacket(connection, message, LFBOE_ACCEPTED_MSG_LEN);
}

static int LFBOE_BuildAndSend_MsgExecuted(t_Connection* connection, t_QueueItem* item,
      const char liqidFlag, const int quantity, const int price, const long matchNum)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  const char executionFlag = (item->orderEntry.quantity == item->cumShare) ? LFBOE_EXECUTION_FLAGS_FILL : LFBOE_EXECUTION_FLAGS_PARTIAL;
  int bid = price;
  int offer = (int)item->orderEntry.price;
  if (item->orderEntry.side == LFBOE_SIDE_BUY)
  {
    bid = (int)item->orderEntry.price;
    offer = price;
  }
  
  char message[LFBOE_EXECUTED_MSG_LEN];
  short m_protocol = LFBOE_PROTOCOL_CODE;
  long m_time = GetLocalTime_usec();

  // Protocol 
  memcpy(&message[0], &m_protocol, 2);  
  // Message Type "E" Rejected message 
  message[2] = LFBOE_EXECUTED_MSG_TYPE;
  // Timestamp
  memcpy(&message[3], &m_time, 8);  
  // Order Token
  memcpy(&message[11], item->orderEntry.clOrdID, 14);
  // Incremental number of shares executed
  memcpy(&message[25], &quantity, 4);
  // The price at which these shares were executed
  memcpy(&message[29], &price, 4);
  // Execution Flags Integer Bit Mask 
  message[33] = executionFlag;
  // Liquidity Flag
  message[34] = liqidFlag;
  // Match Number
  memcpy(&message[35], &matchNum, 8);
  // Bid price at the time of execution
  memcpy(&message[43], &bid, 4);
  // Offer price at the time of execution
  memcpy(&message[47], &offer, 4);
  
  return LFBOE_BuildAndSend_SequencedDataPacket(connection, message, LFBOE_EXECUTED_MSG_LEN);
}

static int LFBOE_BuildAndSend_MsgCancelled(t_Connection* connection, t_QueueItem* item, const short reason)
{
  char message[LFBOE_CANCELED_MSG_LEN];
  short m_protocol = LFBOE_PROTOCOL_CODE;
  long m_time = GetLocalTime_usec();
  
  // Protocol
  memcpy(&message[0], &m_protocol, 2);  
  // Message Type "C"
  message[2] = LFBOE_CANCELED_MSG_TYPE;  
  // Timestamp
  memcpy(&message[3], &m_time, 8);  
  // Order Token
  memcpy(&message[11], item->orderEntry.clOrdID, 14);  
  // Cancel reason
  memcpy(&message[25], &reason, 2);
  
  return LFBOE_BuildAndSend_SequencedDataPacket(connection, message, LFBOE_CANCELED_MSG_LEN);
}

static int LFBOE_BuildAndSend_MsgCancelReject(t_Connection* connection, t_OrderEntry *orderEntry, const short reason)
{
  char message[LFBOE_CANCEL_REJECT_MSG_LEN];
  short m_protocol = LFBOE_PROTOCOL_CODE;
  long m_time = GetLocalTime_usec();
  
  // Protocol
  memcpy(&message[0], &m_protocol, 2);  
  // Message Type "J" Rejected message 
  message[2] = LFBOE_CANCEL_REJECT_MSG_TYPE;  
  // Timestamp
  memcpy(&message[3], &m_time, 8);  
  // Order Token
  memcpy(&message[11], orderEntry->clOrdID, 14);  
  // Reject reason
  memcpy(&message[25], &reason, 2);
  
  return LFBOE_BuildAndSend_SequencedDataPacket(connection, message, LFBOE_CANCEL_REJECT_MSG_LEN);
}

static int LFBOE_Callback_Filled (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  return LFBOE_BuildAndSend_MsgExecuted(connection, item, LFBOE_LIQUIDITY_FLAG_ADDED, quantity, (int)price, matchNum);
}

static int LFBOE_Callback_SystemCancel (t_Connection* connection, t_QueueItem* item)
{
  return LFBOE_BuildAndSend_MsgCancelled(connection, item, 2000); // 2000  Canceled by System 
}

static int LFBOE_Callback_UserCancel (t_Connection* connection, t_QueueItem* item,  const int quantity)
{
  return LFBOE_BuildAndSend_MsgCancelled(connection, item, 1000); // 1000  Canceled by User
}

static int LFBOE_Callback_CancelReject (t_Connection* connection, t_OrderEntry *orderEntry)
{
  return LFBOE_BuildAndSend_MsgCancelReject(connection, orderEntry, 1135); // 1135  LFBOE Order Token is unknown 
}

static int LFBOE_CheckNewOrderEntry (t_Connection* connection, t_OrderEntry* orderEntry)
{
  if(orderEntry->tif != LFBOE_TIF_IOC && orderEntry->tif != LFBOE_TIF_DAY)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    LFBOE_BuildAndSend_MsgReject(connection, orderEntry, 1077); // Invalid time in force 
    return ERROR;
  }
  
  if(orderEntry->side != 'B' && orderEntry->side != 'S' && orderEntry->side != 'T')
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    LFBOE_BuildAndSend_MsgReject(connection, orderEntry, 1003); // Invalid Side
    return ERROR;
  }
  
  if((orderEntry->quantity < 1) || (orderEntry->quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    LFBOE_BuildAndSend_MsgReject(connection, orderEntry, 1082); // Order quantity missing/invalid
    return ERROR;
  }
  
  return SUCCESS;
}

static int LFBOE_Handle_MsgCancelOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  memcpy(orderEntry.clOrdID, &msg[3], 14);
  
  return Simulator_Handle_CancelOrder(connection, &orderEntry, &LFBOE_Callback_UserCancel, &LFBOE_Callback_CancelReject);
}

static int LFBOE_Handle_MsgNewOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  LFBOE_GetNewOrderEntry(&orderEntry, msg, msgLen);
  
  if(LFBOE_CheckNewOrderEntry(connection, &orderEntry) == SUCCESS)
  {
    LFBOE_BuildAndSend_MsgAccepted(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;
      
    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, LFBOE_SIDE_BUY, &LFBOE_Callback_Filled);
    //IOC Random
    if (orderEntry.tif == LFBOE_TIF_IOC)
      if (hasCross)
      {
        if (item.orderEntry.quantity > item.cumShare)
          LFBOE_BuildAndSend_MsgCancelled(connection, &item, 0); // 0  Canceled by ME 
      } 
      else
        Simulator_Handle_RandomIOC(connection, &item, &LFBOE_Callback_Filled, &LFBOE_Callback_SystemCancel);
    else  // DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

int LFBOE_Handle_UnsequencedDataPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  char* msg;
  int msgLen;
  
  msg = &dataBlock->msgContent[LFBOE_PAYLOAD_OFFSET];
  msgLen = dataBlock->msgLen - LFBOE_PAYLOAD_OFFSET;
  
  // Protocol
  t_ShortConverter shortUnion;
  memcpy(shortUnion.c, msg, 2);
  if (shortUnion.value != LFBOE_PROTOCOL_CODE)
  {
    TraceLog(ERROR_LEVEL, "Invalid protocol\n");
    return ERROR;
  }

  switch(msg[2])
  {
    case LFBOE_ENTER_ORDER_MSG_TYPE:
      return LFBOE_Handle_MsgNewOrder(connection, msg, msgLen);
      break;
    
    case LFBOE_REPLACE_MSG_TYPE:
      TraceLog(DEBUG_LEVEL, "Handle replace message'\n");
      break;
    
    case LFBOE_CANCEL_MSG_TYPE:
      return LFBOE_Handle_MsgCancelOrder(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "Unknown message type '%c'\n", msg[2]);
      break;
  }
  
  return SUCCESS;
}

static void* LFBOE_DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;

  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &LFBOE_Callback_Filled, &LFBOE_Callback_SystemCancel);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_LFBOE(t_Connection* connection, t_DataBlock *dataBlock)
{
  switch(dataBlock->msgContent[2])
  {
    case LFBOE_LOGIN_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received login packet from client\n");
      if(LFBOE_Handle_LogonPacket(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, LFBOE_DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, LFBOE_HeartbeatThread, (void*)connection);
      }
      break;
    
    case LFBOE_CLIENT_HEARTBEAT_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received heartbeat packet from client\n");
      break;
    
    case LFBOE_LOGOUT_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logout packet from client\n");
      LFBOE_Handle_LogoutPacket(connection, dataBlock);
      break;
      
    case LFBOE_UNSEQUENCED_DATA_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received unsequenced data packet from client\n");
      LFBOE_Handle_UnsequencedDataPacket(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown packet type '%c'\n", dataBlock->msgContent[2]);
      break;
  }
  return SUCCESS;
}

