/****************************************************************************
** Project name:    US Blink Simulator
** File name:       edge.h
** Description:     N/A
** Specification:   Edge XPRS Specifications (High Performance API) 
**                  Version 1.31
**                  November 25, 2013
**
** Author:          Thien Nguyen-Tai
** First created:   May 06, 2014
** Last updated:    May 08, 2014
****************************************************************************/

#ifndef _EDGE_
#define _EDGE_

#include "global.h"
#include "soupbintcp.h"


// MEP Messages Type
#define EDGE_MEP_PAYLOAD_OFFSET                         SOUPBINTCP_PAYLOAD_OFFSET
#define EDGE_MEP_LOGIN_REQUEST_PACKET_TYPE              SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE
#define EDGE_MEP_CLIENT_HEARTBEAT_PACKET_TYPE           SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE
#define EDGE_MEP_LOGOUT_REQUEST_PACKET_TYPE             SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE
#define EDGE_MEP_UNSEQUENCED_DATA_PACKET_TYPE           SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE
// MEP Handle Messages
#define EDGE_MEP_HeartbeatThread                        SOUPBINTCP_HeartbeatThread
#define EDGE_MEP_Handle_LogonPacket                     SOUPBINTCP_Handle_LogonPacket
#define EDGE_MEP_Handle_LogoutPacket                    SOUPBINTCP_Handle_LogoutPacket

// MEP Messages
#define EDGE_MEP_BuildAndSend_SequencedDataPacket       SOUPBINTCP_BuildAndSend_SequencedDataPacket
#define EDGE_MEP_BuildAndSend_LoginPacket               SOUPBINTCP_BuildAndSend_LoginRequestPacket
#define EDGE_MEP_BuildAndSend_HeartbeatPacket           SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket
#define EDGE_MEP_BuildAndSend_LogoutPacket              SOUPBINTCP_BuildAndSend_LogoutRequestPacket

#define EDGE_CANCEL_MSG_LEN                  19
#define EDGE_ACCEPTED_MSG_LEN                53
#define EDGE_EXTENDED_ACCEPTED_MSG_LEN       80
#define EDGE_EXECUTED_MSG_LEN                44
#define EDGE_REJECTED_MSG_LEN                24
#define EDGE_EXTENDED_REJECTED_MSG_LEN       25
#define EDGE_CANCELLED_MSG_LEN               28

#define EDGE_ENTER_ORDER_MSG_TYPE            'O'
#define EDGE_EXTENDED_ENTER_ORDER_MSG_TYPE   'N'
#define EDGE_CANCEL_MSG_TYPE                 'X'
#define EDGE_ACCEPTED_MSG_TYPE               'A'
#define EDGE_EXTENDED_ACCEPTED_MSG_TYPE      'P'
#define EDGE_EXECUTED_MSG_TYPE               'E'
#define EDGE_REJECTED_MSG_TYPE               'J'
#define EDGE_EXTENDED_REJECTED_MSG_TYPE      'L'
#define EDGE_CANCELLED_MSG_TYPE              'C'

#define EDGE_TIF_IOC                          0
#define EDGE_TIF_DAY                          1

#define EDGE_SIDE_BUY                         'B'
#define EDGE_SIDE_SELL                        'S'
#define EDGE_SIDE_SELL_SHORT                  'T'
#define EDGE_SIDE_SELL_SHORT_EXEMPT           'E'

/****************************************************************************
- Function name:    HandleMsg_EDGE
- Input:            'connection': the connection; 'dataBlock': the receive data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Handle Edge XPRS
- Usage:            N/A
****************************************************************************/
int HandleMsg_EDGE(t_Connection* client, t_DataBlock *dataBlock);

#endif //_EDGE_
