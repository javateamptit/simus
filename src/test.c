#include "sim.h"
#include "global.h"
#include "network.h"


static int testExit = 0;

int (*logon) (t_Connection* connection, const char* session, const char* user, const char* password);
int (*logout) (t_Connection* connection);
int (*heartbeat) (t_Connection* connection);
int (*order) (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
int (*cancel) (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
void (*printInfo) (t_Connection* connection, t_DataBlock* data);

static void TEST_initial (const int venue)
{
  logon = NULL;
  logout = NULL;
  heartbeat = NULL;
  order = NULL;
  cancel = NULL;
  printInfo = NULL;
  
  if (venue == VENUE_BYX)
  {
    int BuildAndSend_BATS_BOE_LoginRequestMsg(t_Connection* connection, const char* sessionSubId, const char* userName, const char* password);
    int BuildAndSend_BATS_BOE_LogoutRequestMsg(t_Connection* connection);
    int BuildAndSend_BATS_BOE_Client_HeartbeatMsg(t_Connection* connection);
    int BuildAndSend_BATS_BOE_Order(t_Connection* connection, char* clOrdId, char* symbol, char* s, char* q, char* p, char* t);
    int BuildAndSend_BATS_BOE_Cancel(t_Connection* connection, char* origClOrdId, char* symbol, char* s, char* q);
    void TEST_BATS_printInfo (t_Connection* connection, t_DataBlock* data);

    logon = &BuildAndSend_BATS_BOE_LoginRequestMsg;
    logout = &BuildAndSend_BATS_BOE_LogoutRequestMsg;
    heartbeat = &BuildAndSend_BATS_BOE_Client_HeartbeatMsg;
    order = &BuildAndSend_BATS_BOE_Order;
    cancel = &BuildAndSend_BATS_BOE_Cancel;
    printInfo = &TEST_BATS_printInfo;
    
    return;
  }
  else if (venue == VENUE_PSX || venue == VENUE_OUCH || venue == VENUE_BX)
  {
    int TEST_PSX_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_PSX_logout (t_Connection* connection);
    int TEST_PSX_heartbeat (t_Connection* connection);
    int TEST_PSX_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_PSX_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_PSX_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_PSX_logon;
    logout = &TEST_PSX_logout;
    heartbeat = &TEST_PSX_heartbeat;
    order = &TEST_PSX_order;
    cancel = &TEST_PSX_cancel;
    printInfo = &TEST_PSX_printInfo;
    
    return;
  }
  else if (venue == VENUE_LAVA)
  {
    int TEST_LAVA_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_LAVA_logout (t_Connection* connection);
    int TEST_LAVA_heartbeat (t_Connection* connection);
    int TEST_LAVA_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_LAVA_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_LAVA_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_LAVA_logon;
    logout = &TEST_LAVA_logout;
    heartbeat = &TEST_LAVA_heartbeat;
    order = &TEST_LAVA_order;
    cancel = &TEST_LAVA_cancel;
    printInfo = &TEST_LAVA_printInfo;
    
    return;
  }
  else if (venue == VENUE_ARCA)
  {
    int TEST_ARCA_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_ARCA_logout (t_Connection* connection);
    int TEST_ARCA_heartbeat (t_Connection* connection);
    int TEST_ARCA_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_ARCA_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_ARCA_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_ARCA_logon;
    logout = &TEST_ARCA_logout;
    heartbeat = &TEST_ARCA_heartbeat;
    order = &TEST_ARCA_order;
    cancel = &TEST_ARCA_cancel;
    printInfo = &TEST_ARCA_printInfo;
    
    return;
  }
  else if (venue == VENUE_EDGX || venue == VENUE_EDGA)
  {
    int TEST_EDGE_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_EDGE_logout (t_Connection* connection);
    int TEST_EDGE_heartbeat (t_Connection* connection);
    int TEST_EDGE_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_EDGE_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_EDGE_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_EDGE_logon;
    logout = &TEST_EDGE_logout;
    heartbeat = &TEST_EDGE_heartbeat;
    order = &TEST_EDGE_order;
    cancel = &TEST_EDGE_cancel;
    printInfo = &TEST_EDGE_printInfo;
    
    return;
  }
  else if (venue == VENUE_RASH)
  {
    int TEST_RASH_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_RASH_logout (t_Connection* connection);
    int TEST_RASH_heartbeat (t_Connection* connection);
    int TEST_RASH_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_RASH_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_RASH_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_RASH_logon;
    logout = &TEST_RASH_logout;
    heartbeat = &TEST_RASH_heartbeat;
    order = &TEST_RASH_order;
    cancel = &TEST_RASH_cancel;
    printInfo = &TEST_RASH_printInfo;
    
    return;
  }
  else if (venue == VENUE_NYSE)
  {
    int TEST_NYSE_logon (t_Connection* connection, const char* session, const char* user, const char* password);
    int TEST_NYSE_logout (t_Connection* connection);
    int TEST_NYSE_heartbeat (t_Connection* connection);
    int TEST_NYSE_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf);
    int TEST_NYSE_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty);
    void TEST_NYSE_printInfo (t_Connection* connection, t_DataBlock* data);
    
    logon = &TEST_NYSE_logon;
    logout = &TEST_NYSE_logout;
    heartbeat = &TEST_NYSE_heartbeat;
    order = &TEST_NYSE_order;
    cancel = &TEST_NYSE_cancel;
    printInfo = &TEST_NYSE_printInfo;
    
    return;
  }
}
static void TEST_Handle_CommandLine(t_Connection* connection, const char* command)
{
  t_Parameters para;
  memset(&para, 0, sizeof(t_Parameters));
  
  Lrc_Split(command, ' ', &para);
  // TraceLog(DEBUG_LEVEL, "*** check parameters = %s; %s; %d\n", para->parameterList[i], para.parameterList[0], para.countParameters);
  
  if (strcmp(para.parameterList[0], "-exit") == 0 || strcmp(para.parameterList[0], "exit") == 0)
  {
    testExit = 1;
    connection->isAlive = 0;
    return;
  }
  
  if (strcmp(para.parameterList[0], "-quit") == 0 || strcmp(para.parameterList[0], "quit") == 0)
  {
    testExit = 1;
    connection->isAlive = 0;
    return;
  }
  
  if (logon == NULL || logout == NULL || heartbeat == NULL /* || order == NULL || cancel == NULL */)
  {
     TraceLog(DEBUG_LEVEL, "\tThe venue <%s> isn't yet supported to test\n", VenueName[connection->configInfo->venue]);
     return;
  }
  
  if (strcmp(para.parameterList[0], "-logon") == 0 || strcmp(para.parameterList[0], "logon") == 0)
  {
    // check logon
    if (para.countParameters < 4)
    {
      TraceLog(ERROR_LEVEL, "*** Usage: -logon <session> <user> <password>\n");
      return;
    }
    
    //send logon
    logon(connection, para.parameterList[1], para.parameterList[2], para.parameterList[3]);
    return;
  }
  
  if (strcmp(para.parameterList[0], "-logout") == 0 || strcmp(para.parameterList[0], "logout") == 0)
  {
    //send logout
    logout(connection);
    
    connection->isAlive = 0;
    return;
  }
  
  if (strcmp(para.parameterList[0], "-heartbeat") == 0 || strcmp(para.parameterList[0], "heartbeat") == 0)
  {
    //handle heartbeat
    heartbeat(connection);
    return;
  }

  if (strcmp(para.parameterList[0], "-order") == 0 || strcmp(para.parameterList[0], "order") == 0)
  {
    //handle new order
    if (para.countParameters < 7)
    {
      TraceLog(ERROR_LEVEL, "*** Usage: -order <token> <symbol> <side> <quantity> <price> <tif>\n");
      return;
    }
    
    order(connection, para.parameterList[1], para.parameterList[2], para.parameterList[3], para.parameterList[4], para.parameterList[5], para.parameterList[6]);
    return;
  }
  
  if (strcmp(para.parameterList[0], "-cancel") == 0 || strcmp(para.parameterList[0], "cancel") == 0)
  {
    //handle cancel order
    if (para.countParameters < 5)
    {
      TraceLog(ERROR_LEVEL, "*** Usage: -cancel <token> <symbol> <side> <quantity>\n");
      return;
    }

    cancel(connection, para.parameterList[1], para.parameterList[2], para.parameterList[3], para.parameterList[4]);
    return;
  }
  
  TraceLog(ERROR_LEVEL, "*** Unsupported command\n");
}

void* TEST_Start_TestThread (void* data)
{
  char cmdLine[128] = "\0";
  t_Connection* connection = (t_Connection*) data;
  
  while (connection->isAlive)
  {
    TraceLog(DEBUG_LEVEL, "\tGet command line >>\t");
    fgets(cmdLine, 128, stdin);
    
    TEST_Handle_CommandLine(connection, cmdLine);
      
    usleep(1000);
  }  

  return NULL;
}

void *TEST_Start_DebugThread (void* data)
{
  t_Connection *connection = (t_Connection*) data;
  
  while(connection->isAlive)
  {
    t_DataBlock dataBlock;
    int len = sizeof(struct sockaddr_in);
    int result;
    
    if (connection->configInfo->type == SOCK_STREAM)
    {
      if(connection->configInfo->venue == VENUE_RASH)
      {
        result = ReceiveMsg_RASH(connection, &dataBlock);
      }
      else  result = ReceiveMsg(connection, &dataBlock);
    }
    else
      result = ReceiveMsgFrom(connection->socket, &dataBlock, (struct sockaddr*)&connection->clientAddr, &len);

    if(result == ERROR)
    {
      TraceLog(ERROR_LEVEL, "TEST_Start_DebugThread: receive msg failed\n");
      break;
    }
    
    if (printInfo)
      printInfo(connection, &dataBlock);
      
    usleep(1000);
  }
 
  return NULL;
}

void TEST_TCP(const int configIdx, struct sockaddr_in* server_addr)
{
  testExit = 0;
  while (!testExit)
  {
    int testSocket = socket(AF_INET, Configs.portList[configIdx].type, 0);
    if (testSocket == -1)
    {
      TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
      break;
    }

    if(connect(testSocket, (struct sockaddr*) server_addr, sizeof(struct sockaddr_in)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Cannot connect to server\n");
      break;
    }
  
    t_UserInfo userInfo;
    t_Connection connection;

    strcpy(userInfo.userName, "_SIM");
    userInfo.outgoingSeq = 0;

    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 0;
    memcpy(&connection.clientAddr, &server_addr, sizeof(struct sockaddr_in));
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);

    pthread_t testThread;
    pthread_create(&testThread, NULL, TEST_Start_TestThread, &connection);
    pthread_t dbgThread;
    pthread_create(&dbgThread, NULL, TEST_Start_DebugThread, &connection);

    pthread_join(testThread, NULL);

    close(testSocket);
  }
}

void TEST_UCP(const int configIdx, struct sockaddr_in* server_addr)
{
  int testSocket = socket(AF_INET, Configs.portList[configIdx].type, 0);
  if (testSocket == -1)
  {
    TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
    return;
  }
  
  testExit = 0;
  while (!testExit)
  {
    t_UserInfo userInfo;
    t_Connection connection;

    strcpy(userInfo.userName, "_SIM");
    userInfo.outgoingSeq = 0;

    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 0;
    memcpy(&connection.clientAddr, server_addr, sizeof(struct sockaddr_in));
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);

    pthread_t testThread;
    pthread_create(&testThread, NULL, TEST_Start_TestThread, &connection);
    pthread_t dbgThread;
    pthread_create(&dbgThread, NULL, TEST_Start_DebugThread, &connection);

    pthread_join(testThread, NULL);
  }
  
  close(testSocket);
}

void TEST_Run ()
{
  char activeVenue[128] = "\0";
  char cmdLine[128] = "\0";
  int i, configIdx;
  
  for (configIdx = 0, i = 0; configIdx < Configs.countPort; configIdx++)
  {
    i += sprintf(&activeVenue[i], "<%s> ", VenueName[Configs.portList[configIdx].venue]);
  }

  TraceLog(DEBUG_LEVEL, "\tTHIS IS THREAD FOR SIMULATOR TESTING\n");
  
  while (1)
  {
    TraceLog(DEBUG_LEVEL, "\tActived venue %s\n", activeVenue);
    TraceLog(DEBUG_LEVEL, "\tPlease ENTER your VENUE to test >>\t");
    fgets(cmdLine, 128, stdin);
    
    for (configIdx = 0; configIdx < Configs.countPort; configIdx++)
    {
      if(strncmp(cmdLine, VenueName[Configs.portList[configIdx].venue], strlen(VenueName[Configs.portList[configIdx].venue])) == 0)
      {
        TraceLog(DEBUG_LEVEL, "\tYou are testing <%s:%d>\n", VenueName[Configs.portList[configIdx].venue], Configs.portList[configIdx].port);
        break;
      }
    }
    
    if (configIdx == Configs.countPort)
    {
      TraceLog(ERROR_LEVEL, "\tYou enter invalid venue\n");
      continue;
    }
    
    TraceLog(DEBUG_LEVEL, "USAGE:\tEnter one of command lines below:\n"\
    
                          "      \t-logon <session> <user> <password>\n"\
                          
                          "      \t-logout\n"\
                          
                          "      \t-heartbeat\n"\
                          
                          "      \t-order <token> <symbol> <side> <quantity> <price> <tif>\n"\
                          "      \t  <token>: token string\n"\
                          "      \t  <symbol>: stock symbol string\n"\
                          "      \t  <side>: \"S\"(sell) or \"B\"(buy) or \"T\"(short sell)\n"\
                          "      \t  <quantity>: integer number of shares\n"\
                          "      \t  <price>: double price, e.g 12.34\n"\
                          "      \t  <tif>: \"IOC\" or \"DAY\"\n"\
                         
                          "      \t-cancel <token> <symbol> <side> <new quantity>\n"\
                          "      \t  <new quantity>: enter \"0\" to cancel all remain shares\n");
                          
    
    TEST_initial(Configs.portList[configIdx].venue);
    
    struct sockaddr_in server_addr;
    
    memset((char*) &server_addr, 0, sizeof(struct sockaddr_in));
    server_addr.sin_family = AF_INET;
    server_addr.sin_port = htons(Configs.portList[configIdx].port);
    if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) != 1)
    {
      TraceLog(ERROR_LEVEL, "Cannot convert net address\n");
      continue;
    }
    
    if (Configs.portList[configIdx].type == SOCK_STREAM)
      TEST_TCP(configIdx, &server_addr);
    else
      TEST_UCP(configIdx, &server_addr);
  }
}