#include "config.h"
#include "utility.h"
#include "global.h"
#include "sim.h"
#include "nyse.h"

/****************************************************************************
- Function name:	Convert_ClOrdID_To_NYSE_Format
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
void Convert_ClOrdID_To_NYSE_Format(int idOrder, char *strResult)
{
	/*
	 * A = 0, B = 1, C = 2, D = 3, E = 4, F = 5, G = 6, H = 7, I = 8, J = 9
	 * To avoid all zeros in the sequence number, we must plus 1
	 * For example: idOrder = 1234567 is converted to BCD 4691
	*/

	int seqNumber = idOrder % 9999;
	int branchCode = (idOrder -  seqNumber) / 9999;
	seqNumber = seqNumber + 1; // Avoid all zeros in the sequence number

	// branch code
	strResult[2] = 65 + branchCode % 10; // ASCII 'A' = 65
	branchCode = branchCode/10;
	strResult[1] = 65 + branchCode % 10;
	branchCode = branchCode/10;
	strResult[0] = 65 + branchCode;

	// space
	strResult[3] =  32; // ASCII ' ' = 32

	// sequence number
	strResult[7] = 48 + seqNumber % 10; // // ASCII '0' = 48
	seqNumber = seqNumber/10;
	strResult[6] = 48 + seqNumber % 10;
	seqNumber = seqNumber/10;
	strResult[5] = 48 + seqNumber % 10;
	seqNumber = seqNumber/10;
	strResult[4] = 48 + seqNumber;

	// add character '/'
	strResult[8] = 47;

	// add Stamptime
  time_t now;
  time(&now);
  struct tm local;
  localtime_r(&now, &local);
  char stamptime[8];
  char year[4];
  Lrc_itoa(local.tm_year + 1900, year);
  char month[2] ;
  __iToStrWithLeftPad(local.tm_mon + 1, '0', 2, month);
  char day[2] ;
  __iToStrWithLeftPad(local.tm_mday, '0', 2, day);
  sprintf(stamptime, "%.2s%.2s%.4s", month, day, year);
  
  // add Stamptime
	strncpy(&strResult[9], stamptime, 8);
}

/****************************************************************************
- Function name:	Convert_NYSECCG_Format_To_ClOrdID
- Input:
- Output:
- Return:
- Description:
- Usage:
****************************************************************************/
int Convert_NYSECCG_Format_To_ClOrdID(char *valueFiled)
{
	int branhCode = 0, seqNumber = 0;
	//We known 8 characters in advance

	// branch code
	branhCode = (valueFiled[0] - 65)* 100;
	branhCode += (valueFiled[1] - 65)* 10;
	branhCode += valueFiled[2] - 65;

	// skip space: valueFiled[3]

	// sequence number
	seqNumber += (valueFiled[4] - 48)* 1000;
	seqNumber += (valueFiled[5] - 48)* 100;
	seqNumber += (valueFiled[6] - 48)* 10;
	seqNumber += (valueFiled[7] - 48);

	// we must minus 1 because we have plus 1 in clientOrderID of Convert_ClOrdID_To_NYSE_Format
	seqNumber -= 1;

	return (branhCode * 9999 + seqNumber);
}

int TEST_NYSE_logon(t_Connection* connection, const char* session, const char* user, const char* password)
{
	 char msgContent[60];
  t_ShortConverter shortUnion;
  memset(msgContent, 0, NYSE_LOGON_TYPE_MSG_LEN);
  // Message Tye 'A'
  shortUnion.value = 0x0021;
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];
  
// Length
  shortUnion.value = NYSE_LOGON_TYPE_MSG_LEN;
  msgContent[2] = shortUnion.c[1];
  msgContent[3] = shortUnion.c[0];
  // SeqNum (4 bytes) => NOT USE
  // LastMsgSeqNumReceived (4 bytes)

  // SenderCompID (12 bytes)
  strncpy(&msgContent[12], "NYSE", 12);
  // MessageVersionProfile (32 bytes - binary)
  shortUnion.value = NYSE_LOGON_TYPE;
  msgContent[24] = shortUnion.c[1];
  msgContent[25] = shortUnion.c[0];
  
  shortUnion.value = NYSE_LOGON_REJECT_TYPE;
  msgContent[26] = shortUnion.c[1];
  msgContent[27] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_ACK_TYPE;
  msgContent[28] = shortUnion.c[1];
  msgContent[29] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_CANCELLED_TYPE;
  msgContent[30] = shortUnion.c[1];
  msgContent[31] = shortUnion.c[0];
  
  shortUnion.value = NYSE_CANCEL_REPLACE_TYPE;
  msgContent[32] = shortUnion.c[1];
  msgContent[33] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_REPLACED_TYPE;
  msgContent[34] = shortUnion.c[1];
  msgContent[35] = shortUnion.c[0];
  
  shortUnion.value = NYSE_CANCEL_ACK_TYPE;
  msgContent[36] = shortUnion.c[1];
  msgContent[37] = shortUnion.c[0];
  
  shortUnion.value = NYSE_BUST_CORRECT_TYPE;
  msgContent[38] = shortUnion.c[1];
  msgContent[39] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_FILLED_SHORT_TYPE;
  msgContent[40] = shortUnion.c[1];
  msgContent[41] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_REJECTED_TYPE;
  msgContent[42] = shortUnion.c[1];
  msgContent[43] = shortUnion.c[0];
  // 44 -> 55 => NOT USE
  // CancelOnDisconnect (1 byte - Alpha/Numeric)
  msgContent[56] = '0';
  // Filler (3 bytes) => NOT USE
  return SendMsg(connection->socket, &connection->lock, msgContent, NYSE_LOGON_TYPE_MSG_LEN);
}

int TEST_NYSE_logout(t_Connection* connection)
{
  connection->isAlive = 0;
  return 1;
}

int TEST_NYSE_heartbeat (t_Connection* connection)
{
  char msgContent[8];
  t_ShortConverter shortUnion;
  memset(msgContent, 0, 8);
  // Message Tye 'A'
  shortUnion.value = 0x0001;
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];
  
  // Length
  shortUnion.value = 8;
  msgContent[2] = shortUnion.c[1];
  msgContent[3] = shortUnion.c[0];
  
  // SeqNum (4 bytes) => NOT USE
  return SendMsg(connection->socket, &connection->lock, msgContent, 8); 

	
}

int TEST_NYSE_order(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
  char msgContent[84];
  memset(msgContent, 0, 84);
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;
  
  //Message Type 
  shortUnion.value = 0x0041;
  msgContent[0] = shortUnion.c[1];
  msgContent[1] = shortUnion.c[0];
  //Length
  shortUnion.value = 84;
  msgContent[2] = shortUnion.c[1];
  msgContent[3] = shortUnion.c[0];
  //Seq Number
  //Original Order Qty
  intUnion.value = atoi(qty);
  msgContent[8] = intUnion.c[3];
  msgContent[9] = intUnion.c[2];
  msgContent[10] = intUnion.c[1]; 
  msgContent[11] = intUnion.c[0];
  
	/*
	 * MaxFloorQty: 4 bytes(12-15)
	 * Currently, EAA will trade with TIF = IOC, so we NOT use MaxFloorQty
	*/

	// Price: 4 bytes(16-19)
	intUnion.value = (int)(atof(pr) * 100.00 + 0.500001); // To round to 2 digit
	msgContent[16] = intUnion.c[3];
	msgContent[17] = intUnion.c[2];
	msgContent[18] = intUnion.c[1];
	msgContent[19] = intUnion.c[0];
  // PriceScale: 1 byte
	msgContent[20] = '2';

	// Symbol: 11 bytes(21 - 31)
	strncpy(&msgContent[21], symbol, 11);

	// ExecInst: 1 byte --> Not use
	// msgContent[32]

	//Side : 1 byte
	msgContent[33] = *sd == 'B'? '1':'2';

	// OrderType: 1 byte
	msgContent[34] = '2';

	// TimeInForce: 1 byte
	msgContent[35] = strcmp(tf, "DAY")==0? '0' : '3';	//IOC

	// Rule80A: 1 byte
	msgContent[36] = 'P';

	// RoutingInstruction: 1 byte
	msgContent[37] = 'I';


	/*
	 * DOTReserve: 1 byte
	 * set DOTReserve value to 'N' if Maxfloor is not used, otherwise set to 'Y'
	 * currently, EAA will trade without Maxfloor, so We set DOTReserve value to 'N'
	*/
	msgContent[38] = 'N';

	// OnBehalfOfCompID: 5 bytes
	strncpy(&msgContent[39], "NYSE", 5);

	// SenderSubID: 5 bytes(44-48) --> Not use

	// ClearingFirm: 5 bytes(49-53) -> Not use

	//Account: 10 bytes(54-63)
	strcpy(&msgContent[54], "TEST");

	// ClientOrderID: 17 bytes(64-80)
	Convert_ClOrdID_To_NYSE_Format(atoi(token), &msgContent[64]);

	// Filler: 3 bytes(81-83) --> Not use

  return SendMsg(connection->socket, &connection->lock, msgContent, 84);
}

int TEST_NYSE_cancel(t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{

	t_ShortConverter shortUnion;

	char msgContent[92];
	memset(msgContent, 0, 92);

	// msgContent type: 2 bytes(0-1)
	shortUnion.value = 0x0061;
	msgContent[0] = shortUnion.c[1];
	msgContent[1] = shortUnion.c[0];

	// msgContent length: 2 bytes(2-3)
	shortUnion.value = 92;
	msgContent[2] = shortUnion.c[1];
	msgContent[3] = shortUnion.c[0];

	//MsgSeqNum: 4 bytes(4-7)   (this will be set right before the order is sent)

	//MEOrderID: 4 bytes(8-11) -> Not use
//	integerUnion.value = cancelOrder->MEOrderID;
//	msgContent[8] = integerUnion.c[3];
//	msgContent[9] = integerUnion.c[2];
//	msgContent[10] = integerUnion.c[1];
//	msgContent[11] = integerUnion.c[0];

	//OriginalOrderQty: 4 bytes(12 - 15)-> Not use
	//CancelQty: 4 bytes( 16 - 19)-> Not use
	//LeavesQty: 4bytes (20 - 23)-> Not use

	// Symbol: 11 bytes( 24 - 34)
	strncpy(&msgContent[24], symbol, 11);

	// Side: 1 byte
	msgContent[35] = *sd;

	//OnBehalfOfCompID: 5 bytes(36- 40)
	strncpy(&msgContent[36], "NYSE", 5);

	//SenderSubID:5 bytes( 41 - 45)-> Not use

	//Account: 10 bytes(46 - 55)
	strcpy(&msgContent[46], "TEST");

	//ClientOrderID: 17 bytes (56-72)
	strcpy(&msgContent[56], token);

	//OrigClientOrderID: 17 bytes(73-89)
	Convert_ClOrdID_To_NYSE_Format(atoi(token), &msgContent[73]);

	//Filler: 2 bytes (90 - 91)-> Not use	

  return SendMsg(connection->socket, &connection->lock, msgContent, 92);   
}

void TEST_NYSE_printInfo(t_Connection* connection, t_DataBlock* data)
{
  t_ShortConverter shortUnion;
  shortUnion.c[0] = data->msgContent[1];
  shortUnion.c[1] = data->msgContent[0];
  //TraceLog(DEBUG_LEVEL, "\n______Recevied Message ___");
  char clOrderID[17];
  int _clOrderID;
  int orderID;
  int leaveShare, lastShare, price, priceScale;
  switch(shortUnion.value)
  {
    case 0x0091:
      // Client Order Id ( 17 bytes)
      memcpy(clOrderID, &data->msgContent[36], 17);
      clOrderID[17] = '\0';
      _clOrderID = Convert_NYSECCG_Format_To_ClOrdID(clOrderID);
      orderID = __builtin_bswap32(*(unsigned int*)(&data->msgContent[8]));
      TraceLog(DEBUG_LEVEL, "[NYSE  Ack] OrderID:%d ClOrdID:%d \n", orderID, _clOrderID);
      break;
      
    case 0x0081:
      leaveShare = __builtin_bswap32(*(unsigned int*)(&data->msgContent[16]));
      lastShare = __builtin_bswap32(*(unsigned int*)(&data->msgContent[20]));
      price = __builtin_bswap32(*(unsigned int*)(&data->msgContent[24]));
      priceScale = data->msgContent[28] - 48;
      char side = data->msgContent[29];
      memcpy(clOrderID, &data->msgContent[99], 17);
      clOrderID[17] = '\0';
      _clOrderID = Convert_NYSECCG_Format_To_ClOrdID(clOrderID);
      TraceLog(DEBUG_LEVEL, "[NYSE  Fill] ClOrdID:%d Side:%s  Share:%d Price:%.4f LeavesQty:%d\n", _clOrderID, side=='1' ? "Buy" : "Sell", 
                  lastShare, price/pow(10.0, (double)priceScale), leaveShare);
      break;
      
    case 0x00D1:
      // Client Order Id ( 17 bytes)
      memcpy(clOrderID, &data->msgContent[37], 17);
      clOrderID[17] = '\0';
      _clOrderID = Convert_NYSECCG_Format_To_ClOrdID(clOrderID);
      orderID = __builtin_bswap32(*(unsigned int*)(&data->msgContent[8]));
      TraceLog(DEBUG_LEVEL, "[NYSE  Cancel] OrderID:%d ClOrdID:%d \n", orderID, _clOrderID);
      break;
      
    case 0x00F1:
      shortUnion.c[0] = data->msgContent[17];
      shortUnion.c[1] = data->msgContent[16];
      char RejectMsgType = data->msgContent[18];
      memcpy(clOrderID, &data->msgContent[39], 17);
      clOrderID[17] = '\0';
      _clOrderID = Convert_NYSECCG_Format_To_ClOrdID(clOrderID);
      char text[40];
      memcpy(text, &data->msgContent[73], 70);
      TraceLog(DEBUG_LEVEL, "[NYSE  Reject] ClOrdID:%d RejectNumber:%d RejectMsgType:%c Reason:%s\n", _clOrderID, shortUnion.value, 
                    RejectMsgType, RemoveCharacters(text, 40));
      break;
    default:
      break;
  }  
}
