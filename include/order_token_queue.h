#ifndef _ORDER_TOKEN_QUEUE
#define _ORDER_TOKEN_QUEUE

#include <stdio.h>
#include "utility.h"
#include <pthread.h>

#define QUEUE_BUFFER_SIZE    1024*10


typedef struct ORDERENTRY
{
  char clOrdID[20]; // Order Token 
  char side;
  int quantity;
  
  long orderID;  
  
  // BATS binary order entry
  char clearingFirm[5];  // OnBehalfOfCompID 
  char clearingAccount[5]; // OnBehalfOfSubID / OnBehalfOfCompID
  long price;
  char execInst;
  char orderType; // Special Order Type / Cross Type
  int tif;
  int minQty;
  int maxFloor;
  
  char symbol[11];
  char symbolSuffix[6];
  char capacity;
  char RoutingInst[4];
  
  char clientId[20]; // account
  char displayIndicator; // Display
  char maxRemovePct;
  short discretionAmount; // Discretionary Offset 
  long pegDifference;
  char preventMemberMatch[5];
  char locateReqd;
  long expireTime;
  
  char attributedQuote;
  char extExecInst;
  
  // EDGE binary order entry
  char      extendedHrsEligible;
  char      routeOutEligibility;
  char      interMarketSweep;
  char      routingDeliveryMethod;
  short     routeStrategy;
  
  // NASDAQ OUCH
  char      customerType;
  
  // NYSE UTPDirect CCG Binary
  char      senderSubID[32];
  char      dotReserve;
  
  // NASDAQ RASH
  char      pegType;
  long      discretionPrice;
  char      discretionPegType;
  long      discretionPegDiff;
  int       randomReserve;
  
} t_OrderEntry;

typedef struct QUEUEITEM
{
  t_OrderEntry orderEntry;
  char priceScale;
  unsigned int cumShare;//total share filled, accumulated  
  long totalPx; //total executing price
} t_QueueItem;

typedef struct ORDERTOKENQUEUE
{
  t_QueueItem buffer[QUEUE_BUFFER_SIZE];
  unsigned int curIndex;
  
  pthread_mutex_t mutex;
} t_OrderTokenQueue;

int InitOrderTokenQueue(t_OrderTokenQueue *queue);
int AddOrderToken(t_OrderTokenQueue *queue, const t_QueueItem *item);
int PickRandomAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item);

int PickRandomToken(t_OrderTokenQueue *queue);
int RemoveToken(t_OrderTokenQueue *queue, const int index);
int UpdateToken(t_OrderTokenQueue *queue, const int index, const t_QueueItem *item);

int SearchToken(t_OrderTokenQueue *queue, const char *clOrdID);
int SearchAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item);

int DetectCrossOrderToken(t_OrderTokenQueue *queue, const t_OrderEntry *orderEntry, const char buySide);

#endif
