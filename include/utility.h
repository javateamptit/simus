#ifndef __UTILITY_H__
#define __UTILITY_H__

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <time.h>
#include <unistd.h>
#include <math.h>
#include <errno.h>
#include <sys/time.h>

/****************************************************************************
** Include files and define several constants
****************************************************************************/
#define TraceLog(LVL, FMT, PARMS...)                              \
  do {                                                 \
    LrcPrintfLogFormat(LVL, FMT, ## PARMS); }  \
  while (0)

#define DBG_TRACK TraceLog(NOTE_LEVEL, "================ %s::%d ================\n", __FUNCTION__, __LINE__);

#define NO_LOG_LEVEL  -1
#define DEBUG_LEVEL    1
#define NOTE_LEVEL    2
#define WARN_LEVEL    3
#define ERROR_LEVEL    4
#define FATAL_LEVEL    5
#define USAGE_LEVEL    6
#define STATS_LEVEL    7
#define VERBOSE_LEVEL  8

#define DEBUG_STR    "debug: "
#define NOTE_STR    "note: "
#define WARN_STR    "warn: "
#define ERROR_STR    "error: "
#define FATAL_STR    "fatal: "
#define USAGE_STR    "usage: "
#define STATS_STR    "stats: "
#define VERBOSE_STR    "verbose: "

#define DEV_LEVEL    0  //development level
#define DEV_STR      "dev: "  

#define ERROR -1
#define SUCCESS 0

#define DATA_BUFF_SIZE 5120

typedef union t_ShortConverter
{
  unsigned short value;
  char c[2];
} t_ShortConverter;

typedef union t_IntConverter
{
  unsigned int value;
  char c[4];
} t_IntConverter;

typedef union t_LongConverter
{
  unsigned long value;
  char c[8];
} t_LongConverter;

typedef union t_DoubleConverter
{
  double value;
  char c[8];
} t_DoubleConverter;

typedef struct t_DataBlock
{
  // Length of block data excluded block length
  int blockLen;
  
  // Length of message content
  int msgLen;
  
  /*
  Length of additional information 
  such as timestamp, Entry/Exit indicator...
  */
  int addLen;
  
  // Content of message will be stored here
  char msgContent[DATA_BUFF_SIZE];
  
  // Content of additional information
  char addContent[100];
} t_DataBlock;

typedef struct t_Parameters
{
  char parameterList[100][100];
  int countParameters;
} t_Parameters;

void LrcPrintfLogFormat(int level, char *fmt, ...);
int Lrc_itoaf(int value, const char pad, const int len, char *strResult);
int Lrc_itoa(int value, char *strResult);
int Lrc_itoafl(int value, const char pad, const int len, char * strResult);
int Lrc_atoi(const char * buffer, int len);
void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult);
void __lToStrWithLeftPad(unsigned long value, const char pad, const int len, char *strResult);
int __StrWithSpaceRightPad(const char* str, const char len, char* strResult);
int __StrWithSpaceLeftPad(const char* str, const char len, char* strResult);

unsigned long ulongAt(const unsigned char* buf, size_t offset);
unsigned int uintAt(const unsigned char* buf, size_t offset);
unsigned int ushortAt(const unsigned char* buf, size_t offset);

char Lrc_Split(const char *str, char c, void *parameter);
char *RemoveCharacters(char *buffer, int bufferLen);
void strcncpy(unsigned char *des, const char *src, char pad, int totalLen);
void Lrc_itoaf_stripped(int value, const char pad, const int len, char *strResult);
long GetLocalTime_usec();
long GetLocalTime_msec();
long GetLocalTime_nsec();

static const double EPSILON = 1.0e-06;
static inline int feq(double a, double b) {   return __builtin_fabs(a-b) < EPSILON; }
static inline int fgt(double a, double b) {   return a - b >  EPSILON; }
static inline int fge(double a, double b) {   return a - b > -EPSILON; }
static inline int flt(double a, double b) {   return a - b < -EPSILON; }
static inline int fle(double a, double b) {   return a - b <  EPSILON; }
#endif
