#include "souptcp.h"
#include "network.h"

int SOUPTCP_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection)
{
  char message[SOUPTCP_HEARTBEAT_PACKET_LEN];
  // Packet type is 'H'
  message[0] = SOUPTCP_SERVER_HEARTBEAT_PACKET_TYPE;
  // Terminating Linefeed 0x0A
  message[1] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_HEARTBEAT_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId, const long seqNum)
{
  char message[SOUPTCP_LOGIN_ACCEPTED_PACKET_LEN];

  // Packet type is 'A'
  message[0] = SOUPTCP_LOGIN_ACCEPTED_PACKET_TYPE;
  // The session ID
  __StrWithSpaceLeftPad(sessionId, 10, &message[1]);
  // The sequence number
  __lToStrWithLeftPad(seqNum, ' ', 10, &message[11]);
  // Terminating Linefeed 0x0A
  message[21] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_LOGIN_ACCEPTED_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason)
{
  char message[SOUPTCP_LOGIN_REJECTED_PACKET_LEN];

  // Packet type is 'J'
  message[0] = SOUPTCP_LOGIN_REJECTED_PACKET_TYPE;
  // Reject Reason Code
  message[1] = reason;
  // Terminating Linefeed 0x0A
  message[2] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_LOGIN_REJECTED_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen)
{
  char message[msgLen + SOUPTCP_ADDITIONAL_DATA_PACKET_LEN];
  // Packet type is 'S'
  message[0] = SOUPTCP_SEQUENCED_DATA_PACKET_TYPE;
  // Message content
  memcpy(&message[1], msg, msgLen);
  // Terminating Linefeed 0x0A
  message[msgLen+1] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, msgLen + SOUPTCP_ADDITIONAL_DATA_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_ClientHeartbeatPacket (t_Connection* connection)
{
  char message[SOUPTCP_HEARTBEAT_PACKET_LEN];

  // Packet type is 'R'
  message[0] = SOUPTCP_CLIENT_HEARTBEAT_PACKET_TYPE;
  // Terminating Linefeed 0x0A
  message[1] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_HEARTBEAT_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_LoginRequestPacket (t_Connection* connection, const char* userName, const char* password,
    const char* sessionId, const long seqNum)
{
  char message[SOUPTCP_LOGIN_REQUEST_PACKET_LEN];

  // Packet type is 'L'
  message[0] = SOUPTCP_LOGIN_REQUEST_PACKET_TYPE;
  // Username
  __StrWithSpaceRightPad(userName, 6, &message[1]);
  // Password
  __StrWithSpaceRightPad(password, 10, &message[7]);
  // Requested Session
  __StrWithSpaceLeftPad(sessionId, 10, &message[17]);
  // Sequence
  __lToStrWithLeftPad(seqNum, ' ', 10, &message[27]);
  // Terminating Linefeed 0x0A
  message[37] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_LOGIN_REQUEST_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_UnsequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen)
{
  char message[msgLen + SOUPTCP_ADDITIONAL_DATA_PACKET_LEN];

  // Packet type is 'U'
  message[0] = SOUPTCP_UNSEQUENCED_DATA_PACKET_TYPE;
  // message content
  memcpy(&message[1], msg, msgLen);
  // Terminating Linefeed 0x0A
  message[msgLen+1] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, msgLen + SOUPTCP_ADDITIONAL_DATA_PACKET_LEN);
}

int SOUPTCP_BuildAndSend_LogoutRequestPacket (t_Connection* connection)
{
  char message[SOUPTCP_LOGOUT_REQUEST_PACKET_LEN];

  // Message type is 'O'
  message[0] = SOUPTCP_LOGOUT_REQUEST_PACKET_TYPE;
  // Terminating Linefeed 0x0A
  message[1] = SOUPTCP_TERMINATING_LINEFEED;

  return SendMsg(connection->socket, &connection->lock, message, SOUPTCP_LOGOUT_REQUEST_PACKET_LEN);
}

int SOUPTCP_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  char sessionId[11] = "\0", seqNum[11] = "\0";
  
  memcpy(connection->userInfo->userName, &dataBlock->msgContent[1], 6);
  
  memcpy(connection->userInfo->password, &dataBlock->msgContent[7], 10);
  
  // TODO: check userName and password
  
  memcpy(sessionId, &dataBlock->msgContent[17], 10);
  
  memcpy(seqNum, &dataBlock->msgContent[27], 10);
  connection->userInfo->incomingSeq = atol(seqNum);
  
  // TODO: check session id and sequence number
  // if (connection->userInfo->incomingSeq != 0)
    // return BuildAndSendLoginRejected (connection, SOUPTCP_LOGIN_REJECTED_NOT_SESSION);
    
  return SOUPTCP_BuildAndSend_LoginAcceptedPacket(connection, sessionId, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1));
}

int SOUPTCP_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  connection->isAlive = 0;
  
  return SUCCESS;
}

void* SOUPTCP_HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    SOUPTCP_BuildAndSend_ServerHeartbeatPacket(connection);
    sleep(1); // The server heartbeat interval is 1 second
  }
  
  TraceLog(DEBUG_LEVEL, "Close SOUPTCP_HeartbeatThread\n");
  return NULL;
}
