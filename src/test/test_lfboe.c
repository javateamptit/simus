#include "sim.h"
#include "global.h"
#include "network.h"
#include "lava.h"

static int testExit = 0;

static int BuildAndSendUnSequencedData (t_Connection* connection, const char* msg, const int msgLen)
{
  char message[msgLen + LFBOE_PAYLOAD_OFFSET];
  t_ShortConverter shortUnion;
  
  // Length
  shortUnion.value = msgLen + 1;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message type is 'U'
  message[2] = LFBOE_UNSEQUENCED_DATA_PACKET_TYPE;
  // message content
  memcpy(&message[LFBOE_PAYLOAD_OFFSET], msg, msgLen);
  
  return SendMsg(connection->socket, &connection->lock, message, msgLen + LFBOE_PAYLOAD_OFFSET);
}

static int BuildAndSendNewOrder (t_Connection* connection,
  const char* ordToken, const char* symbol, const char side, const int orderQty, const int price,
  const int tif, const char* firm, const char orderCapacity,  const int ordFlags,
  const int minQty, const int maxFloor, const short pegDiff, const char SSEReason)
{
  char msg[LFBOE_ENTER_ORDER_MSG_LEN];
  short m_protocol = 0x0101;
  
  // Protocol  
  memcpy(&msg[0], &m_protocol, 2);
  // Message Type "O"
  msg[2] = LFBOE_ENTER_ORDER_MSG_TYPE;
  // Order Token
  strncpy(&msg[3], ordToken, 14);
  // Buy/Sell Indicator
  msg[17]  = side;
  // Shares
  memcpy(&msg[18], &orderQty, 4);
  // Stock Symbol
  strncpy(&msg[22], symbol, 8);
  // Price
  memcpy(&msg[30], &price, 4);
  // Time In Force
  memcpy(&msg[34], &tif, 4);
  // Firm (Account)
  strncpy(&msg[38], firm, 8);
  // Order Capacity
  msg[46] = orderCapacity;  
  // Order Flags Integer Bit Mask  
  memcpy(&msg[47], &ordFlags, 4);  
  // Min Qty
  memcpy(&msg[51], &minQty, 4);  
  // Max Floor
  memcpy(&msg[55], &maxFloor, 4);
  // Difference A peg offset as entered.
  memcpy(&msg[59], &pegDiff, 2);  
  // Short Sale Exemption Reason as entered.
  msg[61] = SSEReason;

  return BuildAndSendUnSequencedData(connection, msg, LFBOE_ENTER_ORDER_MSG_LEN);
}

static int BuildAndSendCancelRequest (t_Connection* connection, const char* ordToken)
{
  char msg[LFBOE_CANCEL_MSG_LEN];
  short m_protocol = 0x0101;
  
  // Protocol  
  memcpy(&msg[0], &m_protocol, 2);
  // Message Type "X"
  msg[2] = LFBOE_CANCEL_MSG_TYPE;
  // Order Token
  memcpy(&msg[3], ordToken, 14);

  return BuildAndSendUnSequencedData(connection, msg, LFBOE_CANCEL_MSG_LEN);
}

int TEST_LAVA_logon (t_Connection* connection, const char* session, const char* user, const char* password)
{
  return SOUPBINTCP_BuildAndSend_LoginRequestPacket (connection, user, password, session, 0);
}

int TEST_LAVA_logout (t_Connection* connection)
{
  return SOUPBINTCP_BuildAndSend_LogoutRequestPacket (connection);
}

int TEST_LAVA_heartbeat (t_Connection* connection)
{
  return SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket (connection);
}

int TEST_LAVA_order (t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
  char side = (strcmp(sd, "b")==0 || strcmp(sd, "B")==0) ? LFBOE_SIDE_BUY :
              (strcmp(sd, "s")==0 || strcmp(sd, "S")==0) ? 'S' : 'T';
  int quantity = atoi(qty);
  long price = (long)(atof(pr) * 10000.0);
  int tif = (strcmp(tf, "ioc")==0 || strcmp(tf, "IOC")==0) ? LFBOE_TIF_IOC: LFBOE_TIF_DAY;
  
  return BuildAndSendNewOrder(connection, token, symbol, side, quantity, price, tif, "_firm_", 'A', 0, 0, 0, 0, 0);
}

int TEST_LAVA_cancel (t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{
  return BuildAndSendCancelRequest(connection, token);
}

void TEST_LAVA_printInfo (t_Connection* connection, t_DataBlock* data)
{
  char type = 0;
  if (data->msgLen > LFBOE_PAYLOAD_OFFSET)
     type = data->msgContent[LFBOE_PAYLOAD_OFFSET+2];

  if (type == LFBOE_EXECUTED_MSG_TYPE)
  {
    char token[14] = "\0";
    memcpy(token, &data->msgContent[LFBOE_PAYLOAD_OFFSET+11], 14);
    int share = *(unsigned int*)(&data->msgContent[LFBOE_PAYLOAD_OFFSET+25]);
    long price = *(unsigned int*)(&data->msgContent[LFBOE_PAYLOAD_OFFSET+29]);
    long match = *(unsigned long*)(&data->msgContent[LFBOE_PAYLOAD_OFFSET+35]);
    
    TraceLog(DEBUG_LEVEL, "%ld.LAVA FILL %s: NUMBER %d OF %s IS %s AT %f\n", match, token, share, "Sym", "s/b?", price / 10000.0f);
    return;
  }
  
  if (type == LFBOE_CANCELED_MSG_TYPE)
  {
    char token[14] = "\0";
    memcpy(token, &data->msgContent[LFBOE_PAYLOAD_OFFSET+11], 14);
    
    TraceLog(DEBUG_LEVEL, "LAVA CANCEL %s\n", token);
    return;
  }
}

static int HandleCommandLine(t_Connection* connection, t_Parameters* para)
{
  int i;
  
  for (i = 0; i < para->countParameters; i++)
  {
    t_Parameters argv;
    memset(&argv, 0, sizeof(t_Parameters));
    Lrc_Split(para->parameterList[i], ' ', &argv);
    // TraceLog(DEBUG_LEVEL, "*** check parameters = %s; %s; %d\n", para->parameterList[i], argv.parameterList[0], argv.countParameters);
    if (strcmp(argv.parameterList[0], "exit") == 0)
    {
      testExit = 1;
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "quit") == 0)
    {
      testExit = 1;
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "logon") == 0)
    {
      //send logon
      SOUPBINTCP_BuildAndSend_LoginRequestPacket(connection, "_us_", "_pass_", " ", 1);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "logout") == 0)
    {
      //send logout
      SOUPBINTCP_BuildAndSend_LogoutRequestPacket(connection);
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "heartbeat") == 0)
    {
      //handle heartbeat
      SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket(connection);
      continue;
    }

    if (strcmp(argv.parameterList[0], "order") == 0)
    {
      //handle new order
      if (argv.countParameters < 7)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -order <token> <symbol> <side> <quantity> <price> <tif>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      char *symbol =  argv.parameterList[2];
      char side = !strcmp(argv.parameterList[3], "b")  ? LFBOE_SIDE_BUY :
                  !strcmp(argv.parameterList[3], "s")  ? 'S' : 'T';
      int quantity = atoi(argv.parameterList[4]);
      double price = atof(argv.parameterList[5]);
      long longPx = (long) (price * 10000.0);
      char tif = !strcmp(argv.parameterList[6], "ioc")? LFBOE_TIF_IOC: LFBOE_TIF_DAY;
      BuildAndSendNewOrder(connection, token, symbol, side, quantity, longPx, tif, "_firm_", 'A', 0, 0, 0, 0, 0);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "cancel") == 0)
    {
      //handle cancel order
      if (argv.countParameters < 5)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -cancel <token> <symbol> <side> <quantity>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      // char *symbol =  argv.parameterList[2];
      // char side = !strcmp(argv.parameterList[3], "b")  ? BATS_BOE_SIDE_BUY :
                  // !strcmp(argv.parameterList[3], "s")  ? '2' : '5';
      // int quantity = atoi(argv.parameterList[4]);
      
      BuildAndSendCancelRequest(connection, token);
      continue;
    }
  }
  
  return 0;
}

static int ParseCommandLine(char* cmdLine, t_Parameters* para)
{
  Lrc_Split(cmdLine, ' ', para);
  
  return para->countParameters;  
}

static int GetCommandLine(t_Parameters* para)
{
  char cmdLine[128];
  
  fgets(cmdLine, 128, stdin);
  // if (cmdLine == NULL)
  // {
    // return 0;
  // }

  return ParseCommandLine(cmdLine, para);
}

void* StartLFBOETestThread (void* data)
{
  t_Parameters para;
  t_Connection* connection = (t_Connection*) data;
  
  while (1)
  {
    TraceLog(DEBUG_LEVEL, "Get command line >>\n");
    GetCommandLine(&para);
    if (HandleCommandLine(connection, &para) == 1)
      break;
    usleep(1000);
  }  

  return NULL;
}

void LFBOEForTesting ()
{
  struct sockaddr_in server_addr;
  
  int configIdx = 0;
  for (configIdx = 0; configIdx < Configs.countPort; configIdx++)
    if (Configs.portList[configIdx].venue == VENUE_LAVA)
      break;
      
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(Configs.portList[configIdx].port);
  if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot convert net address\n");
    return;
  }
  
  while (!testExit)
  {
    int testSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (testSocket == -1)
    {
      TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
      return;
    }  

    if(connect(testSocket, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Cannot connect to server\n");
      return;
    }
  
    t_UserInfo userInfo;
    t_Connection connection;
  
    strcpy(userInfo.userName, "TEST");
    userInfo.outgoingSeq = 0;
    
    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 1;
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);
    
    pthread_t testThread;
    pthread_create(&testThread, NULL, StartLFBOETestThread, &connection);
    
    pthread_join(testThread, NULL);
    
    close(testSocket);
  }
}

