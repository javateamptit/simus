#ifndef _NYSE_
#define _NYSE_

#include "global.h"
#include "network.h"

#define NYSE_HEARTBEAT_MSG_LEN                8
#define NYSE_LOGON_TYPE_MSG_LEN               60
#define NYSE_ORDER_ACK_MSG_LEN                56
#define NYSE_ORDER_FILLED_SHORT_MSG_LEN       116
#define NYSE_ORDER_CANCELLED_MSG_LEN          56
#define NYSE_CANCEL_ACK_MSG_LEN               56
#define NYSE_ORDER_REJECTED_MSG_LEN           116

#define NYSE_HEARTBEAT_TYPE                   0x0001
#define NYSE_LOGON_TYPE                       0x0021
#define NYSE_LOGON_REJECT_TYPE                0x0141
#define NYSE_ORDER_ACK_TYPE                   0x0091
#define NYSE_ORDER_CANCELLED_TYPE             0x00D1
#define NYSE_CANCEL_REPLACE_TYPE              0x00B1
#define NYSE_ORDER_REPLACED_TYPE              0x00E1
#define NYSE_CANCEL_ACK_TYPE                  0x00A1
#define NYSE_BUST_CORRECT_TYPE                0x0101
#define NYSE_ORDER_FILLED_SHORT_TYPE          0x0081
#define NYSE_ORDER_FILLED_VERBOSE_TYPE        0x00C1
#define NYSE_ORDER_REJECTED_TYPE              0x00F1

#define NYSE_TIMEINFORCE_DAY                  '0'
#define NYSE_TIMEINFORCE_IOC                  '3'
#define NYSE_SIDE_BUY                         '1'
#define NYSE_SIDE_SELL                        '2'
#define NYSE_SIDE_SELL_SHORT                  '5'
#define NYSE_PRICE_SCALE_4                    '4'
#define NYSE_BILLING_INDICATOR_BLENDED        '3'
#define NYSE_LAST_MARKET_N                    'N'
#define NYSE_EXECUTING_BROKER                 "B123"
#define NYSE_CONTRA_BROKER_TOD                "TOD"
#define NYSE_CONTRA_TRADER                    "XYZ"
#define NYSE_AWAY_MARKET_ID                   "A"
#define NYSE_BILLING_RATE                     "2/2"

int HandleMsg_NYSE(t_Connection* client, t_DataBlock *dataBlock);

#endif