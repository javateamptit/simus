#include "order_token_queue.h"
#include "global.h"

int InitOrderTokenQueue(t_OrderTokenQueue *queue)
{
  queue->curIndex = 0;
  int res = pthread_mutex_init(&queue->mutex, NULL);
  if(res != 0)
  {
    TraceLog(ERROR_LEVEL, "unable to init queue mutex\n");
    return ERROR;
  }
  
  return SUCCESS;
}

int AddOrderToken(t_OrderTokenQueue *queue, const t_QueueItem *item)
{
  if(queue->curIndex >= QUEUE_BUFFER_SIZE)
  {
    TraceLog(ERROR_LEVEL, "queue buffer full\n");  
    return ERROR;
  }

  memcpy(&queue->buffer[queue->curIndex], item, sizeof(t_QueueItem));
  queue->curIndex++;

  return SUCCESS;
}

int PickRandomAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
  if(queue->curIndex > 0)
  {
    int bad_luck = 0;
    if(queue->curIndex>1)
    {
      bad_luck = rand()%(queue->curIndex-1);
    }
      
    memcpy(item, &queue->buffer[bad_luck], sizeof(t_QueueItem));
    
    memcpy(&queue->buffer[bad_luck], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
    
    queue->curIndex--;
    
    return SUCCESS;
  }
  
  return ERROR;
}

int PickRandomToken(t_OrderTokenQueue *queue)
{
  if(queue->curIndex > 0)
  {
    int bad_luck = 0;
    if(queue->curIndex>1)
    {
      bad_luck = rand()%(queue->curIndex-1);
    }    
    
    return bad_luck;
  }
  
  return -1;
}

int RemoveToken(t_OrderTokenQueue *queue, const int index)
{
  if(index > -1 && index < queue->curIndex)
  {  
    memcpy(&queue->buffer[index], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
    queue->curIndex--;    
    return  SUCCESS;
  }
  
  return ERROR;
}

int SearchAndRemoveToken(t_OrderTokenQueue *queue, t_QueueItem *item)
{
  int i, errorCode = ERROR;
  
  for(i=0; i<queue->curIndex; i++)
  {
    // if(strcmp(queue->buffer[i].orderEntry.symbol, item->orderEntry.symbol) != 0)
    // {
      // errorCode = -1;
      // continue;
    // }
    
    if(strcmp(queue->buffer[i].orderEntry.clOrdID, item->orderEntry.clOrdID) != 0)
    {
      errorCode = -2;
      continue;
    }

    errorCode = SUCCESS;
    memcpy(item, &queue->buffer[i], sizeof(t_QueueItem));
    memcpy(&queue->buffer[i], &queue->buffer[queue->curIndex-1], sizeof(t_QueueItem));
    queue->curIndex--;
    break;
  }

  return errorCode;
}

int UpdateToken(t_OrderTokenQueue *queue, const int index, const t_QueueItem *item)
{
  if(index > -1 && index < queue->curIndex)
  {
    memcpy(&queue->buffer[index], item, sizeof(t_QueueItem));
    return SUCCESS;
  }

  return ERROR;
}

int SearchToken(t_OrderTokenQueue *queue, const char *clOrdID)
{
  int i;
  
  for(i=0; i<queue->curIndex; i++)
  {
    if(strcmp(queue->buffer[i].orderEntry.clOrdID, clOrdID) == 0)
    {
      return i;
    }
  }

  return -1;
}

int DetectCrossOrderToken(t_OrderTokenQueue *queue, const t_OrderEntry *orderEntry, const char buySide)
{
  int i;
  t_OrderEntry *queueEntry;
  
  for (i = 0; i < queue->curIndex; i++)
  {
    queueEntry = &queue->buffer[i].orderEntry;
    // skip difference symbol
    if (strcmp(queueEntry->symbol, orderEntry->symbol) != 0)
      continue;
    
    // difference side
    if (queueEntry->side != orderEntry->side)
    {
      // cross detect: price of buy is greater or equal price of sell
      if ( (queueEntry->side == buySide && queueEntry->price >= orderEntry->price)
        || (orderEntry->side == buySide && orderEntry->price >= queueEntry->price) )
      {
        TraceLog(ERROR_LEVEL, "Found Cross at %d\n", i);
        return i;
      }
    }
  }
  
  return -1;
}

