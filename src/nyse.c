#include "nyse.h"


static const int g_priceDenominator[5] = { 1, 10, 100, 1000, 10000 };

static int BuildAndSendHeartBeat(t_Connection* connection)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - HeartBeat Message 0.1, it includes the following fields:
    + Message Type    2    Binary     0x0001
    + Length      2    Binary    Binary length of the message.
    + Sequence      4    Binary    Ignore (future use)
    ==> Total byte    8
  */
  
  t_ShortConverter shortUnion;
  
  char message[NYSE_HEARTBEAT_MSG_LEN];
  memset(message, 0, NYSE_HEARTBEAT_MSG_LEN);
  
  // Message type is 0x0001
  shortUnion.value = NYSE_HEARTBEAT_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];

  // Length
  shortUnion.value = NYSE_HEARTBEAT_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  
  // SeqNum (4 bytes)
  
  return SendMsg(connection->socket, &connection->lock, message, NYSE_HEARTBEAT_MSG_LEN);
}

static int BuildAndSendLogonResponse(t_Connection* connection)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Logon Message A.1, it includes the following fields:
    + Message Type        2    Binary         0x0021
    + Length          2    Binary        Binary length of the message.
    + Sequence Number      4    Binary        Ignore (future use)
    + LastMsgSeqNumReceived    4    Binary    
    + SenderCompID        12    Alpha/Numeric    UTPDirect Login ID
    + MessageVersionProfile    32    Binary        Provides the desired message versioning profile to use 
                                during the session. Lists the binary messages types. These 
                                consist of an array of up to 16 binary message types.
    + CancelOnDisconnect    1    Alpha/Numeric    0 = Do not cancel orders, 1 = Cancel all orders 
    + Filler          3    Alpha
    ==> Total byte        60
  */
  
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[NYSE_LOGON_TYPE_MSG_LEN];
  memset(message, 0, NYSE_LOGON_TYPE_MSG_LEN);
  
  //Packet Type is 0x0021
  shortUnion.value = NYSE_LOGON_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_LOGON_TYPE_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  // SeqNum (4 bytes) => NOT USE
  // LastMsgSeqNumReceived (4 bytes)
  intUnion.value = connection->userInfo->incomingSeq;
  message[8] = intUnion.c[3];
  message[9] = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // SenderCompID (12 bytes)
  strncpy(&message[12], "NYSE", 12);
  // MessageVersionProfile (32 bytes - binary)
  shortUnion.value = NYSE_LOGON_TYPE;
  message[24] = shortUnion.c[1];
  message[25] = shortUnion.c[0];
  
  shortUnion.value = NYSE_LOGON_REJECT_TYPE;
  message[26] = shortUnion.c[1];
  message[27] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_ACK_TYPE;
  message[28] = shortUnion.c[1];
  message[29] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_CANCELLED_TYPE;
  message[30] = shortUnion.c[1];
  message[31] = shortUnion.c[0];
  
  shortUnion.value = NYSE_CANCEL_REPLACE_TYPE;
  message[32] = shortUnion.c[1];
  message[33] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_REPLACED_TYPE;
  message[34] = shortUnion.c[1];
  message[35] = shortUnion.c[0];
  
  shortUnion.value = NYSE_CANCEL_ACK_TYPE;
  message[36] = shortUnion.c[1];
  message[37] = shortUnion.c[0];
  
  shortUnion.value = NYSE_BUST_CORRECT_TYPE;
  message[38] = shortUnion.c[1];
  message[39] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_FILLED_SHORT_TYPE;
  message[40] = shortUnion.c[1];
  message[41] = shortUnion.c[0];
  
  shortUnion.value = NYSE_ORDER_REJECTED_TYPE;
  message[42] = shortUnion.c[1];
  message[43] = shortUnion.c[0];
  // 44 -> 55 => NOT USE
  // CancelOnDisconnect (1 byte - Alpha/Numeric)
  message[56] = (char) connection->isCancelOnDisconnect;
  // Filler (3 bytes) => NOT USE
  
  return SendMsg(connection->socket, &connection->lock, message, NYSE_LOGON_TYPE_MSG_LEN);
}

static int BuildAndSendACK(t_Connection* connection, t_OrderEntry *orderEntry)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Order Ack Message a.1, it includes the following fields:
    + Message Type        2    Binary         0x0091
    + Length          2    Binary        Binary length of the message.
    + Sequence Number      4    Binary        Exchange assigned sequence number
    + MEOrderID          4    Binary        Exchange assigned Order ID  (will be supported in a future phase)
    + TransactTime        4    Binary        The time the message was sent in milliseconds since Midnight
    + DeliverToCompID      5    Alpha        OnBehalfOfCompID from the client is returned in this field
    + TargetSubID        5    Alpha        SenderSubID from the client is returned in this field.
                                If provided on inbound order it will be returned.
    + Account          10    Alpha        User-defined information that is not validated. Common examples
                                include user or account IDs and will be returned in the 
                                outbound messages. Will be returned if provided in the inbound message
    + ClientOrderID        17    Alpha/Numeric    Client Order ID
    + Filler          3    Alpha                            
    ==> Total byte        56
  */
  
  t_IntConverter intUnion;
  t_ShortConverter shortUnion;
  
  char message[NYSE_ORDER_ACK_MSG_LEN];
  memset(message, 0, NYSE_ORDER_ACK_MSG_LEN);
  
  // Message Type is 0x0091
  shortUnion.value = NYSE_ORDER_ACK_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_ORDER_ACK_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  // Sequence Number
  intUnion.value = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // MEOrderID ( 4 bytes ) = 8 -> 11
  orderEntry->orderID = __sync_add_and_fetch(&connection->orderID, 1);
  intUnion.value = orderEntry->orderID;
  message[8]  = intUnion.c[3];
  message[9]  = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // Transaction Time  ( 4  bytes ) = 12 -> 15
  intUnion.value = (int) GetLocalTime_usec();
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // DeliverToCompID (5 bytes) = 16 -> 20
  memcpy(&message[16], orderEntry->clearingAccount, 5);
  // TargetSubID (5 bytes) = 21 -> 25
  memcpy(&message[21], orderEntry->senderSubID, 5);
  // Account (10 bytes) = 26 -> 35
  memcpy(&message[26], orderEntry->clientId, 10);
  // Client Order Id ( 17 bytes)
  memcpy(&message[36], orderEntry->clOrdID, 17);
  // Filler ( 3 bytes ) = 53 -> 55 => NOT USE
    
  return SendMsg(connection->socket, &connection->lock, message, NYSE_ORDER_ACK_MSG_LEN);
}

static int BuildAndSendShortFill(t_Connection *connection, t_QueueItem* item,
      const int quantity, const int price, const char billingIndicator, const char lastMarket, const char* execBroker,
      const char* contraBroker, const char* contraTrader, const char* execAwayMktID, const char* billingRate)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Order Fill Message  Variant 1 (2.1), it includes the following fields:
    + Message Type        2    Binary         0x0081
    + Length          2    Binary        Binary length of the message.
    + MsgSeqNum          4    Binary        Exchange assigned sequence number
    + MEOrderID          4    Binary        Exchange assigned Order ID  (will be supported in a future phase)
    + TransactTime        4    Binary        Time the message was sent in milliseconds since Midnight
    + LeavesQty          4    Binary        Leaves remaining on order
    + LastShares        4    Binary        Number of equity shares filled
    + LastPrice          4    Binary        Price at which the shares or contracts were filled
    + PriceScale        1    Alpha/Numeric    0 through 4
    + Side            1    Alpha/Numeric    Side of the Order, 1 = Buy, 2 = Sell, 5 = Sell Short
    + BillingIndicator      1    Alpha/Numeric    
    + LastMarket        1    Alpha        Handling Market Center of the Execution
    + DeliverToCompID      5    Alpha        OnBehalfOfCompID (Firm Mnemonic) provided on the original order from the client is returned in this field
    + TargetSubID        5    Alpha        SenderSubID from the client is returned in this field. If provided on inbound order, it will be returned.
    + ExecBroker        5    Alpha/Numeric    Indicates the Executing DMMs/Executing Brokers number
    + ContraBroker        5    Alpha/Numeric    
    + ContraTrader        5    Alpha/Numeric    Identifies the trader of the Contra Broker
    + ExecAwayMktID        6    Alpha        Away Market Identifier followed by optional Market Maker ID field. Returned if executed at an Away Market.
    + BillingRate        6    Alpha/Numeric
    + ExecID          10    Alpha/Numeric    Exchange assigned reference ID returned on the current transaction message. Format: RRRRRSSSSS
    + Account          10    Alpha/Numeric    
    + DBExecID          10    Alpha/Numeric    Associates all buy and sell execution reports and tape prints.  
    + ClientOrderID        17    Alpha/Numeric    Client order ID                        
    ==> Total byte        116
  */
  
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[NYSE_ORDER_FILLED_SHORT_MSG_LEN + 1] = "\0";

  // Message type is 0x0081
  shortUnion.value = NYSE_ORDER_FILLED_SHORT_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_ORDER_FILLED_SHORT_MSG_LEN;
  message[2]  = shortUnion.c[1];
  message[3]  = shortUnion.c[0];
  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // MEOrderID ( 4 bytes)
  intUnion.value = item->orderEntry.orderID;
  message[8]  = intUnion.c[3];
  message[9]  = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];  
  // TransactTime (4 bytes)
  intUnion.value = (int) GetLocalTime_usec();
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // LeavesQty        4
  intUnion.value = item->orderEntry.quantity - item->cumShare;
  message[16] = intUnion.c[3];
  message[17] = intUnion.c[2];
  message[18] = intUnion.c[1];
  message[19] = intUnion.c[0];
  // LastShares        4
  intUnion.value = quantity;
  message[20] = intUnion.c[3];
  message[21] = intUnion.c[2];
  message[22] = intUnion.c[1];
  message[23] = intUnion.c[0];
  // + LastPrice        4
  intUnion.value = price;
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  // PriceScale        1
  message[28] = NYSE_PRICE_SCALE_4;
  // Side          1
  message[29] = item->orderEntry.side;
  // BillingIndicator    1
  message[30] = billingIndicator;
  // LastMarket        1
  message[31] = lastMarket;
   // DeliverToCompID
  memcpy(&message[32], item->orderEntry.clearingAccount, 5);
  // TargetSubID
  memcpy(&message[37], item->orderEntry.senderSubID, 5);
  // ExecBroker        5
  strncpy(&message[42], execBroker, 5);
  // ContraBroker      5
  strncpy(&message[47], contraBroker, 5);
  // ContraTrader      5
  strncpy(&message[52], contraTrader, 5);
  // ExecAwayMktID      6
  strncpy(&message[57], execAwayMktID, 6);
  // BillingRate      6
  strncpy(&message[63], billingRate, 6);
  // ExecID          10
  int value = __sync_add_and_fetch(&connection->executionNumber, 1);
  sprintf(&message[69], "%u", value);
  // Account
  memcpy(&message[79], item->orderEntry.clientId, 10);
  // + DBExecID        10
  sprintf(&message[89], "%u", value);
  // ClientOrderID      17
  memcpy(&message[99], item->orderEntry.clOrdID, 17);

  return SendMsg(connection->socket, &connection->lock, message, NYSE_ORDER_FILLED_SHORT_MSG_LEN);
}

static int NYSE_Callback_Filled (t_Connection* connection, t_QueueItem* item,
      const int quantity, const long price, const long matchNum)
{
  return BuildAndSendShortFill(connection, item, quantity, (int)price,
      NYSE_BILLING_INDICATOR_BLENDED, NYSE_LAST_MARKET_N, NYSE_EXECUTING_BROKER,
      NYSE_CONTRA_BROKER_TOD, NYSE_CONTRA_TRADER, NYSE_AWAY_MARKET_ID, NYSE_BILLING_RATE);
}

static int BuildAndSendCancelACK(t_Connection* connection, t_OrderEntry* orderEntry)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[NYSE_CANCEL_ACK_MSG_LEN];
  memset(message, 0, NYSE_CANCEL_ACK_MSG_LEN);
  
  // Message type is 0x00A1
  shortUnion.value = NYSE_CANCEL_ACK_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_CANCEL_ACK_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // MEOrderID          4
  intUnion.value = orderEntry->orderID;
  message[8]  = intUnion.c[3];
  message[9]  = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // TransactTime        4
  intUnion.value = (int) GetLocalTime_usec();
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // DeliverToCompID
  memcpy(&message[16], orderEntry->clearingAccount, 5);
  // TargetSubID
  memcpy(&message[21], orderEntry->senderSubID, 5);
  // Account
  memcpy(&message[26], orderEntry->clientId, 10);
  // OrigClientOrderID
  memcpy(&message[36], orderEntry->clOrdID, 17);
  // Filler (3 bytes) => NOT USE
  
  return SendMsg(connection->socket, &connection->lock, message, NYSE_CANCEL_ACK_MSG_LEN);
}

static int BuildAndSendCancelled(t_Connection* connection, t_QueueItem* item, const char infoCode)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Order Killed Message  UROUT Message (4.1), it includes the following fields:
    + Message Type        2    Binary         0x00D1
    + Length          2    Binary        Binary length of the message.
    + MsgSeqNum          4    Binary        Exchange assigned sequence number
    + MEOrderID          4    Binary        Exchange assigned Order ID  (will be supported in a future phase). If provided on inbound cancel request, it will be returned.
    + TransactTime        4    Binary        Time the message was sent in milliseconds since Midnight
    + InformationCode      1    Binary        0 = User-Initiated
                                1 = Exchange-Initiated  Unsolicited UROUT
                                2 = Exchange-Initiated  Cancel on Disconnect
                                3 = Exchange-Initiated  Done for Day
                                4 = Exchange-Initiated  Self Trade Prevented
    + DeliverToCompID      5    Alpha        OnBehalfOfCompID from the client is returned in this field
    + TargetSubID        5    Alpha        SenderSubID from the client is returned in this field. If provided on inbound order, it will be returned.
    + Account          10    Alpha/Numeric    
    + OrigClientOrderID      17    Alpha/Numeric    Client order ID of the canceled order
    + Filler          2    Alpha
    ==> Total byte        56
  */
  
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[NYSE_ORDER_CANCELLED_MSG_LEN];
  memset(message, 0, NYSE_ORDER_CANCELLED_MSG_LEN);
  
  // Message type is 0x00D1
  shortUnion.value = NYSE_ORDER_CANCELLED_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_ORDER_CANCELLED_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // MEOrderID          4
  intUnion.value = item->orderEntry.orderID;
  message[8]  = intUnion.c[3];
  message[9]  = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // TransactTime        4
  intUnion.value = (int) GetLocalTime_usec();
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // InformationCode
  message[16] = infoCode;
  // DeliverToCompID
  memcpy(&message[17], item->orderEntry.clearingAccount, 5);
  // TargetSubID
  memcpy(&message[22], item->orderEntry.senderSubID, 5);
  // Account
  memcpy(&message[27], item->orderEntry.clientId, 10);
  // OrigClientOrderID
  memcpy(&message[37], item->orderEntry.clOrdID, 17);
  // Filler (2 bytes) => NOT USE
  
  return SendMsg(connection->socket, &connection->lock, message, NYSE_ORDER_CANCELLED_MSG_LEN);
}

static int NYSE_Callback_Cancelled(t_Connection* connection, t_QueueItem* item)
{
  return BuildAndSendCancelled(connection, item, '1');
}

static int BuildAndSendRejected(t_Connection* connection, t_OrderEntry *orderEntry, 
      const char* clOrdID, const char rejectType, const short reason, const char* text)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Order Cancel/Replace Reject Message (8.1), it includes the following fields:
    + Message Type        2    Binary         0x00F1
    + Length          2    Binary        Binary length of the message.
    + MsgSeqNum          4    Binary        Exchange assigned sequence number
    + MEOrderID          4    Binary        Exchange assigned Order ID  (will be supported in a future phase)
    + TransactTime        4    Binary        Time the message was sent in milliseconds since Midnight
    + RejectReason        2    Binary        Reject reason code (Refer to Appendix B for supported reason codes)
    + RejectMsgType        1    Numeric        1= Order Reject, 2= Cancel Reject, 3= Cancel Replace Reject
    + DeliverToCompID      5    Alpha        OnBehalfOfCompID from the client is returned in this field
    + TargetSubID        5    Alpha        SenderSubID from the client is returned in this field. If provided on inbound cancel or cancel/replace request ,   it will be returned
    + Account          10    Alpha/Numeric    
    + ClientOrderID        17    Alpha/Numeric    Client Order ID of the order, cancel, or cancel replace that was sent
    + OrigClientOrderID      17    Alpha/Numeric    ID of original order
    + Text            40    Alpha        Reason for the rejection
    + Filler          3    Alpha
    ==> Total byte        116
  */
  
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[NYSE_ORDER_REJECTED_MSG_LEN];
  memset(message, 0, NYSE_ORDER_REJECTED_MSG_LEN);
  
  // Message type is 0x00F1
  shortUnion.value = NYSE_ORDER_REJECTED_TYPE;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Length
  shortUnion.value = NYSE_ORDER_REJECTED_MSG_LEN;
  message[2]  = shortUnion.c[1];
  message[3]  = shortUnion.c[0];
  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // MEOrderID  
  intUnion.value =  (int)orderEntry->orderID;
  message[8]  = intUnion.c[3];
  message[9]  = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // Transaction Time 
  intUnion.value = (int)GetLocalTime_usec();
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // RejectReason
  shortUnion.value = reason;
  message[16]  = shortUnion.c[1];
  message[17]  = shortUnion.c[0];
  // RejectMsgType
  message[18] = rejectType;
  // DeliverToCompID
  memcpy(&message[19], orderEntry->clearingAccount, 5);
  // TargetSubID 
  memcpy(&message[24], orderEntry->senderSubID, 5);
  // Account
  memcpy(&message[29], orderEntry->clientId, 10);
  // ClientOrderID        17
  strncpy(&message[39], clOrdID, 17);
  // OrigClientOrderID
  memcpy(&message[56], orderEntry->clOrdID, 17);
  // Text
  strncpy(&message[73], text, 40);
  //Filler (113 -> 115) => NOT USE
  
  return SendMsg(connection->socket, &connection->lock, message, NYSE_ORDER_REJECTED_MSG_LEN);
}

static int HandleMsgLogon(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  // MsgSeqNum
  connection->userInfo->incomingSeq = 0;
  // LastMsgSeqNumReceived
  // TODO: check : int lastMsgSeqNum = uintAt((unsigned char*)dataBlock->msgContent, 8);
  // SenderCompID
  memcpy(connection->userInfo->userName, &dataBlock->msgContent[12], 12);
  // MessageVersionProfile
  memcpy(connection->messageVersionProfile, &dataBlock->msgContent[24], 32);
  // CancelOnDisconnect
  connection->isCancelOnDisconnect = dataBlock->msgContent[56];
  // Filler => NOT USE
  
  return BuildAndSendLogonResponse(connection);
}

static int HandleMsgCancelOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 27-Mar-2014
  - Modified by: Tuan Thieu-Van
  - Order Cancel Message (F.1), it includes the following fields:
    + Message Type        2    Binary         0x0061
    + MsgLength          2    Binary        Binary length of the message.
    + MsgSeqNum          4    Binary        Client-assigned message sequence number
    + MEOrderID          4    Binary        Exchange assigned Order ID. (will be supported in a future phase)
    + OriginalOrderQty      4    Binary        Original order quantity specified in the order
    + CancelQty          4    Binary        Used for Cancel Reduce Only. Required when Leaves Quantity is present.
    + LeavesQty          4    Binary        Used for Cancel Reduce Only. Required when Cancel Quantity is present.
    + Symbol          11    Alpha        Stock symbol, including the suffix, separated by a blank space e.g., BRK A
    + Side            1    Alpha/Numeric    1 = Buy, 2 = Sell, 5 = Sell Short
    + OnBehalfOfCompID      5    Alpha        Firm mnemonic assigned by the NYSE
    + SenderSubID        5    Alpha/Numeric    User-defined information that is not validated
    + Account          10    Alpha/Numeric
    + ClientOrderID        17    Alpha/Numeric
    + OrigClientOrderID      17    Alpha/Numeric
    + Filler          2    
    ==> Total byte        92
  */
  
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  // TODO: should be check MsgSeqNum before
  // int seqNum = uintAt((unsigned char*)dataBlock->msgContent, 4);
  // MEOrderID
  orderEntry.orderID = uintAt((unsigned char*)dataBlock->msgContent, 8);
  // OriginalOrderQty
  orderEntry.quantity = uintAt((unsigned char*)dataBlock->msgContent, 12);
  // TODO: CancelQty
  // int cancelQty = uintAt((unsigned char*)dataBlock->msgContent, 16);
  // TODO: LeavesQty
  // int leavesQty = uintAt((unsigned char*)dataBlock->msgContent, 20);
  // Symbol
  memcpy(orderEntry.symbol, &dataBlock->msgContent[24], 11);
  // Side
  orderEntry.side = dataBlock->msgContent[35];
  // OnBehalfOfCompID
  memcpy(orderEntry.clearingAccount, &dataBlock->msgContent[36], 5);
  // SenderSubID
  memcpy(orderEntry.senderSubID, &dataBlock->msgContent[41], 5);
  // Account
  memcpy(orderEntry.clientId, &dataBlock->msgContent[46], 10);
  // ClientOrderID => NOT USE
  // OrigClientOrderID
  memcpy(orderEntry.clOrdID, &dataBlock->msgContent[73], 17);
  // Filler => NOT USE

  // TODO: Check validated : 
  BuildAndSendCancelACK(connection, &orderEntry);
  
  t_QueueItem item;
  memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));

  int res = SearchAndRemoveToken(&connection->orderQueue, &item);

  if(res == SUCCESS)
  {
    BuildAndSendCancelled(connection, &item, '0');
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "NYSE: Not found order(%s), errorCode(%d)\n", orderEntry.clOrdID, res);
    BuildAndSendRejected(connection, &item.orderEntry, item.orderEntry.clOrdID, '2', 4035, "REJ - clOrdID not found");
  }
  
  return SUCCESS;
}

static int HandleMsgNewOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  /*
  - API version: NYSE 1.14
  - Date updated: 21-Mar-2014
  - Modified by: Tuan Thieu-Van
  - New Order Message D.1, it includes the following fields:
    + Message Type        2    Binary         0x0041
    + Length          2    Binary        Binary length of the message.
    + MsgSeqNum          4    Binary        Client-assigned message sequence number
    + OrderQty          4    Binary        number of shares
    + MaxFloorQty        4    Binary        
    + Price            4    Binary        The price, as a long value.
    + PriceScale        1    Alpha/Numeric    0 through 4  as defined in the Price Scale Codes
    + Symbol          11    Alpha        Stock symbol, including the suffix, separated by a blank space e.g., BRK A
    + ExecInst          1    Alpha/Numeric    E = DNI (Do Not Increase), F = DNR (Do not Reduce)
    + Side            1    Alpha/Numeric    1 = Buy, 2 = Sell, 5 = Sell Short
    + OrderType          1    Alpha/Numeric    1 = Market, 2 = Limit, 3 = Stop, 5 = Market on close, B = Limit On Close
    + TimeInForce        1    Alpha/Numeric    0 = Day, 3 = IOC (Immediate Or Cancel)
    + OrderCapacity        1    Alpha/Numeric    Valid values: A through Z (see Appendix C for definitions)
    + RoutingInstruction    1    Alpha/Numeric
    + DOTReserve        1    Alpha
    + OnBehalfOfCompID      5    Alpha
    + SenderSubID        5    Alpha/Numeric
    + ClearingFirm        5    Alpha
    + Account          10    Alpha/Numeric
    + ClientOrderID        17    Alpha/Numeric
    + Filler          3    Alpha
    ==> Total byte        84
  */
  
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  // TODO: should be check MsgSeqNum before
  // int seqNum = uintAt((unsigned char*)dataBlock->msgContent, 4);
  // OrderQty
  orderEntry.quantity = uintAt((unsigned char*)dataBlock->msgContent, 8);
  // MaxFloorQty
  orderEntry.maxFloor = uintAt((unsigned char*)dataBlock->msgContent, 12);
  // Price
  orderEntry.price = uintAt((unsigned char*)dataBlock->msgContent, 16);
  // PriceScale
  char priceScale = dataBlock->msgContent[20];
  // Symbol
  memcpy(orderEntry.symbol, &dataBlock->msgContent[21], 11);
  // ExecInst
  orderEntry.execInst = dataBlock->msgContent[32];
  // Side
  orderEntry.side = dataBlock->msgContent[33];
  // OrderType
  orderEntry.orderType = dataBlock->msgContent[34];
  // TimeInForce
  orderEntry.tif = dataBlock->msgContent[35];
  // OrderCapacity
  orderEntry.capacity = dataBlock->msgContent[36];
  // RoutingInstruction
  orderEntry.RoutingInst[0] = dataBlock->msgContent[37];
  // TODO: DOTReserve
  orderEntry.dotReserve = dataBlock->msgContent[38];
  // OnBehalfOfCompID
  memcpy(orderEntry.clearingAccount, &dataBlock->msgContent[39], 5);
  // SenderSubID
  memcpy(orderEntry.senderSubID, &dataBlock->msgContent[44], 5);
  // ClearingFirm
  memcpy(orderEntry.clearingFirm, &dataBlock->msgContent[49], 5);
  // Account
  memcpy(orderEntry.clientId, &dataBlock->msgContent[54], 10);
  // ClientOrderID
  memcpy(orderEntry.clOrdID, &dataBlock->msgContent[64], 17);
  // Filler => NOT USE

  if(orderEntry.tif != NYSE_TIMEINFORCE_DAY && orderEntry.tif != NYSE_TIMEINFORCE_IOC)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    BuildAndSendRejected(connection, &orderEntry, orderEntry.clOrdID, '1', 4035, "REJ - TIF Feature Unavailable");
  }
  else if(orderEntry.side != NYSE_SIDE_BUY && orderEntry.side != NYSE_SIDE_SELL && orderEntry.side != NYSE_SIDE_SELL_SHORT)
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    BuildAndSendRejected(connection, &orderEntry, orderEntry.clOrdID, '1', 4035, "REJ - Side Feature Unavailable");
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    BuildAndSendRejected(connection, &orderEntry, orderEntry.clOrdID, '1', 4009, "REJ - Invalid Order Quantity");
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "NYSE New order: symbol(%.8s), clOrdID(%s), quantity(%d), price(%.4f) side(%c) tif(%c)\n", 
                    orderEntry.symbol, orderEntry.clOrdID, orderEntry.quantity, 
                    orderEntry.price/ pow(10.0, (double) (priceScale-48)) , orderEntry.side, orderEntry.tif);
    BuildAndSendACK(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = NYSE_PRICE_SCALE_4;
    item.cumShare = 0;
    item.totalPx = 0;
    // re-calculate price with scale '4'
    item.orderEntry.price *= g_priceDenominator[NYSE_PRICE_SCALE_4 - priceScale];
    
    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, NYSE_SIDE_BUY, &NYSE_Callback_Filled);
    //IOC Random
    if (orderEntry.tif == NYSE_TIMEINFORCE_IOC)
      if (hasCross)
        BuildAndSendCancelled(connection, &item, '1');
      else
        Simulator_Handle_RandomIOC(connection, &item, &NYSE_Callback_Filled, &NYSE_Callback_Cancelled);
    else if (item.orderEntry.quantity > item.cumShare) // DAY
      AddOrderToken(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static void* DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    Simulator_Handle_RandomDAY(connection, &NYSE_Callback_Filled, &NYSE_Callback_Cancelled);
    
    sleep(15);
  }
  
  return NULL;
}

static void* HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    BuildAndSendHeartBeat(connection);
    sleep(30); // The server heartbeat interval is 60 seconds
  }
  
  TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
  return NULL;
} 

int HandleMsg_NYSE(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_ShortConverter shortUnion;
  shortUnion.c[0] = dataBlock->msgContent[1];
  shortUnion.c[1] = dataBlock->msgContent[0];

  switch(shortUnion.value)
  {
    case 0x0021:
      TraceLog(DEBUG_LEVEL, "Received login message from client\n");
      if(HandleMsgLogon(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, HeartbeatThread, (void*)connection);
      }
      break;
      
    case 0x0011:
      break;
    
    case 0x0001:
      break;
    
    case 0x0041:
      HandleMsgNewOrder(connection, dataBlock);
      break;
      
    case 0x0061:
      HandleMsgCancelOrder(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown MsgType(%d)\n", shortUnion.value);
      break;
  }
  return SUCCESS;
}