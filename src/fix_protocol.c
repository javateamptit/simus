#include "fix_protocol.h"
#include "utility.h"

int FIX_GetTagValue (const char* msg, const int tag, char* value)
{
  char tagMask[8];
  char *msgTag, *term;
  
  sprintf(tagMask, "%d=", tag);
  msgTag = strstr(msg, tagMask);
  if (msgTag == NULL)
  {
    TraceLog(DEBUG_LEVEL, "Tag %d doestn't appear\n", tag);
    value[0] = '\0';
    return -1;
  }

  msgTag += strlen(tagMask);
  term = strstr(msgTag, "\001");
  
  strncpy (value, msgTag, term - msgTag);
  value[term - msgTag] = '\0';
  
  return term - msg + 1;
}

int FIX_AddTagValueChar (char* msg, const int tag, const char value)
{
  // "4=B<SOH>"
  return sprintf(msg, "%d=%c\001", tag, value);  
}

int FIX_AddTagValueString (char* msg, const int tag, const char* value)
{
  // "8=FIX.x.y<SOH>"
  return sprintf(msg, "%d=%s\001", tag, value);  
}

int FIX_AddTagValueInt (char* msg, const int tag, const int value)
{
  // "9=81<SOH>"
  return sprintf(msg, "%d=%d\001", tag, value);
}

int FIX_AddTagValueDouble (char* msg, const int tag, const double value)
{
  // "6=23.00<SOH>"
  return sprintf(msg, "%d=%f\001", tag, value);
}

int FIX_AddTagValueTime (char* msg, const int tag, const time_t value)
{
  struct tm local_tm;
  localtime_r(&value, &local_tm);
  
  // "42=20140410-10:12:23<SOH>"
  return sprintf(msg, "%d=%d%02d%02d-%02d:%02d:%02d\001", tag, 
        local_tm.tm_year + 1900, local_tm.tm_mon + 1, local_tm.tm_mday,
        local_tm.tm_hour, local_tm.tm_min, local_tm.tm_sec);
}

unsigned int FIX_CalculateChecksum(const char* buf, const long bufLen)
{
  long idx;
  unsigned int cks;

  for(idx = 0L, cks = 0; idx < bufLen; cks += (unsigned int)buf[idx++]);

  return (cks % 256);
}

int FIX_BuildCommonTagsField(char* msg, const char msgType, const char* senderComp, const char* targetComp, int* seqNum)
{
  int length = 0;
  time_t now;
  time(&now);
  
  // "35=A<SOH>49=TEST<SOH>56=NSX<SOH>34=1<SOH>52=20140414-05:04:03<SOH>"
  length += FIX_AddTagValueChar(&msg[length], FIX_TAG_MSG_TYPE, msgType);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_SENDER_COMPID, senderComp);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_TARGET_COMPID, targetComp);
  length += FIX_AddTagValueInt(&msg[length], FIX_TAG_MSG_SEQNO, __sync_add_and_fetch(seqNum, 1));
  length += FIX_AddTagValueTime(&msg[length], FIX_TAG_SENDING_TIME, now);
  
  return length;
}

char* FIX_BuildMsgWithHeaderField(char* msg, const char* beginStr, const int bodyLen, int* headerLen)
{
  // "8=FIX4.2<SOH>9=23<SOH>"
  sprintf(msg, "%d=%s\001%d=%d\001", FIX_TAG_BEGIN_STRING, beginStr, FIX_TAG_BODY_LENGTH, bodyLen);
  *headerLen = strlen(msg);
  
  int i, offset = FIX_RESERVE_HEADER_LEN - *headerLen;
  
  if (offset > 0)
    for (i = 0; i < *headerLen; i++)
      msg[FIX_RESERVE_HEADER_LEN - i - 1] = msg[*headerLen - i - 1];
      
  return &msg[offset];
}

char* FIX_BuildMsgWithTrailerField(char* msg, const int bodyLen, const int headerLen)
{
  unsigned int checksum;
  
  checksum = FIX_CalculateChecksum(msg, headerLen + bodyLen);
  
  // "10=023<SOH>"
  sprintf(&msg[headerLen + bodyLen], "%d=%03d\001", FIX_TAG_CHECKSUM, checksum);
      
  return msg;
}

int FIX_StringReplaceAll(char* outStr, const char* inStr, const char* regular, const char* replacement)
{
  const char *strMark, *strSub = inStr;
  const int regLen = strlen(regular), repLen = strlen(replacement);
  int match = 0;
  
  memset(outStr, 0, sizeof(outStr));
  
  while (1)
  {
    // reach end of inStr
    if (strSub[0] == '\0')
      break;
      
    // check the match for regular
    strMark = strstr(strSub, regular);
    if (strMark == NULL)
    {
      strcpy(outStr, strSub);
      break;
    }
      
    // copy content to outStr
    const int contLen = strMark - strSub;
    strncpy(outStr, strSub, contLen);
    strcpy(&outStr[contLen], replacement);
    
    // update string
    outStr += contLen + repLen;
    strSub = strMark + regLen;
    match++;
  }
  
  return match;
}

