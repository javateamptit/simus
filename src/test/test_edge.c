#include "config.h"
#include "utility.h"
#include "global.h"
#include "sim.h"
#include "edge.h"
#include "network.h"


int TEST_EDGE_logon(t_Connection* connection, const char* session, const char* user, const char* password)
{
  return EDGE_MEP_BuildAndSend_LoginPacket(connection, user, password, session, 0);
}

int TEST_EDGE_logout(t_Connection* connection)
{
  return EDGE_MEP_BuildAndSend_LogoutPacket(connection);
}

int TEST_EDGE_heartbeat (t_Connection* connection)
{
	return EDGE_MEP_BuildAndSend_HeartbeatPacket(connection);
}

static int BuildAndSend_EDGE_OrderShortMsg(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
	t_ShortConverter shortUnion;
	t_IntConverter intUnion;
  char msgContent[40] ;  
  // Unsequence data packet
  shortUnion.value = 38; 
	msgContent[0] = shortUnion.c[1];
	msgContent[1] = shortUnion.c[0];
	
  msgContent[2] = EDGE_MEP_UNSEQUENCED_DATA_PACKET_TYPE;
  
  // Message Type
  msgContent[3] = 'O';
	
 // Order Token
  __StrWithSpaceRightPad(token, 14, &msgContent[4]);
  
	/* Buy/Sell indicator */
	msgContent[18] = *sd;
	
	/* Share */
	intUnion.value = atoi(qty);
	msgContent[19] = intUnion.c[3];
	msgContent[20] = intUnion.c[2];
	msgContent[21] = intUnion.c[1];
	msgContent[22] = intUnion.c[0];
	
	/* Symbol: 6 Bytes Alpha */
	strcncpy((unsigned char *)&msgContent[23], symbol, ' ', 6);
	
	/* Price: 4 bytes Integer, The limit price of the order. The price is a 5 digit whole number followed by a 5 decimal digits. */
  double price = atof(pr);
	if (fge(price, 1.00))
	{
		if (*sd == 'B')	//BUY SIDE
		{
			intUnion.value = (int)(price * 100.00 + 0.500001) * 1000;
		}
		else
		{
			intUnion.value = (int)(price * 100.00 + 0.000001) * 1000;
		}
	}
	else
	{
		intUnion.value = (int)(price * 100000.00 + 0.500001);
	}
	msgContent[29] = intUnion.c[3];
	msgContent[30] = intUnion.c[2];
	msgContent[31] = intUnion.c[1];
	msgContent[32] = intUnion.c[0];
	
	/* Time in Force: 1 byte Interger, 0 = IOC, 1 = Day, 2 = Fill or Kill */
	msgContent[33] = strcmp(tf, "DAY") ==0 ? 1 : 0;
	
	/* Display: 1 byte Alpha, �Y� = Displayed, �N� = Hidden, �A� = Displayed with Attribution */
	msgContent[34] = 'Y';

	/* Special order type: 1 byte Alpha
		�M�= MidPoint Match EDGX/MidPoint Peg EDGA
		�D� = Midpoint Discretionary Order � EDGA Only*
		�U� = Route Peg Order
		Re-Pricing Options:
			�S� = Hide Not Slide
			�P� = Price Adjust
			�R� = Single Re-Price
			�C� = Cancel Back
		(See Appendix B for details on Re-Pricing options.)
		*Midpoint Discretionary: Orders require a �Y� in the Display field.
	
		Enter a space for this field if	not applicable.
	*/
	msgContent[35] = ' ';
	
	/* Extended hours eligible: 1 byte alpha
		�R� = Regular Session Only
		�P� = Pre-Market and Regular Session Eligible
		�A� = Regular Session and Post-Market Eligible
		�B� = All Sessions Eligible*/
	msgContent[36] = 'B';
	
	/* Capacity: 1 byte Alpha
		�A� = Agency
		�P� = Principal
		�R� = Riskless Principal
	*/
	msgContent[37] = 'P';
	
  // Route out Eligibility 
	msgContent[38] = 'N';
	
	// ISO eligiblity
	msgContent[39] = 'Y';

  return SendMsg(connection->socket, &connection->lock, msgContent, 40);  	
}

static int BuildAndSend_EDGX_OrderExtendedMsg(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
	t_ShortConverter shortUnion;
	t_IntConverter intUnion;
  char msgContent[40] ;  
   // Unsequence data packet
  shortUnion.value = 65; 
	msgContent[0] = shortUnion.c[1];
	msgContent[1] = shortUnion.c[0];
	
  msgContent[2] = EDGE_MEP_UNSEQUENCED_DATA_PACKET_TYPE;
  
  // Message Type
  msgContent[3] = 'N';
	
 // Order Token
  __StrWithSpaceRightPad(token, 14, &msgContent[4]);
  
	/* Buy/Sell indicator */
	msgContent[18] = *sd;
	
	/* Share */
	intUnion.value = atoi(qty);
	msgContent[19] = intUnion.c[3];
	msgContent[20] = intUnion.c[2];
	msgContent[21] = intUnion.c[1];
	msgContent[22] = intUnion.c[0];
	
	/* Symbol: 6 Bytes Alpha */
	strcncpy((unsigned char *)&msgContent[23], symbol, ' ', 6);
	
	/* Price: 4 bytes Integer, The limit price of the order. The price is a 5 digit whole number followed by a 5 decimal digits. */
	if (fge(atof(pr), 1.00))
	{
		if (*sd == 'B')	//BUY SIDE
		{
			intUnion.value = (int)(atof(pr) * 100.00 + 0.500001) * 1000;
		}
		else
		{
			intUnion.value = (int)(atof(pr) * 100.00 + 0.000001) * 1000;
		}
	}
	else
	{
		intUnion.value = (int)(atof(pr) * 100000.00 + 0.500001);
	}
	msgContent[29] = intUnion.c[3];
	msgContent[30] = intUnion.c[2];
	msgContent[31] = intUnion.c[1];
	msgContent[32] = intUnion.c[0];
	
	/* Time in Force: 1 byte Interger, 0 = IOC, 1 = Day, 2 = Fill or Kill */
	msgContent[33] = strcmp(tf, "DAY") ==0 ? 1 : 0;
	
	/* Display: 1 byte Alpha, �Y� = Displayed, �N� = Hidden, �A� = Displayed with Attribution */
	msgContent[34] = 'Y';

	/* Special order type: 1 byte Alpha
		�M�= MidPoint Match EDGX/MidPoint Peg EDGA
		�D� = Midpoint Discretionary Order � EDGA Only*
		�U� = Route Peg Order
		Re-Pricing Options:
			�S� = Hide Not Slide
			�P� = Price Adjust
			�R� = Single Re-Price
			�C� = Cancel Back
		(See Appendix B for details on Re-Pricing options.)
		*Midpoint Discretionary: Orders require a �Y� in the Display field.
	
		Enter a space for this field if	not applicable.
	*/
	msgContent[35] = ' ';
	
	/* Extended hours eligible: 1 byte alpha
		�R� = Regular Session Only
		�P� = Pre-Market and Regular Session Eligible
		�A� = Regular Session and Post-Market Eligible
		�B� = All Sessions Eligible*/
	msgContent[36] = 'B';
	
	/* Capacity: 1 byte Alpha
		�A� = Agency
		�P� = Principal
		�R� = Riskless Principal
	*/
	msgContent[37] = 'P';
	
  // Route out Eligibility 
	msgContent[38] = 'N';
	
	// ISO eligiblity
	msgContent[39] = 'Y';

  // Routing delivery method
  msgContent[40] = ' ';
  
  // Route strategy
  shortUnion.value = 1;
  msgContent[41] = shortUnion.c[1];
  msgContent[42] = shortUnion.c[0];
  
  // Minimum quantity
  intUnion.value = 0;
  msgContent[43] = intUnion.c[3];
  msgContent[44] = intUnion.c[2];
  msgContent[45] = intUnion.c[1];
  msgContent[46] = intUnion.c[0];
  
  // Max floor
  intUnion.value = 0;
  msgContent[47] = intUnion.c[3];
  msgContent[48] = intUnion.c[2];
  msgContent[49] = intUnion.c[1];
  msgContent[50] = intUnion.c[0];
  
  // Peg difference
  msgContent[51] = 0;
  
  // Discretionary offset
  msgContent[52] = 0;
  
  // Expire time
  t_LongConverter longUnion;
  longUnion.value = 0;
  msgContent[53] = longUnion.c[7];
  msgContent[54] = longUnion.c[6];
  msgContent[55] = longUnion.c[5];
  msgContent[56] = longUnion.c[4];
  msgContent[57] = longUnion.c[3];
  msgContent[58] = longUnion.c[2];
  msgContent[59] = longUnion.c[1];
  msgContent[60] = longUnion.c[0];
  
  // Symbol suffix
  strcncpy((unsigned char *)&msgContent[61], "TEST", ' ', 6);

  return SendMsg(connection->socket, &connection->lock, msgContent, 67);  	  
}

int TEST_EDGE_order(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
  //Short or Extend
  int newOrderType = rand() % 2;

  if(newOrderType == 0)
  {
    return BuildAndSend_EDGE_OrderShortMsg(connection, token, symbol, sd, qty, pr, tf);
  }
  else
  {
    return BuildAndSend_EDGX_OrderExtendedMsg(connection, token, symbol, sd, qty, pr, tf);
  }  
}

int TEST_EDGE_cancel(t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{
	t_IntConverter tmpValue;
	t_ShortConverter shortUnion;
  
  char msgContent[22] ;
  memset(msgContent, 0, 19);

  // Unsequence data packet
  shortUnion.value = 19; 
	msgContent[0] = shortUnion.c[1];
	msgContent[1] = shortUnion.c[0];
	
  msgContent[2] = EDGE_MEP_UNSEQUENCED_DATA_PACKET_TYPE;
  
  // Message type is 'X'
	msgContent[3] = 'X';
  
	// Order Token 
  __StrWithSpaceRightPad(token, 14, &msgContent[4]);
  
  // Shares
	tmpValue.value =  atoi(newQty);
	msgContent[18] = tmpValue.c[3];
	msgContent[19] = tmpValue.c[2];
	msgContent[20] = tmpValue.c[1];
	msgContent[21] = tmpValue.c[0];
	
  return SendMsg(connection->socket, &connection->lock, msgContent, 22);   
}

void TEST_EDGE_printInfo(t_Connection* connection, t_DataBlock* data)
{
  int contentOff = 3;
  char type = data->msgContent[contentOff+0];
  char token[14];
  if(data->msgContent[2] != 'H')
  {
    if (type == 'E')
    {
      memcpy(token, &data->msgContent[contentOff+9], 14);
      int share = __builtin_bswap32(*(unsigned int*)(&data->msgContent[contentOff+23]));
      int price = __builtin_bswap32(*(unsigned int*)(&data->msgContent[contentOff+27]));
     
      TraceLog(DEBUG_LEVEL, "[EDGE  FILLED] ClOrdID:%.8s  Share:%d Price:%.5f\n", token, share, price/100000.0);
      return;
    } 
    else if (type == 'J' || type == 'L')
    {
      memcpy(token, &data->msgContent[contentOff+9], 14);
      char reason = data->msgContent[contentOff+23];
      
      TraceLog(DEBUG_LEVEL, "[EDGE REJECT] ClOrdID:%.8s REASON : %c\n", token, reason);
      return;
    }
    else if (type == 'C')
    {
      memcpy(token, &data->msgContent[contentOff+9], 14);
      char reason = data->msgContent[contentOff+27];
      
      TraceLog(DEBUG_LEVEL, "[EDGE CANCELLED] ClOrdID:%.8s REASON : %c\n", token, reason);
      return;
    }
    else if (type == 'A' || type == 'P')
    {
      char symbol[6];
      memcpy(token, &data->msgContent[contentOff+9], 14);
      char side = data->msgContent[contentOff+23];
      memcpy(symbol, &data->msgContent[contentOff+28], 6);
      int share = __builtin_bswap32(*(unsigned int*)(&data->msgContent[contentOff+24]));
      int price = __builtin_bswap32(*(unsigned int*)(&data->msgContent[contentOff+34]));
      
      TraceLog(DEBUG_LEVEL, "[EDGE  ACCEPTED] ClOrdID:%s Side:%s Symbol:%.6s Share:%d Price:%.5f\n", token, side=='B' ? "Buy" : "Sell" , symbol, share, price/100000.0);
      return;
    }
  }
  
}
