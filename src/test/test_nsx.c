#include "sim.h"
#include "global.h"
#include "network.h"
#include "fix_protocol.h"
#include "nsx.h"

static int testExit = 0;

static int BuildCommonMsgHeaderField(t_Connection* connection, char* msg, const char msgType)
{
  int length = 0;
  time_t now;
  time(&now);
  
  length += FIX_AddTagValueChar(&msg[length], FIX_TAG_MSG_TYPE, msgType);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_SENDER_COMPID, connection->userInfo->userName);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_TARGET_COMPID, NSX_COMPID);
  length += FIX_AddTagValueInt(&msg[length], FIX_TAG_MSG_SEQNO, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1));
  length += FIX_AddTagValueTime(&msg[length], FIX_TAG_SENDING_TIME, now);
  
  return length;  
}

static int BuildMsgHeaderField(char* msg, const int bodyLength)
{
  int i, length;
  
  // "8=FIX4.2<SOH>9=23<SOH>"
  sprintf(msg, "%s%d=%d\001", NSX_MSG_BEGIN_STRING, FIX_TAG_BODY_LENGTH, bodyLength);
  length = strlen(msg);
  
  if (FIX_RESERVE_HEADER_LEN > length)
    for (i = 0; i < length; i++)
      msg[FIX_RESERVE_HEADER_LEN - i - 1] = msg[length - i - 1];
      
  return length;
}

static char* BuildMsgTrailerField(char* msg, const int headerLen, const int bodyLength)
{
  int offset;
  unsigned int checksum;
  
  offset = FIX_RESERVE_HEADER_LEN - headerLen;  
  checksum = FIX_CalculateChecksum(&msg[offset], headerLen + bodyLength);
  
  // "10=023<SOH>"
  sprintf(&msg[offset + headerLen + bodyLength], "%d=%03d\001", FIX_TAG_CHECKSUM, checksum);
      
  return &msg[offset];
}

static int BuildAndSendHeartBeat(t_Connection* connection)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_HEARTBEAT);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendLogon(t_Connection* connection)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_LOGON);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 98, 0);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 108, 30);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendLogout(t_Connection* connection)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_LOGOUT);  
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendResendRequest(t_Connection* connection, const int beginSeq, const int endSeq)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_RESEND_REQUEST);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_BEGIN_SEQNO, beginSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_END_SEQNO, endSeq);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendSequenceReset(t_Connection* connection, const int newSeq)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_SEQ_RESET);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_NEW_SEQNO, newSeq);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendReject(t_Connection* connection, const int errSeq)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_REJECT);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_REF_SEQNO, errSeq);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendNewOrder(t_Connection* connection,
    const char* clOrdId, const char* symbol, const char side, const int orderQty, const double price,
    const char tif, const char orderType, const char orderCapacity,  const char* clientId)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  time_t now;
  time(&now);
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_NEW_ORDER, connection->userInfo->userName, NSX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdId);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 21, FIX_HANDL_INST_AUTO_NO_BROKER);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 38, orderQty);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 40, orderType);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 44, price);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 47, orderCapacity);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, side);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 59, tif);
  bodyLen += FIX_AddTagValueTime(&message[bodyLen], 60, now);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 109, clientId);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, NSX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendCancelRequest(t_Connection* connection,
    const char* origClOrdId, const char* clOrdId, const char* symbol, const char side, const int orderQty,
    const char* orderId, const char* clientId,  const char* text)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  time_t now;
  time(&now);
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_CANCEL_ORDER, connection->userInfo->userName, NSX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, origClOrdId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdId);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 38, orderQty);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, side);
  bodyLen += FIX_AddTagValueTime(&message[bodyLen], 60, now);
  if (orderId != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 37, orderId);
  if (clientId != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 109, clientId);
  if (text != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, text);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, NSX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);

  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int HandleCommandLine(t_Connection* connection, t_Parameters* para)
{
  int i;
  
  for (i = 0; i < para->countParameters; i++)
  {
    t_Parameters argv;
    memset(&argv, 0, sizeof(t_Parameters));
    Lrc_Split(para->parameterList[i], ' ', &argv);
    // TraceLog(DEBUG_LEVEL, "*** check parameters = %s; %s; %d\n", para->parameterList[i], argv.parameterList[0], argv.countParameters);
    if (strcmp(argv.parameterList[0], "exit") == 0)
    {
      testExit = 1;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "quit") == 0)
    {
      testExit = 1;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "logon") == 0)
    {
      //send logon
      BuildAndSendLogon(connection);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "logout") == 0)
    {
      //send logout
      BuildAndSendLogout(connection);
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "heartbeat") == 0)
    {
      //handle heartbeat
      BuildAndSendHeartBeat(connection);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "request") == 0)
    {
      //handle resend request
      BuildAndSendResendRequest(connection, 2, 3);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "reset") == 0)
    {
      //handle sequence reset
      BuildAndSendSequenceReset(connection, 2);
      connection->userInfo->outgoingSeq = 1;
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "reject") == 0)
    {
      //handle reject
      BuildAndSendReject(connection, 2);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "order") == 0)
    {
      //handle new order
      if (argv.countParameters < 7)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -order <token> <symbol> <side> <quantity> <price> <tif>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      char *symbol =  argv.parameterList[2];
      char side = !strcmp(argv.parameterList[3], "b")  ? FIX_SIDE_BUY :
                  !strcmp(argv.parameterList[3], "s")  ? FIX_SIDE_SELL :
                  !strcmp(argv.parameterList[3], "ss") ? FIX_SIDE_SELL_SHORT : FIX_SIDE_SELL_SHORT_EXEMPT;
      int quantity = atoi(argv.parameterList[4]);
      double price = atof(argv.parameterList[5]);
      char tif = !strcmp(argv.parameterList[6], "ioc")? FIX_TIMEINFORCE_IOC: FIX_TIMEINFORCE_DAY;
      char clientId[32];
      sprintf(clientId, "client_%d", (int) pthread_self());
      BuildAndSendNewOrder(connection, token, symbol, side, quantity, price, tif, '1', 'A',  clientId);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "cancel") == 0)
    {
      //handle cancel order
      if (argv.countParameters < 5)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -cancel <token> <symbol> <side> <quantity>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      char *symbol =  argv.parameterList[2];
      char side = !strcmp(argv.parameterList[3], "b")? FIX_SIDE_BUY:
                  !strcmp(argv.parameterList[3], "s")? FIX_SIDE_SELL:
                  !strcmp(argv.parameterList[3], "ss")? FIX_SIDE_SELL_SHORT: FIX_SIDE_SELL_SHORT_EXEMPT;
      int quantity = atoi(argv.parameterList[4]);
      char clientId[32];
      sprintf(clientId, "client_%d", (int) pthread_self());
      BuildAndSendCancelRequest(connection, token, "cancel_token", symbol, side, quantity, NULL, clientId, NULL);
      continue;
    }
  }
  
  return 0;
}

static int ParseCommandLine(char* cmdLine, t_Parameters* para)
{
  Lrc_Split(cmdLine, '-', para);
  
  return para->countParameters;  
}

static int GetCommandLine(t_Parameters* para)
{
  char cmdLine[128];
  
  fgets(cmdLine, 128, stdin);
  // if (cmdLine == NULL)
  // {
    // return 0;
  // }

  return ParseCommandLine(cmdLine, para);
}

void* StartNSXTestThread (void* data)
{
  t_Parameters para;
  t_Connection* connection = (t_Connection*) data;
  
  while (1)
  {
    TraceLog(DEBUG_LEVEL, "Get command line >>\n");
    memset(&para, 0, sizeof(t_Parameters));
    GetCommandLine(&para);
    if (HandleCommandLine(connection, &para) == 1)
      break;
    usleep(1000);
  }  

  return NULL;
}

void NSXForTesting ()
{
  struct sockaddr_in server_addr;
  
  int configIdx = 0;
  for (configIdx = 0; configIdx < Configs.countPort; configIdx++)
    if (Configs.portList[configIdx].venue == VENUE_NSX)
      break;

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(Configs.portList[configIdx].port);
  if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot convert net address\n");
    return;
  }
  
  while (!testExit)
  {
    int testSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (testSocket == -1)
    {
      TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
      return;
    }  

    if(connect(testSocket, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Cannot connect to server\n");
      return;
    }
    
    t_UserInfo userInfo;
    t_Connection connection;
  
    strcpy(userInfo.userName, "TEST");
    userInfo.outgoingSeq = 0;
    
    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 1;
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);
    
    pthread_t testThread;
    pthread_create(&testThread, NULL, StartNSXTestThread, &connection);
    
    pthread_join(testThread, NULL);
    
    close(testSocket);
  }
}

