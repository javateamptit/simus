#include "ouch.h"
#include "soupbintcp.h"
#include "ufo.h"


int OUCH_Callback_Filled (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  return OUCH_BuildAndSend_MsgExecuted(connection, item, quantity, (int)price, OUCH_LIQUIDITY_FLAG_REMOVED, matchNum);
}

int OUCH_Callback_SystemCancel(t_Connection* connection, t_QueueItem* item)
{
  return OUCH_BuildAndSend_MsgCancelled(connection, &item->orderEntry, item->orderEntry.quantity - item->cumShare, OUCH_CANCEL_REASON_SYSTEM);
}

int OUCH_Callback_UserCancel(t_Connection* connection, t_QueueItem* item, const int quantity)
{
  return OUCH_BuildAndSend_MsgCancelled(connection, &item->orderEntry, quantity, OUCH_CANCEL_REASON_USER);
}

int OUCH_Callback_CancelReject(t_Connection* connection, t_OrderEntry* orderEntry)
{
  return OUCH_BuildAndSend_MsgCancelReject(connection, orderEntry);
}

int OUCH_GetNewOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  // NASDAQ OMX BX and PSX don't have CustomerType field
  if (msg == NULL || msgLen < OUCH_ENTER_ORDER_MSG_LEN-1 || msgLen > OUCH_ENTER_ORDER_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "OUCH: NewOrder message is null or not match length\n");
    return ERROR;
  }
  
  // Order Token
  memcpy(orderEntry->clOrdID, &msg[1], 14);
  // Buy/Sell Indicator
  orderEntry->side = msg[15];
  // Shares
  orderEntry->quantity = uintAt((unsigned char*)msg, 16);
  // Stock Symbol
  memcpy(orderEntry->symbol, &msg[20], 8);
  // The price of the order
  orderEntry->price = uintAt((unsigned char*)msg, 28);
  // Time In Force
  orderEntry->tif = uintAt((unsigned char*)msg, 32);
  // Firm (Account)
  memcpy(orderEntry->clientId, &msg[36], 4);
  // Display
  orderEntry->displayIndicator =  msg[40];
  // Order Capacity
  orderEntry->displayIndicator =  msg[41];
  // Intermarket Sweep Eligibility
  orderEntry->interMarketSweep = msg[42];
  // Min Qty
  orderEntry->minQty = uintAt((unsigned char*)msg, 43);
  // Cross Type
  orderEntry->orderType =  msg[47];
  // Customer Type can be missing in BX or PSX
  if (msgLen == OUCH_ENTER_ORDER_MSG_LEN)
    orderEntry->customerType =  msg[48];

  return SUCCESS;
}

int OUCH_GetCancelOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  if (msg == NULL || msgLen != OUCH_CANCEL_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "OUCH: CancelOrder message is null or not match length\n");
    return ERROR;
  }
  
  // Order Token
  memcpy(orderEntry->clOrdID, &msg[1], 14);
  // Shares
  orderEntry->quantity = uintAt((unsigned char*)msg, 15);
  
  return SUCCESS;
}

int OUCH_BuildAndSend_MsgReject(t_Connection* connection, t_OrderEntry* orderEntry, const char reason)
{
  t_LongConverter longUnion;
  char message[OUCH_REJECTED_MSG_LEN];
  
  // Message Type
  message[0] = OUCH_REJECTED_MSG_TYPE;
  // Timestamp
  longUnion.value = GetLocalTime_nsec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  memcpy(&message[9], orderEntry->clOrdID, 14);
  // Reject reason
  message[23] = reason;
  
  if (connection->configInfo->type == SOCK_STREAM)
    return SOUPBINTCP_BuildAndSend_SequencedDataPacket(connection, message, OUCH_REJECTED_MSG_LEN);
  else
    return UFO_BuildAndSend_SequencedDataPacket(connection, message, OUCH_REJECTED_MSG_LEN);
}

int OUCH_BuildAndSend_MsgAccepted(t_Connection* connection, t_OrderEntry* orderEntry,
      const char orderState, const char* weightIndicator)
{
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  // NASDAQ OMX PSX don't have WeightIndicator field
  const int msgLen = weightIndicator ? OUCH_ACCEPTED_MSG_LEN : OUCH_ACCEPTED_MSG_LEN-1;
  char message[msgLen];
  
  // Message Type "A" Accepted message
  message[0] = OUCH_ACCEPTED_MSG_TYPE;  
  // Timestamp
  longUnion.value = GetLocalTime_nsec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // The order Token field as entered.
  memcpy(&message[9], orderEntry->clOrdID, 14);
  // Buy/Sell Indicator as entered.
  message[23] = orderEntry->side;
  // Total number of shares accepted.
  intUnion.value = orderEntry->quantity;
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  // Stock Symbol as entered.
  memcpy(&message[28], orderEntry->symbol, 8);
  // Price
  intUnion.value = orderEntry->price;
  message[36] = intUnion.c[3];
  message[37] = intUnion.c[2];
  message[38] = intUnion.c[1];
  message[39] = intUnion.c[0];
  // Time In Force
  intUnion.value = orderEntry->tif;
  message[40] = intUnion.c[3];
  message[41] = intUnion.c[2];
  message[42] = intUnion.c[1];
  message[43] = intUnion.c[0];
  // Firm (Account)
  memcpy(&message[44], orderEntry->clientId, 4);
  // Display
  message[48] = orderEntry->displayIndicator;
  // Order Reference Number
  longUnion.value = orderEntry->orderID;
  message[49] = longUnion.c[7];
  message[50] = longUnion.c[6];
  message[51] = longUnion.c[5];
  message[52] = longUnion.c[4];
  message[53] = longUnion.c[3];
  message[54] = longUnion.c[2];
  message[55] = longUnion.c[1];
  message[56] = longUnion.c[0];
  // The capacity specified on the order
  message[57] = orderEntry->capacity;
  // Intermarket Sweep Eligibility
  message[58] = orderEntry->interMarketSweep;
  // Minimum number of shares to execute on the replacement
  intUnion.value = orderEntry->minQty;
  message[59] = intUnion.c[3];
  message[60] = intUnion.c[2];
  message[61] = intUnion.c[1];
  message[62] = intUnion.c[0];
  // The Cross Type as entered 
  message[63] = orderEntry->orderType;
  // Order State
  message[64] = orderState;
  // BBO Weight indicator can be missing in PSX
  if (weightIndicator)
    message[65] = weightIndicator[0];

  if (connection->configInfo->type == SOCK_STREAM)
    return SOUPBINTCP_BuildAndSend_SequencedDataPacket(connection, message, msgLen);
  else
    return UFO_BuildAndSend_SequencedDataPacket(connection, message, msgLen);
}

int OUCH_BuildAndSend_MsgExecuted(t_Connection* connection, t_QueueItem* item,
      const int quantity, const int price, const char liqidFlag, const long matchNum)
{
  // Update executed shares and total price
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  char message[OUCH_EXECUTED_MSG_LEN];

  // Message Type
  message[0] = OUCH_EXECUTED_MSG_TYPE;  
  // Timestamp
  longUnion.value = GetLocalTime_nsec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  memcpy(&message[9], item->orderEntry.clOrdID, 14);
  // Incremental number of shares executed
  intUnion.value = quantity;
  message[23] = intUnion.c[3];
  message[24] = intUnion.c[2];
  message[25] = intUnion.c[1];
  message[26] = intUnion.c[0];
  // The price at which these shares were executed
  intUnion.value = price;
  message[27] = intUnion.c[3];
  message[28] = intUnion.c[2];
  message[29] = intUnion.c[1];
  message[30] = intUnion.c[0];
  // Liquidity Flag
  message[31] = liqidFlag;
  // Match Number
  longUnion.value = matchNum;
  message[32] = longUnion.c[7];
  message[33] = longUnion.c[6];
  message[34] = longUnion.c[5];
  message[35] = longUnion.c[4];
  message[36] = longUnion.c[3];
  message[37] = longUnion.c[2];
  message[38] = longUnion.c[1];
  message[39] = longUnion.c[0];
  
  if (connection->configInfo->type == SOCK_STREAM)
    return SOUPBINTCP_BuildAndSend_SequencedDataPacket(connection, message, OUCH_EXECUTED_MSG_LEN);
  else
    return UFO_BuildAndSend_SequencedDataPacket(connection, message, OUCH_EXECUTED_MSG_LEN);
}

int OUCH_BuildAndSend_MsgCancelled(t_Connection* connection, t_OrderEntry* orderEntry,
      const int quantity, const char reason)
{
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  char message[OUCH_CANCELLED_MSG_LEN];
  
  // Message Type
  message[0] = OUCH_CANCELLED_MSG_TYPE;  
  // Timestamp
  longUnion.value = GetLocalTime_nsec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  memcpy(&message[9], orderEntry->clOrdID, 14);
  // TODO: Decrement Shares
  intUnion.value = quantity;
  message[23] = intUnion.c[3];
  message[24] = intUnion.c[2];
  message[25] = intUnion.c[1];
  message[26] = intUnion.c[0];
  // Cancel reason
  message[27] = reason;
  
  if (connection->configInfo->type == SOCK_STREAM)
    return SOUPBINTCP_BuildAndSend_SequencedDataPacket(connection, message, OUCH_CANCELLED_MSG_LEN);
  else
    return UFO_BuildAndSend_SequencedDataPacket(connection, message, OUCH_CANCELLED_MSG_LEN);
}


int OUCH_BuildAndSend_MsgCancelReject(t_Connection* connection, t_OrderEntry* orderEntry)
{
  t_LongConverter longUnion;
  char message[OUCH_CANCEL_REJECT_MSG_LEN];

  // Message Type
  message[0] = OUCH_CANCEL_REJECT_MSG_TYPE;  
  // Timestamp
  longUnion.value = GetLocalTime_nsec();
  message[1] = longUnion.c[7];
  message[2] = longUnion.c[6];
  message[3] = longUnion.c[5];
  message[4] = longUnion.c[4];
  message[5] = longUnion.c[3];
  message[6] = longUnion.c[2];
  message[7] = longUnion.c[1];
  message[8] = longUnion.c[0];
  // Order Token
  memcpy(&message[9], orderEntry->clOrdID, 14);
  
  if (connection->configInfo->type == SOCK_STREAM)
    return SOUPBINTCP_BuildAndSend_SequencedDataPacket(connection, message, OUCH_CANCEL_REJECT_MSG_LEN);
  else
    return UFO_BuildAndSend_SequencedDataPacket(connection, message, OUCH_CANCEL_REJECT_MSG_LEN);
}

static int OUCH_Handle_MsgCancelOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  OUCH_GetCancelOrderEntry(&orderEntry, msg, msgLen);
  
  return Simulator_Handle_CancelOrder(connection, &orderEntry, &OUCH_Callback_UserCancel, &OUCH_Callback_CancelReject);
}

static int OUCH_Handle_MsgNewOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  OUCH_GetNewOrderEntry(&orderEntry, msg, msgLen);
  
  if(orderEntry.tif != OUCH_TIF_IOC && orderEntry.tif != OUCH_TIF_DAY)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    OUCH_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Invalid time in force 
  }
  else if(orderEntry.side != OUCH_SIDE_BUY && orderEntry.side != OUCH_SIDE_SELL && orderEntry.side != OUCH_SIDE_SELL_SHORT)
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    OUCH_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Invalid Side
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    OUCH_BuildAndSend_MsgReject(connection, &orderEntry, 'O'); // Order quantity missing/invalid
  }
  else
  {
    OUCH_BuildAndSend_MsgAccepted(connection, &orderEntry,  OUCH_ORDER_STATE_LIVE, OUCH_BBO_WEIGHT_INDICATOR_UNSPECIFIED);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;

    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, OUCH_SIDE_BUY, &OUCH_Callback_Filled);
    //IOC Random
    if (orderEntry.tif == OUCH_TIF_IOC)
    {
      if (hasCross)
      {
        if (item.orderEntry.quantity > item.cumShare)
          OUCH_BuildAndSend_MsgCancelled(connection, &item.orderEntry, item.orderEntry.quantity - item.cumShare, OUCH_CANCEL_REASON_IOC);
      } else {
        Simulator_Handle_RandomIOC(connection, &item, &OUCH_Callback_Filled, &OUCH_Callback_SystemCancel);
      }
    } else // DAY
    {
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
    }
  }
  
  return SUCCESS;
}

static int OUCH_Handle_UnsequencedDataPacket(t_Connection* connection, const char* msg, const int msgLen)
{
  switch(msg[0])
  {
    case OUCH_ENTER_ORDER_MSG_TYPE:
      return OUCH_Handle_MsgNewOrder(connection, msg, msgLen);
      break;
    
    case OUCH_CANCEL_MSG_TYPE:
      return OUCH_Handle_MsgCancelOrder(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "Unknown message type '%c'\n", msg[0]);
      break;
  }
  
  return SUCCESS;
}

static void* OUCH_DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;

  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &OUCH_Callback_Filled, &OUCH_Callback_SystemCancel);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_OUCH(t_Connection* connection, t_DataBlock *dataBlock)
{
  if (connection->configInfo->type != SOCK_STREAM)
  {
    TraceLog(ERROR_LEVEL, "HandleMsg_OUCH: Socket type isn't yet supported\n");
    return ERROR;
  }
  
  switch(dataBlock->msgContent[2])
  {
    case SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received login packet from client\n");
      if(SOUPBINTCP_Handle_LogonPacket(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, OUCH_DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, SOUPBINTCP_HeartbeatThread, (void*)connection);
      }
      break;
    
    case SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received heartbeat packet from client\n");
      break;
    
    case SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logout packet from client\n");
      SOUPBINTCP_Handle_LogoutPacket(connection, dataBlock);
      break;
    
    case SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received unsequenced data packet from client\n");
      int msgLen;
      char* msg = SOUPBINTCP_GetContent_UnsequencedPacket(dataBlock, &msgLen);
      OUCH_Handle_UnsequencedDataPacket(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown packet type '%c'\n", dataBlock->msgContent[2]);
      break;
  }
  return SUCCESS;
}

