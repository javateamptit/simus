/****************************************************************************
** Project name:    US Blink Simulator
** File name:       ouch.h
** Description:     N/A
** Specification:   O*U*C*H 4.2 
**                  Version 4.2
**                  February 25th, 2013
**
** Author:          Thien Nguyen-Tai
** First created:   May 08, 2014
** Last updated:    May 09, 2014
****************************************************************************/

#ifndef __OUCH__
#define __OUCH__

#include "global.h"

#define OUCH_ENTER_ORDER_MSG_LEN                   49
#define OUCH_CANCEL_MSG_LEN                        19
#define OUCH_ACCEPTED_MSG_LEN                      66
#define OUCH_EXECUTED_MSG_LEN                      40
#define OUCH_REJECTED_MSG_LEN                      24
#define OUCH_CANCELLED_MSG_LEN                     27
#define OUCH_CANCEL_REJECT_MSG_LEN                 23

#define OUCH_ENTER_ORDER_MSG_TYPE                  'O'
#define OUCH_CANCEL_MSG_TYPE                       'X'
#define OUCH_ACCEPTED_MSG_TYPE                     'A'
#define OUCH_EXECUTED_MSG_TYPE                     'E'
#define OUCH_REJECTED_MSG_TYPE                     'J'
#define OUCH_CANCELLED_MSG_TYPE                    'C'
#define OUCH_CANCEL_REJECT_MSG_TYPE                'I'

#define OUCH_TIF_IOC                               0
#define OUCH_TIF_DAY                               0x1869F
#define OUCH_SIDE_BUY                              'B'
#define OUCH_SIDE_SELL                             'S'
#define OUCH_SIDE_SELL_SHORT                       'T'
#define OUCH_ORDER_STATE_LIVE                      'L'
#define OUCH_ORDER_STATE_DEAD                      'D'
#define OUCH_BBO_WEIGHT_INDICATOR_UNSPECIFIED      " "

#define OUCH_CANCEL_REASON_USER                    'U'
#define OUCH_CANCEL_REASON_IOC                     'I'
#define OUCH_CANCEL_REASON_SYSTEM                  'Z'

#define OUCH_LIQUIDITY_FLAG_ADDED                  'A'
#define OUCH_LIQUIDITY_FLAG_REMOVED                'R'

/****************************************************************************
- Function name:    OUCH_GetNewOrderEntry
- Input:            N/A
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int OUCH_GetNewOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen);

int OUCH_GetCancelOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen);

int OUCH_Callback_Filled (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum);

int OUCH_Callback_SystemCancel(t_Connection* connection, t_QueueItem* item);

int OUCH_Callback_UserCancel(t_Connection* connection, t_QueueItem* item, const int quantity);

int OUCH_Callback_CancelReject(t_Connection* connection, t_OrderEntry* orderEntry);

/****************************************************************************
- Function name:    OUCH_BuildAndSend_MsgReject
- Input:            N/A
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int OUCH_BuildAndSend_MsgReject(t_Connection* connection, t_OrderEntry* orderEntry, const char reason);

int OUCH_BuildAndSend_MsgAccepted(t_Connection* connection, t_OrderEntry* orderEntry, const char orderState, const char* weightIndicator);

int OUCH_BuildAndSend_MsgExecuted(t_Connection* connection, t_QueueItem* item, const int quantity, const int price, const char liqidFlag, const long matchNum);

int OUCH_BuildAndSend_MsgCancelled(t_Connection* connection, t_OrderEntry* orderEntry, const int quantity, const char reason);

int OUCH_BuildAndSend_MsgCancelReject(t_Connection* connection, t_OrderEntry* orderEntry);

/****************************************************************************
- Function name:    HandleMsg_OUCH
- Input:            'connection': the connection; 'dataBlock': the receive data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Handle NASDAQ OUCH
- Usage:            N/A
****************************************************************************/
int HandleMsg_OUCH(t_Connection* client, t_DataBlock *dataBlock);

#endif //__OUCH__
