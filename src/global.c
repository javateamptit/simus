#include "global.h"

int Simulator_Handle_CancelOrder(t_Connection* connection, t_OrderEntry *cancelEntry, t_UserCancel cancel, t_CancelReject reject)
{
  int errorCode;

  pthread_mutex_lock(&connection->orderQueue.mutex);
    int res = SearchToken(&connection->orderQueue, cancelEntry->clOrdID);
    if(res == ERROR)
    {
      TraceLog(DEBUG_LEVEL, "Simulator_Handle_CancelOrder: Not found order '%s'\n", cancelEntry->clOrdID);
      errorCode = reject(connection, cancelEntry);
    }
    else
    {
      t_QueueItem *item = &connection->orderQueue.buffer[res];
      const int remainShare = item->orderEntry.quantity - item->cumShare;
      if (item->cumShare >= cancelEntry->quantity)
      {
        // If executed quantity greater than new intended quantity, we should cancel all remain shares and remove this item
        errorCode = cancel(connection, item, remainShare);
        RemoveToken(&connection->orderQueue, res);
      }
      else
      {
        // We should cancel the difference quantity of origin shares and new intended shares, and update this item
        errorCode = cancel(connection, item, item->orderEntry.quantity - cancelEntry->quantity);
      }
    }
  pthread_mutex_unlock(&connection->orderQueue.mutex);
  
  return errorCode;
}

int Simulator_Handle_CrossOrder(t_Connection* connection, t_QueueItem *item, const char buySide, t_Fill fill)
{
  int i, hasCross = 0;
  const int venueIndex = connection->configInfo->index;
  t_OrderTokenQueue *queue;
  
  for (i = 0; i < MAX_CONNECTION_PER_PORT; i++)
  {
    if (Connections[venueIndex][i].isAlive == 0)
      continue;
    
    queue = &Connections[venueIndex][i].orderQueue;
    
    pthread_mutex_lock(&queue->mutex);
      while (1)
      {
        int cross = DetectCrossOrderToken(queue, &item->orderEntry, buySide);
        
        if (cross > -1)
        {
          hasCross = 1;
          
          t_QueueItem *crossItem = &queue->buffer[cross];
          int crossQty = crossItem->orderEntry.quantity - crossItem->cumShare;
          int orderQty = item->orderEntry.quantity - item->cumShare;          
          int diffQty = crossQty - orderQty;
          const long match = rand();
          
          if (diffQty == 0)
          {
            fill(connection, item, crossQty, crossItem->orderEntry.price, match);
            
            fill(&Connections[venueIndex][i], crossItem, crossQty, crossItem->orderEntry.price, match);
            RemoveToken(queue, cross);
          }
          else if (diffQty > 0)
          {
            fill(connection, item, orderQty, crossItem->orderEntry.price, match);
            
            fill(&Connections[venueIndex][i], crossItem, orderQty, crossItem->orderEntry.price, match);
          }
          else
          {
            fill(connection, item, crossQty, crossItem->orderEntry.price, match);
            
            fill(&Connections[venueIndex][i], crossItem, crossQty, crossItem->orderEntry.price, match);
            RemoveToken(queue, cross);
            
            // the order entry isn't yet fulfilled, contine dectect cross!!!!
            continue;
          }
        }
        // doesn't have cross item or the order entry is fulfilled, to be check in next step!!!!
        break;
      }
    pthread_mutex_unlock(&queue->mutex);
    
    // doesn't need to check cross item when the order entry is fulfilled
    if(item->orderEntry.quantity == item->cumShare)
      break;
  }
  
  return hasCross;
}

int Simulator_Handle_RandomIOC(t_Connection* connection, t_QueueItem *item, t_Fill fill, t_SystemCancel cancel)
{
  if (item->orderEntry.quantity == item->cumShare)
    return ERROR;
  
  if((rand() % 100) < lrint(connection->configInfo->fillRate * 100.00))
  {
    int hasFill = 0;
    int shareRemain = item->orderEntry.quantity - item->cumShare;
    int numExecute = 1;
    if(shareRemain > 50)
      numExecute = 1+rand()%3;
    
    while(numExecute > 1)
    {
      numExecute--;
      
      hasFill = 1;
      
      int execShare;
      if(shareRemain == 0)
        break;
      else if(shareRemain == 1)
        execShare = 1;
      else
        execShare = 1+ rand() % (shareRemain/2);
        
      shareRemain -= execShare;
      fill(connection, item, execShare, item->orderEntry.price, 0);
    }
    
    // last round
    if(shareRemain != 0)
    {
      if(rand() % 2 && hasFill == 1)
      {
        cancel(connection, item);
      }
      else
      {
        fill(connection, item, shareRemain, item->orderEntry.price, 1);
      }
    }
  }
  else
  {
    cancel(connection, item);
  }
  
  return SUCCESS;
}

int Simulator_Handle_RandomDAY(t_Connection* connection, t_Fill fill, t_SystemCancel cancel)
{
  t_OrderTokenQueue *queue = &connection->orderQueue;

  pthread_mutex_lock(&queue->mutex);
    int res = PickRandomToken(queue);

    if(res != ERROR)
    {
      t_QueueItem *item = &queue->buffer[res];
                
      int fullfill = (rand() % 10 == 1);
      int remainShare = item->orderEntry.quantity - item->cumShare;
      if(remainShare < 100 || fullfill)
      {
        fill(connection, item, remainShare, item->orderEntry.price, 1);
        RemoveToken(queue, res);
      }
      else //partial
      {
        int execShare = 1 + rand() % (remainShare-1);
        
        fill(connection, item, execShare, item->orderEntry.price, 0);
        // UpdateToken(queue, res, item);
      }
    }
  pthread_mutex_unlock(&queue->mutex);
  
  return res;
}

int Simulator_Add_DAYOrder(t_OrderTokenQueue* queue, t_QueueItem *item)
{
  if (item->orderEntry.quantity <= item->cumShare)
  {
    TraceLog(DEBUG_LEVEL, "Simulator_Add_DAYOrder: All shares were executed\n");
    return ERROR;
  }
  
  pthread_mutex_lock(&queue->mutex);
    int errorCode = AddOrderToken(queue, item);
  pthread_mutex_unlock(&queue->mutex);
  
  return errorCode;
}

 void Simulator_Initialize_User(t_Connection* connection)
 {
  connection->userInfo = &UserAccounts[connection->configInfo->index][connection->clientIndex];
  
  connection->userInfo->outgoingSeq = 0;
  connection->userInfo->incomingSeq = 1;
  pthread_mutex_init(&connection->userInfo->outgoingLock, NULL);
 }
