/****************************************************************************
** Project name:    US Blink Simulator
** File name:       nasdaq_psx.h
** Description:     N/A
** Specification:   PSXOUCH42 
**                  Version 4.2
**                  May 01, 2013
**
** Author:          Thien Nguyen-Tai
** First created:   May 09, 2014
** Last updated:    May 15, 2014
****************************************************************************/

#ifndef __NASDAQ_PSX__
#define __NASDAQ_PSX__

#include "global.h"
#include "soupbintcp.h"
#include "ufo.h"
#include "ouch.h"


#if NASDAQ_PSX_USE_UDP
    // UFO protocol
#   define PSX_LOGIN_REQUEST_PACKET_TYPE              UFO_LOGIN_REQUEST_MSG_TYPE
#   define PSX_CLIENT_HEARTBEAT_PACKET_TYPE           UFO_CLIENT_HEARTBEAT_MSG_TYPE
#   define PSX_LOGOUT_REQUEST_PACKET_TYPE             UFO_LOGOUT_REQUEST_MSG_TYPE
#   define PSX_UNSEQUENCED_DATA_PACKET_TYPE           UFO_UNSEQUENCED_MSG_TYPE

#   define PSX_HeartbeatThread                        UFO_HeartbeatThread
#   define PSX_Handle_LogonPacket                     UFO_Handle_LogonPacket
#   define PSX_Handle_LogoutPacket                    UFO_Handle_LogoutPacket

#   define PSX_GetContent_UnsequencedPacket           UFO_GetContent_UnsequencedMsg
#   define PSX_BuildAndSend_SequencedDataPacket       UFO_BuildAndSend_SequencedDataPacket
#else
    // SOUPBINTCP protocol
#   define PSX_LOGIN_REQUEST_PACKET_TYPE              SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE
#   define PSX_CLIENT_HEARTBEAT_PACKET_TYPE           SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE
#   define PSX_LOGOUT_REQUEST_PACKET_TYPE             SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE
#   define PSX_UNSEQUENCED_DATA_PACKET_TYPE           SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE

#   define PSX_HeartbeatThread                        SOUPBINTCP_HeartbeatThread
#   define PSX_Handle_LogonPacket                     SOUPBINTCP_Handle_LogonPacket
#   define PSX_Handle_LogoutPacket                    SOUPBINTCP_Handle_LogoutPacket

#   define PSX_GetContent_UnsequencedPacket           SOUPBINTCP_GetContent_UnsequencedPacket
#   define PSX_BuildAndSend_SequencedDataPacket       SOUPBINTCP_BuildAndSend_SequencedDataPacket
#endif

// OUCH protocol
#define PSX_ENTER_ORDER_MSG_TYPE                   OUCH_ENTER_ORDER_MSG_TYPE
#define PSX_CANCEL_MSG_TYPE                        OUCH_CANCEL_MSG_TYPE

#define PSX_TIF_IOC                                OUCH_TIF_IOC
#define PSX_TIF_DAY                                OUCH_TIF_DAY
#define PSX_SIDE_BUY                               OUCH_SIDE_BUY
#define PSX_SIDE_SELL                              OUCH_SIDE_SELL
#define PSX_SIDE_SELL_SHORT                        OUCH_SIDE_SELL_SHORT
#define PSX_ORDER_STATE_LIVE                       OUCH_ORDER_STATE_LIVE
#define PSX_ORDER_STATE_DEAD                       OUCH_ORDER_STATE_DEAD

#define PSX_CANCEL_REASON_USER                     OUCH_CANCEL_REASON_USER
#define PSX_CANCEL_REASON_IOC                      OUCH_CANCEL_REASON_IOC
#define PSX_CANCEL_REASON_SYSTEM                   OUCH_CANCEL_REASON_SYSTEM

#define PSX_LIQUIDITY_FLAG_ADDED                   OUCH_LIQUIDITY_FLAG_ADDED
#define PSX_LIQUIDITY_FLAG_REMOVED                 OUCH_LIQUIDITY_FLAG_REMOVED

#define PSX_GetNewOrderEntry                       OUCH_GetNewOrderEntry
#define PSX_GetCancelOrderEntry                    OUCH_GetCancelOrderEntry

#define PSX_Callback_Filled                        OUCH_Callback_Filled
#define PSX_Callback_SystemCancel                  OUCH_Callback_SystemCancel
#define PSX_Callback_UserCancel                    OUCH_Callback_UserCancel
#define PSX_Callback_CancelReject                  OUCH_Callback_CancelReject

#define PSX_BuildAndSend_MsgReject                 OUCH_BuildAndSend_MsgReject
#define PSX_BuildAndSend_MsgAccepted               OUCH_BuildAndSend_MsgAccepted
#define PSX_BuildAndSend_MsgExecuted               OUCH_BuildAndSend_MsgExecuted
#define PSX_BuildAndSend_MsgCancelled              OUCH_BuildAndSend_MsgCancelled
#define PSX_BuildAndSend_MsgCancelReject           OUCH_BuildAndSend_MsgCancelReject

int HandleMsg_PSX(t_Connection* client, t_DataBlock *dataBlock);

#endif // __NASDAQ_PSX__
