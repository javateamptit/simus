#ifndef _ARCA_
#define _ARCA_

#include "global.h"
#include "network.h"

#define ARCA_HEARTBEAT_MSG_LEN            12
#define ARCA_LOGON_TYPE_MSG_LEN           48
#define ARCA_LOGON_REJECT_MSG_LEN         60
#define ARCA_ORDER_REJECTED_MSG_LEN_V1    80
#define ARCA_ORDER_ACK_MSG_LEN            48
#define ARCA_ORDER_FILLED_MSG_LEN         88
#define ARCA_SEND_CANCEL_MSG_LEN          40
#define ARCA_ORDER_CANCEL_MSG_LEN         72

#define ARCA_HEARTBEAT_TYPE               '0'
#define ARCA_LOGON_TYPE                   'A'
#define ARCA_LOGON_REJECT_TYPE            'L'
#define ARCA_ORDER_REJECTED_TYPE          '8'
#define ARCA_ORDER_ACK_TYPE               'a'
#define ARCA_ORDER_FILLED_TYPE            '2'
#define ARCA_SEND_CANCEL_TYPE             '4'
#define ARCA_ORDER_CANCEL_TYPE            'F'

#define ARCA_VERSION_PROFILE "L\001D\001G\001E\001a\0012\0016\0014\001\0"

int HandleMsg_ARCA(t_Connection* client, t_DataBlock *dataBlock);

#endif