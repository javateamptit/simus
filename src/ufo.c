#include "ufo.h"
#include "network.h"

char* UFO_GetContent_UnsequencedMsg (t_DataBlock* dataBlock, int* len)
{
  const int contentOffset = UFO_HEADER_MESSAGE_BLOCK_LEN + 1;
  int msgLen = ushortAt((unsigned char*)dataBlock->msgContent, 0) + UFO_HEADER_MESSAGE_BLOCK_LEN;  
  if(msgLen != dataBlock->msgLen)
  {
    TraceLog(ERROR_LEVEL, "UFO_GetContent_UnsequencedMsg: length of message incorrect\n");
    return NULL;
  }
  
  char msgType = dataBlock->msgContent[UFO_HEADER_MESSAGE_BLOCK_LEN];
  if(msgType != UFO_UNSEQUENCED_MSG_TYPE)
  {
    TraceLog(ERROR_LEVEL, "UFO_GetContent_UnsequencedMsg: type of message incorrect\n");
    return NULL;
  }
  
  *len = msgLen - contentOffset;
  
  return &dataBlock->msgContent[contentOffset];
}

static void UFO_Build_MsgBlock (char* msgBlock, const char* data, const int dataLen)
{
  t_ShortConverter shortUnion;
  
  // Message Block Length
  shortUnion.value = dataLen;
  msgBlock[0] = shortUnion.c[1];
  msgBlock[1] = shortUnion.c[0];
  // Message Block Data
  memcpy(&msgBlock[2], data, dataLen);
}

int UFO_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection)
{
  t_IntConverter intUnion;
  char message[UFO_SERVER_HEARTBEAT_PACKET_LEN];
  
  // Packet type is 'S'
  message[0] = UFO_SERVER_HEARTBEAT_PACKET_TYPE;
  // Sequence Number
  intUnion.value = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[1] = intUnion.c[3];
  message[2] = intUnion.c[2];
  message[3] = intUnion.c[1];
  message[4] = intUnion.c[0];
  // Number of messages in this packet
  message[5] = 0;
  message[6] = 0;
  
  return SendMsgTo(connection->socket, &connection->lock, message, UFO_SERVER_HEARTBEAT_PACKET_LEN, (struct sockaddr*)&connection->clientAddr, sizeof(struct sockaddr_in));
}

int UFO_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId)
{
  t_IntConverter intUnion;
  char message[UFO_LOGIN_ACCEPTED_PACKET_LEN];

  // Packet type is 'A'
  message[0] = UFO_LOGIN_ACCEPTED_PACKET_TYPE;
  // The session ID
  __StrWithSpaceRightPad(sessionId, 10, &message[1]);
  // Sequence Number
  intUnion.value = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[11] = intUnion.c[3];
  message[12] = intUnion.c[2];
  message[13] = intUnion.c[1];
  message[14] = intUnion.c[0];
  
  return SendMsgTo(connection->socket, &connection->lock, message, UFO_LOGIN_ACCEPTED_PACKET_LEN, (struct sockaddr*)&connection->clientAddr, sizeof(struct sockaddr_in));
}

int UFO_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason)
{
  char message[UFO_LOGIN_REJECTED_PACKET_LEN];

  // Packet type is 'J'
  message[0] = UFO_LOGIN_REJECTED_PACKET_TYPE;
  // Reject Reason Code
  message[1] = reason;
  
  return SendMsgTo(connection->socket, &connection->lock, message, UFO_LOGIN_REJECTED_PACKET_LEN, (struct sockaddr*)&connection->clientAddr, sizeof(struct sockaddr_in));
}

int UFO_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;
  const int packetLen = msgLen + UFO_HEADER_SEQUENCED_PACKET_LEN + UFO_HEADER_MESSAGE_BLOCK_LEN;
  char message[packetLen];

  // Packet type is 'S'
  message[0] = UFO_SEQUENCED_DATA_PACKET_TYPE;
  // Sequence Number
  intUnion.value = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[1] = intUnion.c[3];
  message[2] = intUnion.c[2];
  message[3] = intUnion.c[1];
  message[4] = intUnion.c[0];
  // Number of messages in this packet
  shortUnion.value = UFO_MAX_NUMBER_MESSAGES;
  message[5] = shortUnion.c[1];
  message[6] = shortUnion.c[0];
  // Message block
  UFO_Build_MsgBlock(&message[UFO_SEQUENCED_MESSAGE_BLOCK_OFFSET], msg, msgLen);
  
  return SendMsgTo(connection->socket, &connection->lock, message, packetLen, (struct sockaddr*)&connection->clientAddr, sizeof(struct sockaddr_in));
}

static int UFO_BuildAndSend_UpstreamPacket (t_Connection* connection, const char* data, const int dataLen)
{
  t_ShortConverter shortUnion;
  const int packetLen = dataLen + UFO_HEADER_MESSAGE_BLOCK_LEN;
  char message[packetLen];
  
  // Message Block Length
  shortUnion.value = dataLen;
  message[0] = shortUnion.c[1];
  message[1] = shortUnion.c[0];
  // Message Block Data
  memcpy(&message[2], data, dataLen);
  
  return SendMsgTo(connection->socket, &connection->lock, message, packetLen, (struct sockaddr*)&connection->clientAddr, sizeof(struct sockaddr_in));
}

int UFO_BuildAndSend_ClientHeartbeatMsg (t_Connection* connection)
{
  char message[UFO_CLIENT_HEARTBEAT_MSG_LEN];

  // Message type is 'R'
  message[0] = UFO_CLIENT_HEARTBEAT_MSG_TYPE;
  
  return UFO_BuildAndSend_UpstreamPacket(connection, message, UFO_CLIENT_HEARTBEAT_MSG_LEN);
}

int UFO_BuildAndSend_LoginRequestMsg (t_Connection* connection, const char* userName, const char* password, const char* sessionId)
{
  char message[UFO_LOGIN_REQUEST_MSG_LEN];

  // Message type is 'L'
  message[0] = UFO_LOGIN_REQUEST_MSG_TYPE;
  // Username
  __StrWithSpaceRightPad(userName, 6, &message[1]);
  // Password
  __StrWithSpaceRightPad(password, 10, &message[7]);
  // Requested Session
  __StrWithSpaceRightPad(sessionId, 10, &message[17]);
    
  return UFO_BuildAndSend_UpstreamPacket(connection, message, UFO_LOGIN_REQUEST_MSG_LEN);
}

int UFO_BuildAndSend_UnsequencedMsg (t_Connection* connection, const char* msg, const int msgLen)
{
  const int lenth = msgLen + UFO_HEADER_UNSEQUENCED_MSG_LEN;
  char message[lenth];

  // Message type is 'U'
  message[0] = UFO_UNSEQUENCED_MSG_TYPE;
  // message content
  memcpy(&message[1], msg, msgLen);
  
  return UFO_BuildAndSend_UpstreamPacket(connection, message, lenth);
}

int UFO_BuildAndSend_LogoutRequestMsg (t_Connection* connection)
{
  char message[UFO_LOGOUT_REQUEST_MSG_LEN];

  // Message type is 'O'
  message[0] = UFO_LOGOUT_REQUEST_MSG_TYPE;
  
  return UFO_BuildAndSend_UpstreamPacket(connection, message, UFO_LOGOUT_REQUEST_MSG_LEN);
}

int UFO_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  const char* logonData = &dataBlock->msgContent[UFO_HEADER_MESSAGE_BLOCK_LEN];
  char sessionId[11] = "\0";
  
  memcpy(connection->userInfo->userName, &logonData[1], 6);
  
  memcpy(connection->userInfo->password, &logonData[7], 10);  
  // TODO: check userName and password
  
  memcpy(sessionId, &logonData[17], 10);  
  // TODO: check session id 
    
  return UFO_BuildAndSend_LoginAcceptedPacket(connection, sessionId);
}

int UFO_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  connection->isAlive = 0;
  
  return SUCCESS;
}

void* UFO_HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    UFO_BuildAndSend_ServerHeartbeatPacket(connection);
    sleep(1); // The server heartbeat interval is 1 second
  }
  
  TraceLog(DEBUG_LEVEL, "Close UFO_HeartbeatThread\n");
  return NULL;
}
