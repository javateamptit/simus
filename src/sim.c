#include "sim.h"

#include "global.h"
#include "network.h"

#include "ouch.h"
#include "nasdaq_rash.h"
#include "nyse.h"
#include "arca.h"
#include "bats_boe.h"
#include "edge.h"
#include "fix_protocol.h"
#include "nsx.h"
#include "chx.h"
#include "lava.h"
#include "nasdaq_psx.h"

#include <stdlib.h>

t_Configs Configs;
t_MsgHandler MsgHandler;
t_Connection Connections[MAX_VENUE_TYPE][MAX_CONNECTION_PER_PORT];
t_UserInfo UserAccounts[MAX_VENUE_TYPE][MAX_CONNECTION_PER_PORT];

int HeaderBytes[MAX_VENUE_TYPE] = {  4,  // ARCA
                2,  // OUCH
                2,  // BX
                2,  // RASH
                4,  // NYSE
                4,  // BATSZ
                3,  // EDGX
                3,  // EDGA
                4,  // BYX
                FIX_RESERVE_HEADER_LEN,  // CHX
                2,  // PSX
                FIX_RESERVE_HEADER_LEN,  // NSX
                LFBOE_HEARDER_PACKET_LEN,   // LAVA
              }; 
              
char *VenueName[MAX_VENUE_TYPE] = {"ARCA", "OUCH", "BX", "RASH", "NYSE", "BATSZ", "EDGX", "EDGA", "BYX", "CHX", "PSX", "NSX", "LAVA"};

static void *StartClient(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  int venue = connection->configInfo->venue;
  
  while(connection->isAlive)
  {
    t_DataBlock dataBlock;
    
    if(MsgHandler.receiveMsg[venue](connection, &dataBlock) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "%s: receive msg failed\n", VenueName[venue]);
      break;
    }
    
    if(MsgHandler.handleMsg[venue](connection, &dataBlock) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "%s: handle msg failed\n", VenueName[venue]);
      break;
    }
  }
  
  CloseConnection(connection);
  
  return NULL;
}

static void *StartServer(void* arg)
{
  t_ServerHandler *serverHandler = (t_ServerHandler*)arg;
  
  int serverSocket = serverHandler->serverSock;
  int venueIdx = serverHandler->index;
  int venue = Configs.portList[venueIdx].venue;
  unsigned short port = Configs.portList[venueIdx].port;
  
  struct sockaddr_in serverAddr;  
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);;
  serverAddr.sin_port = htons(port);
    
  if(bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: bind socket on port %d\n", VenueName[venue], port);
    return NULL;
  }

  if(listen(serverSocket, MAX_CONNECTION_PER_PORT) == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: listen socket on port %d\n", VenueName[venue], port);
    return NULL;
  }
  
  while(1)
  {
    int clientSocket;
    struct sockaddr_in clientAddress;
    socklen_t clientAddrLen = sizeof(clientAddress);
    memset(&clientAddress, 0, clientAddrLen);
    
    //TraceLog(DEBUG_LEVEL, "%s: Waiting for connection from client on port %d ...\n", VenueName[venue], port);
    
    clientSocket = accept(serverSocket, (struct sockaddr*)&clientAddress, &clientAddrLen);
    
    if (clientSocket != -1)
    {
      TraceLog(DEBUG_LEVEL, "%s: --> New client was accepted on port %d\n", VenueName[venue], port);
      
      int index, slot = -1;
      
      t_Connection* connection = NULL;
      
      for(index = 0; index < MAX_CONNECTION_PER_PORT; index++)
      {
        connection = &Connections[venueIdx][index];
        if(connection->isAlive == 0)
        {
          slot = index;
          break;
        }
      }
  
      if(slot != -1)
      {
        connection->configInfo = &Configs.portList[venueIdx];
        connection->userInfo = NULL;
        connection->clientIndex = slot;
        connection->socket = clientSocket;
        connection->isAlive = 1;
        connection->executionNumber = 0;
        connection->orderID = 1;
        InitOrderTokenQueue(&connection->orderQueue);
        pthread_spin_init(&connection->lock, 0);
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Pool on port %d is full\n", VenueName[venue], port);
        close(clientSocket);
        continue;
      }
      
      int iVal = 1;
      setsockopt( clientSocket, IPPROTO_TCP, TCP_NODELAY, (void *)&iVal, sizeof(iVal));

      pthread_t tClient;
      pthread_create(&tClient, NULL, StartClient, connection);      
    }
    else
    {
      TraceLog(ERROR_LEVEL, "accept clientSocket\n");
    }
  }
   
  if(serverSocket > -1)
  {
    close(serverSocket);
  }
  
  return NULL;
}

static void *StartClientUDP(void* arg)
{
  t_CompData *compData = (t_CompData*)arg;
  
  t_Connection *connection = &Connections[compData->venueIndex][compData->clientIndex];
  int venue = connection->configInfo->venue;
  
  t_DataBlock dataBlock;
  memcpy(&dataBlock, &compData->data, sizeof(t_DataBlock));
  
  if(MsgHandler.handleMsg[venue](connection, &dataBlock) == ERROR)
  {
    TraceLog(ERROR_LEVEL, "%s: handle msg failed\n", VenueName[venue]);
  }
  
  return NULL;
}

static void *StartServerUDP(void* arg)
{
  t_ServerHandler *serverHandler = (t_ServerHandler*)arg;
  
  int serverSocket = serverHandler->serverSock;
  int venueIdx = serverHandler->index;
  int venue = Configs.portList[venueIdx].venue;
  unsigned short port = Configs.portList[venueIdx].port;
  
  // bind to server port
  struct sockaddr_in serverAddr;  
  
  memset((char*) &serverAddr, 0, sizeof(struct sockaddr_in));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_addr.s_addr = htonl(INADDR_ANY);;
  serverAddr.sin_port = htons(port);
    
  if(bind(serverSocket, (struct sockaddr*)&serverAddr, sizeof(serverAddr)) == -1)
  {
    TraceLog(ERROR_LEVEL, "%s: bind socket on port %d\n", VenueName[venue], port);
    return NULL;
  }

  while(1)
  {
    t_CompData compData;
    struct sockaddr_in clientAddr;
    int addrLen = sizeof(struct sockaddr_in);
    
    if(ReceiveMsgFrom(serverSocket, &compData.data, (struct sockaddr*)&clientAddr, &addrLen) == SUCCESS)
    {
      int index, slot = -1;      
      t_Connection* connection = NULL;
      
      for(index = MAX_CONNECTION_PER_PORT-1; index >= 0; index--)
      {
        connection = &Connections[venueIdx][index];
        if(connection->isAlive == 1)
        {
          if (  connection->clientAddr.sin_family == clientAddr.sin_family
            &&  connection->clientAddr.sin_port == clientAddr.sin_port
            &&  connection->clientAddr.sin_addr.s_addr == clientAddr.sin_addr.s_addr)
          {
            slot = index;
            break;
          }
        }
        else
        {
          slot = index;
        }
      }
      
      if(slot != -1)
      {
        if (connection->isAlive == 0)
        {
          connection->configInfo = &Configs.portList[venueIdx];
          connection->userInfo = NULL;
          connection->clientIndex = slot;
          memcpy(&connection->clientAddr, &clientAddr, sizeof(struct sockaddr_in));
          connection->socket = serverSocket;
          connection->isAlive = 1;
          connection->executionNumber = 0;
          connection->orderID = 1;
          InitOrderTokenQueue(&connection->orderQueue);
          pthread_spin_init(&connection->lock, 0);
        }
      }
      else
      {
        TraceLog(DEBUG_LEVEL, "%s: Pool on port %d is full\n", VenueName[venue], port);
        continue;
      }
      
      compData.venueIndex = venueIdx;
      compData.clientIndex = slot;
      
      pthread_t tClient;
      pthread_create(&tClient, NULL, StartClientUDP, &compData);        
    }
    else
    {
      TraceLog(ERROR_LEVEL, "receive message from clientSocket\n");
    }
  }
  
  if(serverSocket > -1)
  {
    close(serverSocket);
  }
  
  return NULL;
}

static int CreateServerSocket(int *serverSockList)
{
  int i;
  for(i = 0; i < Configs.countPort; i++)
  {
    serverSockList[i] = socket(AF_INET, Configs.portList[i].type, 0);
    if(serverSockList[i] == -1)
    {
      TraceLog(ERROR_LEVEL, "create ServerSocket");
      return ERROR;
    }
    
    const int optVal = 1;
    const socklen_t optLen = sizeof(optVal);
    setsockopt(serverSockList[i], SOL_SOCKET, SO_REUSEADDR, (void*) &optVal, optLen);
  }
  
  return 0;
}

static void RegisterHandleFunctions()
{
  MsgHandler.receiveMsg[VENUE_ARCA]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_OUCH]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_BX]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_NYSE]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_BATSZ]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_EDGX]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_EDGA]   = ReceiveMsg;
  MsgHandler.receiveMsg[VENUE_RASH]   = ReceiveMsg_RASH;
  MsgHandler.receiveMsg[VENUE_BYX]   = ReceiveMsg;  //<thiennt> added
  MsgHandler.receiveMsg[VENUE_NSX]   = ReceiveMsg_FIX;  //<thiennt> added
  MsgHandler.receiveMsg[VENUE_CHX]   = ReceiveMsg_FIX;  //<thiennt> added
  MsgHandler.receiveMsg[VENUE_LAVA]   = ReceiveMsg;  //<thiennt> added
  MsgHandler.receiveMsg[VENUE_PSX]   = ReceiveMsg;  //<thiennt> added
  
  MsgHandler.handleMsg[VENUE_ARCA]  = HandleMsg_ARCA;
  MsgHandler.handleMsg[VENUE_OUCH]  = HandleMsg_OUCH;
  MsgHandler.handleMsg[VENUE_BX]    = HandleMsg_OUCH;
  MsgHandler.handleMsg[VENUE_NYSE]  = HandleMsg_NYSE;
  MsgHandler.handleMsg[VENUE_BATSZ]  = HandleMsg_BATS;
  MsgHandler.handleMsg[VENUE_EDGX]  = HandleMsg_EDGE;
  MsgHandler.handleMsg[VENUE_EDGA]  = HandleMsg_EDGE;
  MsgHandler.handleMsg[VENUE_RASH]  = HandleMsg_RASH;
  MsgHandler.handleMsg[VENUE_BYX]    = HandleMsg_BATS;  //<thiennt> added, same as BATSZ
  MsgHandler.handleMsg[VENUE_NSX]    = HandleMsg_NSX;  //<thiennt> added
  MsgHandler.handleMsg[VENUE_CHX]    = HandleMsg_CHX;  //<thiennt> added
  MsgHandler.handleMsg[VENUE_LAVA]  = HandleMsg_LFBOE;  //<thiennt> added
  MsgHandler.handleMsg[VENUE_PSX]  = HandleMsg_PSX;  //<thiennt> added
}

static int GetIndexFromVenueName(char *venue)
{
  if(strcmp(venue, "ARCA") == 0)
  {
    return VENUE_ARCA;
  }
  else if(strcmp(venue, "OUCH") == 0)
  {
    return VENUE_OUCH;
  }
  else if(strcmp(venue, "BX") == 0)
  {
    return VENUE_BX;
  }
  else if(strcmp(venue, "RASH") == 0)
  {
    return VENUE_RASH;
  }
  else if(strcmp(venue, "NYSE") == 0)
  {
    return VENUE_NYSE;
  }
  else if(strcmp(venue, "BATSZ") == 0)
  {
    return VENUE_BATSZ;
  }
  else if(strcmp(venue, "EDGX") == 0)
  {
    return VENUE_EDGX;
  }
  else if(strcmp(venue, "EDGA") == 0)
  {
    return VENUE_EDGA;
  }
  else if(strcmp(venue, "BYX") == 0)
  {
    return VENUE_BYX;
  }
  else if(strcmp(venue, "CHX") == 0)
  {
    return VENUE_CHX;
  }
  else if(strcmp(venue, "PSX") == 0)
  {
    return VENUE_PSX;
  }
  else if(strcmp(venue, "NSX") == 0)
  {
    return VENUE_NSX;
  }
  else if(strcmp(venue, "LAVA") == 0)
  {
    return VENUE_LAVA;
  }
  else
  {
    return ERROR;
  }
}

static int ParsePortVenues(int argc, char **argv)
{
  t_Parameters parameters;
  
  memset(&Configs, 0, sizeof(Configs));
  
  int i, j, countPort;
  double fillRate = 0.50;
  for(i = 1; i < argc; i++)
  {
    Lrc_Split(argv[i], ':', &parameters);
    
    if (parameters.countParameters == 2) // Venue:port
    {
      countPort = Configs.countPort++;
      if (countPort == MAX_VENUE_TYPE)
      {
        TraceLog(ERROR_LEVEL, "List of port is full, size limit = %d\n", MAX_VENUE_TYPE);
        return ERROR;
      }
      
      int venue = GetIndexFromVenueName(parameters.parameterList[0]);
      if(venue == ERROR)
      {
        TraceLog(ERROR_LEVEL, "%s: Unknown venue\n", parameters.parameterList[0]);
        return ERROR;
      }
      
      Configs.portList[countPort].index = countPort;
      Configs.portList[countPort].port = atoi(parameters.parameterList[1]);
      Configs.portList[countPort].venue = venue;
    #if NASDAQ_PSX_USE_UDP
      Configs.portList[countPort].type = (venue == VENUE_PSX)? SOCK_DGRAM : SOCK_STREAM;
    #else // NASDAQ_PSX_USE_UDP
      Configs.portList[countPort].type = SOCK_STREAM;
    #endif // NASDAQ_PSX_USE_UDP
      Configs.portList[countPort].fillRate = -0.1;
      
      TraceLog(DEBUG_LEVEL, "%s:%d\n", VenueName[venue], Configs.portList[countPort].port);
    }
    else // fill rate
    {
      fillRate = atof(argv[i]);
      TraceLog(DEBUG_LEVEL, "Fill rate = %lf\n", fillRate);
    }
  }
  
  // Default fill rate
  for (j = 0; j < Configs.countPort; j++)
  {
    if (Configs.portList[j].fillRate < 0.0)
    {
      Configs.portList[j].fillRate = fillRate;
    }
  }
  
  return SUCCESS;
}

static void PrintHelp(char* progname)
{
  printf("Usage:\t%s <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <venue:port> <fill fraction>\n"\
            "\tvenue:  string type, venue name, consist of %d venues: ARCA, OUCH, BX, RASH, NYSE, BATSZ, EDGX, EDGA, BYX, CHX, NSX, LAVA, PSX\n"\
            "\tport:  string type, port to listen on, e.g. 12345\n"\
            "\tfill fraction:    double type, range 0.00 to 1\n"\
            "Note: one-port/venue can be used for multiple client (connect to same port)\n"\
    , progname, MAX_VENUE_TYPE);
}

void NSXForTesting ();
void CHXForTesting ();
void LFBOEForTesting ();
void BATSForTesting ();
void TEST_Run ();

int main(int argc, char** argv)
{  
  if (ParsePortVenues(argc, argv) == ERROR)
  {
    PrintHelp(argv[0]);
    return 0;
  }
  
  if (Configs.countPort < 1)
  {
    PrintHelp(argv[0]);
    return 0;
  }
  
  // initialize random seed:
  srand (time(NULL));
  
  RegisterHandleFunctions();
  
  int serverSockList[Configs.countPort];
  CreateServerSocket(serverSockList);

  pthread_t threadList[Configs.countPort];
  t_ServerHandler serverHandler[Configs.countPort];
  
  int i;
  for(i = 0; i < Configs.countPort; i++)
  {

    memset(&serverHandler[i], 0, sizeof(t_ServerHandler));
    
    serverHandler[i].index = i;
    serverHandler[i].serverSock = serverSockList[i];
    
    if(Configs.portList[i].type == SOCK_STREAM)
      pthread_create(&threadList[i], NULL, StartServer, &serverHandler[i]);
    else
      pthread_create(&threadList[i], NULL, StartServerUDP, &serverHandler[i]);
      
    usleep(10000);
  }
  
  // <thiennt> added for testing
  TEST_Run();
  
  for(i = 0; i < Configs.countPort; i++)
  {
    pthread_join(threadList[i], NULL);
  }
  
  return 0;
}

