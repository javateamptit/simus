#include "utility.h"


long GetLocalTime_usec()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  
  struct tm local;
  localtime_r(&tv.tv_sec, &local);
      
  long lt_sec = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  return lt_sec * 1000000 + tv.tv_usec;
}

long GetLocalTime_msec()
{
  return GetLocalTime_usec() / 1000;
}

long GetLocalTime_nsec()
{
  return GetLocalTime_usec() * 1000;
}

char *GetLocalTimeString(char *timeString)
{
  time_t now;

  time(&now);

  struct tm *l_time;

  // Convert to local time
  l_time = (struct tm *)localtime(&now);

  // Get time string
  strftime(timeString, 16, "%H:%M:%S", l_time);
  timeString[8] = 0;

  return timeString;
}

void LrcPrintfLogFormat(int level, char *fmt, ...)
{
  char formatedStr[5120];
#ifdef RELEASE_MODE
  switch (level)
  {
    case DEBUG_LEVEL:
      strcpy(formatedStr, DEBUG_STR);
      break;
    case NOTE_LEVEL:
      strcpy(formatedStr, NOTE_STR);
      break;
    case WARN_LEVEL:
      strcpy(formatedStr, WARN_STR);
      break;
    case ERROR_LEVEL:
      strcpy(formatedStr, ERROR_STR);
      break;
    case FATAL_LEVEL:
      strcpy(formatedStr, FATAL_STR);
      break;
    case USAGE_LEVEL:
      strcpy(formatedStr, USAGE_STR);
      break;
    case STATS_LEVEL:
      strcpy(formatedStr, STATS_STR);
      break;
    default:
      formatedStr[0] = 0;
      break;
  }
#else //DEBUG MODE
  GetLocalTimeString(formatedStr);
  strcat(formatedStr, " ");
  
  switch (level)
  {
    case DEBUG_LEVEL:
      strcat(formatedStr, DEBUG_STR);
      break;
    case NOTE_LEVEL:
      strcat(formatedStr, NOTE_STR);
      break;
    case WARN_LEVEL:
      strcat(formatedStr, WARN_STR);
      break;
    case ERROR_LEVEL:
      strcat(formatedStr, ERROR_STR);
      break;
    case FATAL_LEVEL:
      strcat(formatedStr, FATAL_STR);
      break;
    case USAGE_LEVEL:
      strcat(formatedStr, USAGE_STR);
      break;
    case DEV_LEVEL:
      strcat(formatedStr, DEV_STR);
      break;
    case STATS_LEVEL:
      strcat(formatedStr, STATS_STR);
      break;
    case VERBOSE_LEVEL:
      strcat(formatedStr, VERBOSE_STR);
      break;
    default:
      formatedStr[0] = 0;
      break;
  }
#endif

  strcat(formatedStr, fmt);
  
  va_list ap;
  va_start (ap, fmt);
  
  // vprintf (fmt, ap);
  vprintf (formatedStr, ap);

  va_end (ap);

  fflush(stdout);
}

int Lrc_itoa(int value, char *strResult)
{
  char temp[11];
  register int index = 0;
  register int i = 0;

  if (value > 0)
  {
    while (value != 0)
    {
      temp[index] = (48 + value % 10);
      value = value / 10;
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;

    return index;
  }
  else if (value < 0)
  {
    *strResult++ = '-';

    value = -1 * value;

    while (value != 0)
    {
      temp[index] = (char)(48 + value % 10);
      value = value / 10;
      index++;
    }

    for (i = index - 1; i > -1; i--)
    {
      *strResult++ = (char)temp[i];
    }

    *strResult = 0;

    return (index + 1);
  }
  else
  {
    strResult[0] = 48;
    strResult[1] = 0;

    return 1;
  }
}

int Lrc_itoaf(int value, const char pad, const int len, char *strResult)
{
  register int i = 0;
  if (value == 0)
  {
    for (i = 0; i < len; i++)
    {
      strResult[i] = pad;
    }
    strResult[len] = 0;

    return len;
  }

  register int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10;
    index++;
  }

  for (i = 0; i < len - index; i++)
  {
    strResult[i] = pad;
  }

  strResult[len] = 0;

  return len;
}

int Lrc_itoafl(int value, const char pad, const int len, char * strResult)
{
  char temp[len + 1];

  register int i = 0;
  if (value == 0)
  {
    strResult[0] = '0';

    for (i = 1; i < len; i++)
    {
      strResult[i] = pad;
    }
    strResult[len] = 0;

    return len;
  }

  register int index = 0;

  while (value != 0)
  {
    temp[index] = 48 + value % 10;
    value = value / 10;  
    index++;
  }

  for (i = 0; i < index; i++)
  {
    strResult[i] = temp[index - i - 1];
  }
  
  for (i = index; i < len; i++)
  {
    strResult[i] = pad;
  }

  strResult[len] = 0;
  
  return len;
}

int Lrc_atoi(const char * buffer, int len)
{
  register int retval = 0;

  while (*buffer == 32)
  {
    buffer++;
    len--;
  }
  
  for (; len > 0; buffer++, len--)
  {
    retval = retval * 10 + ((*buffer) - 48);
  }
  return retval;
}

void __iToStrWithLeftPad(int value, const char pad, const int len, char *strResult)
{
  register int i = 0;
  if (value == 0)
  {
    while (i < len) strResult[i++] = pad;
    strResult[len - 1] = '0';
    return;
  }

  register int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10;
    index++;
  }

  while (i < len-index) strResult[i++] = pad;
  
  return;
}

void __lToStrWithLeftPad(unsigned long value, const char pad, const int len, char *strResult)
{
  register int i = 0;
  if (value == 0)
  {
    while (i < len) strResult[i++] = pad;
    strResult[len - 1] = '0';
    return;
  }

  register int index = 0;

  while (value != 0)
  {
    strResult[len - index - 1] = 48 + value % 10;
    value = value / 10;
    index++;
  }

  while (i < len-index) strResult[i++] = pad;
  
  return;
}

int __StrWithSpaceRightPad(const char* str, const char len, char* strResult)
{
  return snprintf(strResult, len, "%-*s", len, str);
}

int __StrWithSpaceLeftPad(const char* str, const char len, char* strResult)
{
  return snprintf(strResult, len, "%*s", len, str);
}

inline unsigned long ulongAt(const unsigned char* buf, size_t offset) { return __builtin_bswap64(*(unsigned long*)(buf+offset)); }

inline unsigned int uintAt(const unsigned char* buf, size_t offset) { return __builtin_bswap32(*(unsigned int*)(buf+offset)); }

inline unsigned int ushortAt(const unsigned char* buf, size_t offset) {
  unsigned short x = *(unsigned short*)(buf+offset);
  asm("xchgb %b0, %h0" : "=Q" (x) : "0" (x));
  return x;
}

char *RemoveCharacters(char *buffer, int bufferLen)
{
  int i;

  for (i = bufferLen -1 ; i > -1; i--)
  {
    if (buffer[i] <= 32)
    {
      buffer[i] = 0;
    }
    else
    {
      break;
    }
  }

  return buffer;
}

char Lrc_Split(const char *str, char c, void *parameter)
{
  t_Parameters *workingList = (t_Parameters *)parameter;

  int i = 0;

  // Initialize ...
  workingList->countParameters = 0;

  while ((*str != 0) && (*str != 10) && (*str != 13))
  {
    if (*str != c)
    {
      workingList->parameterList[workingList->countParameters][i] = *str;

      i ++;
    }
    else
    {
      workingList->parameterList[workingList->countParameters][i] = 0;
      workingList->countParameters ++;

      i = 0;
    }

    str ++;
  }

  if (i > 0)
  {
    workingList->parameterList[workingList->countParameters][i] = 0;

    workingList->countParameters ++;
  }

  return workingList->countParameters;
}

void strcncpy(unsigned char *des, const char *src, char pad, int totalLen)
{
  while(totalLen--)
  {
    if (*src != 0)
    {
      *des++ = *src++;
    }
    else
    {
      *des++ = pad;
    }
  }
}

/****************************************************************************
- Function name:	Lrc_itoaf_stripped
- Input:			+ value
				+ pad
				+ len
- Output:		+ strResult
- Return:		[To be done]
- Description:	+ The routine hides the following facts
				  - Convert value from integer data type to 'string'
				+ There are no preconditions guaranteed to the routine
- Usage:			N/A
****************************************************************************/
void Lrc_itoaf_stripped(int value, const char pad, const int len, char *strResult)
{
	int i = 0;
	if (value == 0)
	{
		for (i = 0; i < len; i++)
		{
			strResult[i] = pad;
		}
		
		return;
	}

	int index = 0;

	while (value != 0)
	{
		strResult[len - index - 1] = 48 + value % 10;
		value = value / 10;	
		index++;
	}
	
	for (i = 0; i < len - index; i++)
	{
		strResult[i] = pad;
	}

	return;
}


