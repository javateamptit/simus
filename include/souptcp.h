/****************************************************************************
** Project name:    US Blink Simulator
** File name:       souptcp.h
** Description:     N/A
** Specification:   SoupTCP
**                  Version 2.00
**                  March 20, 2014
**
** Author:          Thien Nguyen-Tai
** First created:   May 12, 2014
** Last updated:    May 12, 2014
****************************************************************************/

#ifndef __SOUPTCP__
#define __SOUPTCP__

#include "global.h"


#define SOUPTCP_ADDITIONAL_DATA_PACKET_LEN                2

#define SOUPTCP_LOGIN_REQUEST_PACKET_LEN                  38
#define SOUPTCP_LOGIN_ACCEPTED_PACKET_LEN                 22
#define SOUPTCP_LOGIN_REJECTED_PACKET_LEN                 3
#define SOUPTCP_HEARTBEAT_PACKET_LEN                      2
#define SOUPTCP_LOGOUT_REQUEST_PACKET_LEN                 2

#define SOUPTCP_LOGIN_REQUEST_PACKET_TYPE                 'L'
#define SOUPTCP_LOGIN_ACCEPTED_PACKET_TYPE                'A'
#define SOUPTCP_LOGIN_REJECTED_PACKET_TYPE                'J'
#define SOUPTCP_SERVER_HEARTBEAT_PACKET_TYPE              'H'
#define SOUPTCP_CLIENT_HEARTBEAT_PACKET_TYPE              'R'
#define SOUPTCP_LOGOUT_REQUEST_PACKET_TYPE                'O'
#define SOUPTCP_DEBUG_PACKET_TYPE                         '+'
#define SOUPTCP_SEQUENCED_DATA_PACKET_TYPE                'S'
#define SOUPTCP_UNSEQUENCED_DATA_PACKET_TYPE              'U'

#define SOUPTCP_LOGIN_REJECTED_NOT_AUTHORIZED             'A'
#define SOUPTCP_LOGIN_REJECTED_NOT_SESSION                'S'

#define SOUPTCP_TERMINATING_LINEFEED                      0x0A

 
/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_ServerHeartbeatPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Server Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_LoginAcceptedPacket
- Input:            'connection': the connection; 'sessionId': The session is now logged into; 'seqNum': The sequence number
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Accepted Packet 
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId, const long seqNum);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_LoginRejectedPacket
- Input:            'connection': the connection; 'reason': reject reason
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Rejected Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_SequencedDataPacket
- Input:            'connection': the connection; 'msg': content message; 'msgLen': message length
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Sequenced Data Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_ClientHeartbeatPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Client Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_ClientHeartbeatPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_LoginRequestPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Request Packet  
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_LoginRequestPacket (t_Connection* connection, const char* userName, const char* password, const char* sessionId, const long seqNum);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_UnsequencedDataPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Unsequenced Data Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_UnsequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    SOUPTCP_BuildAndSend_LogoutRequestPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Logout Request Packet
- Usage:            N/A
****************************************************************************/
int SOUPTCP_BuildAndSend_LogoutRequestPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPTCP_Handle_LogonPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int SOUPTCP_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    SOUPTCP_Handle_LogoutPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int SOUPTCP_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    SOUPTCP_HeartbeatThread
- Input:            'arg': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
void* SOUPTCP_HeartbeatThread(void* arg);

#endif //__SOUPTCP__
