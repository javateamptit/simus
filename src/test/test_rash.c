#include "config.h"
#include "utility.h"
#include "global.h"
#include "sim.h"
#include "nasdaq_rash.h"
#include "network.h"


int TEST_RASH_logon(t_Connection* connection, const char* session, const char* user, const char* password)
{
  return RASH_BuildAndSend_LoginRequestPacket(connection, user, password, session, 0);
}

int TEST_RASH_logout(t_Connection* connection)
{
  return RASH_BuildAndSend_LogoutRequestPacket(connection);
}

int TEST_RASH_heartbeat (t_Connection* connection)
{
  return RASH_BuildAndSend_ClientHeartbeatPacket(connection);
}

int TEST_RASH_order(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
	// Unsequence data packet
	char msgContent[143];
  memset(msgContent, ' ', 143);
	msgContent[0] = 'U';

	// Order message type
	msgContent[1] = 'O';
	
	// Order token (2 digit account + orderID --> 14 char)
  strcncpy((unsigned char *)&msgContent[2], token, 32, 14);
	
	// Buy/Sell indicator	
	msgContent[16] = *sd;
	
	// Share		
	Lrc_itoaf_stripped(atoi(qty), '0', 6, &msgContent[17]);
	
	// Stock
	strcncpy((unsigned char *)&msgContent[23], symbol, 32, 8);
	
	// Price
  double price = atof(pr);
	if (fge(price, 1.00))
	{
		if (*sd == 'B')	//BUY SIDE
		{
			Lrc_itoaf_stripped((int)(price * 100.00 + 0.500001) * 100, '0', 10, &msgContent[31]);
		}
		else
		{
			Lrc_itoaf_stripped((int)(price * 100.00 + 0.000001) * 100, '0', 10, &msgContent[31]);
		}
	}
	else
	{
		Lrc_itoaf_stripped((int)(price * 10000.00 + 0.500001), '0', 10, &msgContent[31]);
	}
	
	// Time in Force	(IOC: "00000")  (DAY: "99999")
  if(strcmp(tf, "DAY")==0) 
  {
    strncpy(&msgContent[41], "99999", 5);    
  }
  else
  {
    strncpy(&msgContent[41], "00000", 5);      
  }

	// Firm
	strncpy(&msgContent[46], "MPID", 4);

	// Display
	msgContent[50] = 'Y';
	
	/***************************************************************
	- MinQty: Minimum Fill Amount Allowed. 
	If a non-zero value is put in this field, TIF must be IOC (or 0)
	***************************************************************/
	memset(&msgContent[51], '0', 6);

	/***************************************************************
	- Max Floor: Shares to Display. If zero this field will default 
	to the order qty. Use the display field to specify a hidden order.
	***************************************************************/
	memset(&msgContent[57], '0', 6);
	
	/***************************************************************
	- Peg Type: N � No Peg
				P � Market
				R � Primary
	***************************************************************/
	msgContent[63] = 'N';

	/***************************************************************
	- Peg Difference Sign:	+
							-
	If peg type is set to �N�, specify �+�
	***************************************************************/
	msgContent[64] = '+';
	
	/***************************************************************
	- Peg Difference: Amount; 6.4 (implied decimal). If peg type is 
	set to �N�, specify 0
	***************************************************************/
	memset(&msgContent[65], '0', 10);
	
	/***************************************************************
	- Discretion Price: Discretion Price for Discretionary Order. 
	If set to 0, then this order does not have discretion.
	***************************************************************/
	memset(&msgContent[75], '0', 10);
	
	/***************************************************************
	- Discretion Peg Type:	N � No Peg
							P � Market
							R � Primary
	***************************************************************/
	msgContent[85] = 'N';

	/***************************************************************
	- Discretion Peg Difference Sign:	+
										-
	If peg type is set to �N�, specify �+�
	***************************************************************/
	msgContent[86] = '+';
	
	/***************************************************************
	- Discretion Peg Difference: Amount; 6.4 (implied decimal). 
	If peg type is set to �N�, specify 0
	***************************************************************/ 
	memset(&msgContent[87], '0', 10);

	/***************************************************************
	- Capacity/Rule 80A Indicator: Capacity Code
	***************************************************************/
	msgContent[97] = 'P';

	/***************************************************************
	- Random Reserve: Shares to do random reserve with
	***************************************************************/
	memset(&msgContent[98], '0', 6);

	/***************************************************************
	- Route Dest / Exec Broker: Target ID (INET, DOTN, DOTA, DOTM, 
	DOTP, STGY and SCAN)
	***************************************************************/
	//memcpy(&message[102], "MOPP", 4);
	memcpy(&msgContent[104], "INET", 4);

	/***************************************************************
	- Cust / Terminal ID / Sender SubID: Client Initiated; Pass-thru. 
	Must be left justified and may not start with a space.
	***************************************************************/
	memset(&msgContent[108], ' ', 32);

	// Customer Type
	msgContent[140] = 'N';

  // Terminating Linefeed 0x0A
  msgContent[141] = 10;
    
    return SendMsg(connection->socket, &connection->lock, msgContent, 142); 
}

int TEST_RASH_cancel(t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{
	// Unsequence data packet
	char msgContent[23];
  memset(msgContent, 0, 23);
	msgContent[0] = 'U';
	
	// Order message type
	msgContent[1] = 'X';
	
	// Order token (2 digit account + orderID --> 14 char)
  strcncpy((unsigned char *)&msgContent[2], token, 32, 14);
  
  	// Share		
	Lrc_itoaf_stripped(atoi(newQty), '0', 6, &msgContent[16]);
 
  // Terminating Linefeed 0x0A
		msgContent[22] = 10;
  return SendMsg(connection->socket, &connection->lock, msgContent, 23); 
}

void TEST_RASH_printInfo(t_Connection* connection, t_DataBlock* data)
{
  int contentOff =  1;

  if(data->msgContent[0] =='S')
  {
    char type = data->msgContent[contentOff+8];
    char token[14];
    int shares;
    int price;
    char reason;
    char side;
    char symbol[8];
    switch(type)
    {
      case 'E':
        memcpy(token, &data->msgContent[contentOff+9], 14);
        shares = Lrc_atoi(&data->msgContent[contentOff+23], 6);
        price = Lrc_atoi(&data->msgContent[contentOff+29], 10);
        TraceLog(DEBUG_LEVEL, "[RASH  FILLED] ClOrdID:%.8s  Share:%d Price:%.4f\n", token, shares, price/10000.0);
        break;
        
      case 'A':
        memcpy(token, &data->msgContent[contentOff+9], 14);
        side = data->msgContent[contentOff+23];
        memcpy(symbol, &data->msgContent[contentOff+30], 8);
        shares = Lrc_atoi(&data->msgContent[contentOff+24], 6);
        price = Lrc_atoi(&data->msgContent[contentOff+38], 10);
        
        TraceLog(DEBUG_LEVEL, "[RASH  ACCEPTED] ClOrdID:%.8s Side:%s Symbol:%.6s Share:%d Price:%.4f\n", token,
                      side=='B' ? "Buy" : "Sell", symbol, shares, price/10000.0);
        break;
        
      case 'J':
        memcpy(token, &data->msgContent[contentOff+9], 14);
        reason = data->msgContent[contentOff+23];
        TraceLog(DEBUG_LEVEL, "[RASH REJECT] ClOrdID:%.8s REASON : %c\n", token, reason);
        break;
        
      case 'C':
        memcpy(token, &data->msgContent[contentOff+9], 14);
        reason = data->msgContent[contentOff+29];
        shares = Lrc_atoi(&data->msgContent[contentOff+23], 6);     
        TraceLog(DEBUG_LEVEL, "[RASH CANCELLED] ClOrdID:%.8s Share:%d  REASON : %c\n", token, shares, reason);
        break;
    }
  }
}
