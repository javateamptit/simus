#include "arca.h"

static long TimeStamp()
{
  struct timeval tv;
  gettimeofday(&tv, NULL);
  struct tm local;
  localtime_r(&tv.tv_sec, &local);
      
  long l1 = local.tm_hour * 3600 + local.tm_min * 60 + local.tm_sec;
  long l2 = l1 * 1000000;
  long l3 = l2 + tv.tv_usec;
  return l3;
}

static int BuildAndSendHeartBeat(t_Connection* connection)
{
  t_ShortConverter shortUnion;
  char message[ARCA_HEARTBEAT_MSG_LEN];
  memset(message, 0, ARCA_HEARTBEAT_MSG_LEN);
  
  // Message type is '0'
  message[0] = ARCA_HEARTBEAT_TYPE;
  
  // Variant (1 byte) = 1
  message[1] = 1;
  
  // Length
  shortUnion.value = ARCA_HEARTBEAT_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];
  
  // SeqNum (4 bytes)
  
  // Filler (3 byte)

  // Terminator
  message[11] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_HEARTBEAT_MSG_LEN);
}

static int BuildAndSendLogonReject(t_Connection* connection)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[ARCA_LOGON_REJECT_MSG_LEN];
  memset(message, 0, ARCA_LOGON_REJECT_MSG_LEN);
  
  //Packet Type is 'L'
  message[0] = ARCA_LOGON_REJECT_TYPE;
  // Variant
  message[1] = 1;
  // Length
  shortUnion.value = ARCA_LOGON_REJECT_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];  
  // SeqNum should be ignored 
  // Last Sequence Number Server Received 
  intUnion.value = connection->userInfo->incomingSeq;
  message[8] = intUnion.c[3];
  message[9] = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // Last Sequence Number Server Sent
  intUnion.value = connection->userInfo->outgoingSeq;
  message[12] = intUnion.c[3];
  message[13] = intUnion.c[2];
  message[14] = intUnion.c[1];
  message[15] = intUnion.c[0];
  // Reject Type 
  // The rejection reason description.
  strcpy(&message[18], "Invalid logon format / Unsupported");
  // Terminator
  message[59] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_LOGON_REJECT_MSG_LEN);
}

static int BuildAndSendLogonResponse(t_Connection* connection)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;

  char message[ARCA_LOGON_TYPE_MSG_LEN];
  memset(message, 0, ARCA_LOGON_TYPE_MSG_LEN);
  
  //Packet Type is 'A'
  message[0] = ARCA_LOGON_TYPE;
  // Variant
  message[1] = 1;
  // Length
  shortUnion.value = ARCA_LOGON_TYPE_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];  
  // SeqNum
  intUnion.value = connection->userInfo->outgoingSeq;
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  // Last sequence number
  intUnion.value = connection->userInfo->incomingSeq;
  message[8] = intUnion.c[3];
  message[9] = intUnion.c[2];
  message[10] = intUnion.c[1];
  message[11] = intUnion.c[0];
  // User name
  memcpy(&message[12], connection->userInfo->userName, 5);
  // Symbology
   message[17] = 2;
  // Message Version Profile
  message[18] = 'L';
  message[19] = 1;  
  // Cancel On Disconnect
  message[46] = connection->isCancelOnDisconnect;
  // Terminator
  message[47] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_LOGON_TYPE_MSG_LEN);
}

static int BuildAndSendACK(t_Connection* connection, t_OrderEntry *orderEntry)
{
  t_ShortConverter shortUnion;
  t_IntConverter intUnion;
  t_LongConverter longUnion;
  
  char message[ARCA_ORDER_ACK_MSG_LEN];
  memset(message, 0, ARCA_ORDER_ACK_MSG_LEN);
  
  // Message Type is 'a'
  message[0] = ARCA_ORDER_ACK_TYPE;
  
  //Variant
  message[1] = 1;
  
  // Length
  shortUnion.value = ARCA_ORDER_ACK_MSG_LEN;
  message[2]  = shortUnion.c[1];
  message[3]  = shortUnion.c[0];
  
  // Sequence Number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  
  // Sending Time ( 8 bytes ) = 8 -> 15
  longUnion.value = TimeStamp();
  message[8] = longUnion.c[7];
  message[9] = longUnion.c[6];
  message[10] = longUnion.c[5];
  message[11] = longUnion.c[4];
  message[12] = longUnion.c[3];
  message[13] = longUnion.c[2];
  message[14] = longUnion.c[1];
  message[15] = longUnion.c[0];
  
  // Transaction Time  ( 8  bytes ) = 16 -> 23
  message[16] = longUnion.c[7];
  message[17] = longUnion.c[6];
  message[18] = longUnion.c[5];
  message[19] = longUnion.c[4];
  message[20] = longUnion.c[3];
  message[21] = longUnion.c[2];
  message[22] = longUnion.c[1];
  message[23] = longUnion.c[0];
  
  // Client Order Id
  intUnion.value =  atol(orderEntry->clOrdID);
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
                
  // Order ID ( 8 bytes ) = 28 -> 35
  longUnion.value = __sync_add_and_fetch(&connection->orderID, 1);
  message[28] = longUnion.c[7];
  message[29] = longUnion.c[6];
  message[30] = longUnion.c[5];
  message[31] = longUnion.c[4];
  message[32] = longUnion.c[3];
  message[33] = longUnion.c[2];
  message[34] = longUnion.c[1];
  message[35] = longUnion.c[0];
  
  // Price ( 4 bytes ) = 36 -> 39
  intUnion.value = orderEntry->price;
  message[36] = intUnion.c[3];
  message[37] = intUnion.c[2];
  message[38] = intUnion.c[1];
  message[39] = intUnion.c[0];
  
  // Price Scale ( 1 byte ) = 40
  message[40] = '2';
  
  // Liquidity ( 1 byte ) = 41
  
  // Filler ( 5 bytes ) = 42 -> 46
  
  // Message Terminator    
  message[47] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_ORDER_ACK_MSG_LEN);
}

static int BuildAndSendFILL(t_Connection *connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  t_ShortConverter shortUnion;

  char message[ARCA_ORDER_FILLED_MSG_LEN + 1] = "\0";

  // Message type is '2'
  message[0] = ARCA_ORDER_FILLED_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  shortUnion.value = ARCA_ORDER_FILLED_MSG_LEN;
  message[2]  = shortUnion.c[1];
  message[3]  = shortUnion.c[0];

  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  
  // Sending Time ( 8 bytes ) = 8 -> 15
  longUnion.value = TimeStamp();
  message[8] = longUnion.c[7];
  message[9] = longUnion.c[6];
  message[10] = longUnion.c[5];
  message[11] = longUnion.c[4];
  message[12] = longUnion.c[3];
  message[13] = longUnion.c[2];
  message[14] = longUnion.c[1];
  message[15] = longUnion.c[0];
  
  // Transaction Time  ( 8  bytes ) = 16 -> 23
  message[16] = longUnion.c[7];
  message[17] = longUnion.c[6];
  message[18] = longUnion.c[5];
  message[19] = longUnion.c[4];
  message[20] = longUnion.c[3];
  message[21] = longUnion.c[2];
  message[22] = longUnion.c[1];
  message[23] = longUnion.c[0];
  
  // Client order Id
  intUnion.value =  atol(item->orderEntry.clOrdID);
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  
  // Order ID ( 8 bytes ) = 28 --> 35
  longUnion.value = item->orderEntry.orderID;
  message[28] = longUnion.c[7];
  message[29] = longUnion.c[6];
  message[30] = longUnion.c[5];
  message[31] = longUnion.c[4];
  message[32] = longUnion.c[3];
  message[33] = longUnion.c[2];
  message[34] = longUnion.c[1];
  message[35] = longUnion.c[0];
  
  // Execution ID ( 8 bytes ) = 36 -> 43
  longUnion.value = __sync_add_and_fetch(&connection->executionNumber, 1);
  message[36] = longUnion.c[7];
  message[37] = longUnion.c[6];
  message[38] = longUnion.c[5];
  message[39] = longUnion.c[4];
  message[40] = longUnion.c[3];
  message[41] = longUnion.c[2];
  message[42] = longUnion.c[1];
  message[43] = longUnion.c[0];
  
  // ArcaExID ( 20 bytes ) = 44 --> 63
  sprintf(&message[44], "Record_%010ld", longUnion.value);
  
  // Last Shares or Contracts ( 4 bytes ) = 64 -> 67
  intUnion.value = quantity;
  message[64] = intUnion.c[3];
  message[65] = intUnion.c[2];
  message[66] = intUnion.c[1];
  message[67] = intUnion.c[0];
  
  // Last Price ( 4 bytes ) = 68 --> 71
  intUnion.value = (int)price;
  message[68] = intUnion.c[3];
  message[69] = intUnion.c[2];
  message[70] = intUnion.c[1];
  message[71] = intUnion.c[0];
  
  // Price Scale ( 1 byte ) = 72
  message[72] = '2';
  
  // Liquidity Indicator ( 1 byte ) = 73
  message[73] = 'R';
  
  // Side  ( 1 byte ) = 74
  message[74] = item->orderEntry.side;
  
  // LastMkt ( 2 bytes ) = 75 -> 76
  message[75] = 'A';
  message[76] = 'O';
  
  // Filler ( 10 bytes ) = 77 -> 86
    
  //Terminator
  message[87] = '\n';
    
  return SendMsg(connection->socket, &connection->lock, message, ARCA_ORDER_FILLED_MSG_LEN);
}

static int BuildAndSendCanceled(t_Connection* connection, t_QueueItem *item)
{
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  t_ShortConverter shortUnion;

  char message[ARCA_SEND_CANCEL_MSG_LEN + 1] = "\0";
  
  // Message type is '4'
  message[0] = ARCA_SEND_CANCEL_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  shortUnion.value = ARCA_SEND_CANCEL_MSG_LEN;
  message[2] = shortUnion.c[1];
  message[3] = shortUnion.c[0];

  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];
  
  // Sending Time ( 8 bytes ) = 8 -> 15
  longUnion.value = TimeStamp();
  message[8] = longUnion.c[7];
  message[9] = longUnion.c[6];
  message[10] = longUnion.c[5];
  message[11] = longUnion.c[4];
  message[12] = longUnion.c[3];
  message[13] = longUnion.c[2];
  message[14] = longUnion.c[1];
  message[15] = longUnion.c[0];
  
  // Transaction Time  ( 8  bytes ) = 16 -> 23
  message[16] = longUnion.c[7];
  message[17] = longUnion.c[6];
  message[18] = longUnion.c[5];
  message[19] = longUnion.c[4];
  message[20] = longUnion.c[3];
  message[21] = longUnion.c[2];
  message[22] = longUnion.c[1];
  message[23] = longUnion.c[0];
  
  // Client order Id
  intUnion.value = atol(item->orderEntry.clOrdID);
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  
  // Order ID (8 bytes) = null = 28 -> 35
  longUnion.value = item->orderEntry.orderID;
  message[28] = longUnion.c[7];
  message[29] = longUnion.c[6];
  message[30] = longUnion.c[5];
  message[31] = longUnion.c[4];
  message[32] = longUnion.c[3];
  message[33] = longUnion.c[2];
  message[34] = longUnion.c[1];
  message[35] = longUnion.c[0];
  
  // Information Text (1 byte - Binary)
  message[36] = 1; // �0� = user-initiated kill, �1� = exchange-initiated for PNP crossed market.
  
  // Filler (2 bytes) = null
  
  // Terminator
  message[39] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_SEND_CANCEL_MSG_LEN);
}

static int BuildAndSendREJECT(t_Connection* connection, const int clOrdID, const int orgClOrdID,
    const char type, const char* text, const char reason)
{
  t_LongConverter longUnion;
  t_IntConverter intUnion;
  t_ShortConverter shortUnion;

  char message[ARCA_ORDER_REJECTED_MSG_LEN_V1 + 1] = "\0";

  // Message type is '8'
  message[0] = ARCA_ORDER_REJECTED_TYPE;

  // Variant (1 byte) = 1
  message[1] = 1;

  // Length
  shortUnion.value = ARCA_ORDER_REJECTED_MSG_LEN_V1;
  message[2]  = shortUnion.c[1];
  message[3]  = shortUnion.c[0];

  // Sequence number
  intUnion.value =  __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
  message[4] = intUnion.c[3];
  message[5] = intUnion.c[2];
  message[6] = intUnion.c[1];
  message[7] = intUnion.c[0];

  // Sending Time ( 8 bytes ) = 8 -> 15
  longUnion.value = TimeStamp();
  message[8] = longUnion.c[7];
  message[9] = longUnion.c[6];
  message[10] = longUnion.c[5];
  message[11] = longUnion.c[4];
  message[12] = longUnion.c[3];
  message[13] = longUnion.c[2];
  message[14] = longUnion.c[1];
  message[15] = longUnion.c[0];
  
  // Transaction Time  ( 8  bytes ) = 16 -> 23
  message[16] = longUnion.c[7];
  message[17] = longUnion.c[6];
  message[18] = longUnion.c[5];
  message[19] = longUnion.c[4];
  message[20] = longUnion.c[3];
  message[21] = longUnion.c[2];
  message[22] = longUnion.c[1];
  message[23] = longUnion.c[0];
  
  // Client order Id
  intUnion.value =  clOrdID;
  message[24] = intUnion.c[3];
  message[25] = intUnion.c[2];
  message[26] = intUnion.c[1];
  message[27] = intUnion.c[0];
  
  // Original Client Order ID ( 4 bytes ) = 28 --> 31
  intUnion.value =  orgClOrdID;
  message[28] = intUnion.c[3];
  message[29] = intUnion.c[2];
  message[30] = intUnion.c[1];
  message[31] = intUnion.c[0];
  
  // Reject message type       Numeric     ( = 1 or 2 or 3 ) = 32
  message[32] = type;
  
  // Text ( 40 bytes ) = 33 --> 72
  strncpy(&message[33], text, 40);
  
  // Reject reason ( 1 byte ) = 73
  message[73] = reason; // 0 = Too late to cancel, 1 = Unknown order
  
  // Filler ( 5 bytes ) = 74 --> 78
    
  //Terminator
  message[79] = '\n';
  
  return SendMsg(connection->socket, &connection->lock, message, ARCA_ORDER_REJECTED_MSG_LEN_V1);
}

static int HandleMsgLogon(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);

  // Variant
  if (dataBlock->msgContent[1] != 1)
  { 
    TraceLog(ERROR_LEVEL, "ARCA: Variant isn't yet supported (%d) \n", dataBlock->msgContent[1]);
    BuildAndSendLogonReject(connection);
    return ERROR;
  }
  // SeqNum
  connection->userInfo->incomingSeq = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[4]));
  // UserName
  memcpy(connection->userInfo->userName, &dataBlock->msgContent[12], 5);
  // Cancel On Disconnect 
  connection->isCancelOnDisconnect = dataBlock->msgContent[46];
  // Message Terminator
  if (dataBlock->msgContent[47] != '\n')
  { 
    TraceLog(ERROR_LEVEL, "ARCA: Invalid Message Terminator \n");
    BuildAndSendLogonReject(connection);
    return ERROR;
  }
  
  return BuildAndSendLogonResponse(connection);
}

static int HandleMsgCancelOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_QueueItem item;
  memset(&item, 0, sizeof(t_QueueItem));
  int orderID = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[16])); // OriginalClOrdID
  sprintf(item.orderEntry.clOrdID, "%u", orderID);
  memcpy(item.orderEntry.symbol, &dataBlock->msgContent[32], 8);
  TraceLog(DEBUG_LEVEL, "ARCA: Cancel order(%s)  symbol(%s)\n", item.orderEntry.clOrdID, item.orderEntry.symbol);
  
  int res = SearchAndRemoveToken(&connection->orderQueue, &item);
  if(res == SUCCESS)
  {
    BuildAndSendCanceled(connection, &item);
  }
  else
  {
    TraceLog(DEBUG_LEVEL, "ARCA: Not found order(%s), errorCode(%d)\n", item.orderEntry.clOrdID, res);
    BuildAndSendREJECT(connection, orderID, orderID, '2', "Not found", '1');
  }
  
  return SUCCESS;
}

static int HandleMsgNewOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  int clOrdID = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[8]));
  sprintf(orderEntry.clOrdID, "%u", clOrdID);
  
  orderEntry.side = dataBlock->msgContent[51];
  orderEntry.quantity = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[16])); // 16-19
  
  memcpy(orderEntry.symbol, &dataBlock->msgContent[27], 8);
  
  orderEntry.price = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[20])); // 20-23
  
  orderEntry.tif = dataBlock->msgContent[53];
  
  TraceLog(DEBUG_LEVEL, "ARCA New order: symbol(%.8s), clOrdID(%s), quantity(%d), price(%lf) side(%c) tif(%c)\n", 
                    orderEntry.symbol, orderEntry.clOrdID, orderEntry.quantity, 
                    orderEntry.price * 0.01, orderEntry.side, orderEntry.tif);
  
  if(orderEntry.tif != '0' && orderEntry.tif != '3')
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by TIF", '1');
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
  }
  else if(orderEntry.side != '1' && orderEntry.side != '2' && orderEntry.side != '5')
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by Side", '1');
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by Quantity", '1');
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
  }
  else
  {
    BuildAndSendACK(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;
    
    // Detect cross
    int cross = Simulator_Handle_CrossOrder(connection, &item, '1', &BuildAndSendFILL);
    //IOC Random
    if (orderEntry.tif == '3')
      if (cross == -1)
        Simulator_Handle_RandomIOC(connection, &item, &BuildAndSendFILL, &BuildAndSendCanceled);
      else
        BuildAndSendCanceled(connection, &item);      
    else if (item.orderEntry.quantity > item.cumShare) // DAY
      AddOrderToken(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static int HandleMsgNewOrder_V3(t_Connection* connection, t_DataBlock *dataBlock)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  int clOrdID = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[8]));
  sprintf(orderEntry.clOrdID, "%u", clOrdID);
  
  orderEntry.side = dataBlock->msgContent[93];
  orderEntry.quantity = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[16])); // 16-19
  
  memcpy(orderEntry.symbol, &dataBlock->msgContent[61], 8);
  
  orderEntry.price = __builtin_bswap32(*(unsigned int*)(&dataBlock->msgContent[48])); // 20-23
  
  orderEntry.tif = dataBlock->msgContent[95];
  
  // printf("debug: HandleMsgNewOrder_V3: symbol(%.8s), clOrdID(%20s), quantity(%d), price(%lf) side(%c) tif(%c)\n", 
                    // orderEntry.symbol, orderEntry.clOrdID, orderEntry.quantity, 
                    // orderEntry.price * 0.01, orderEntry.side, orderEntry.tif);
  
  if(orderEntry.tif != '0' && orderEntry.tif != '3')
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by TIF", '1');
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
  }
  else if(orderEntry.side != '1' && orderEntry.side != '2' && orderEntry.side != '5')
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by Side", '1');
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    BuildAndSendREJECT(connection, clOrdID, clOrdID, '1', "Reject by Quantity", '1');
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
  }
  else
  {
    BuildAndSendACK(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;
    
    // Detect cross
    int cross = Simulator_Handle_CrossOrder(connection, &item, '1', &BuildAndSendFILL);
    //IOC Random
    if (orderEntry.tif == '3')
      if (cross == -1)
        Simulator_Handle_RandomIOC(connection, &item, &BuildAndSendFILL, &BuildAndSendCanceled);
      else
        BuildAndSendCanceled(connection, &item);      
    else if (item.orderEntry.quantity > item.cumShare) // DAY
      AddOrderToken(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static void* HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    BuildAndSendHeartBeat(connection);
    sleep(30); // The server heartbeat interval is 60 seconds
  }
  
  TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
  return NULL;
}

static void* DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &BuildAndSendFILL, &BuildAndSendCanceled);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_ARCA(t_Connection* connection, t_DataBlock *dataBlock)
{
  switch(dataBlock->msgContent[0])
  {
    case 'A':
      TraceLog(DEBUG_LEVEL, "Received login message from client\n");
      if(HandleMsgLogon(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, HeartbeatThread, (void*)connection);
      }
      break;
      
    case '1':
      // printf("'1' - Test request.\n");
      break;
    
    case '0':
      // printf("'0' - Heartbeat message\n");
      break;
    
    case 'D':
      // printf("'D' - HandleMsgNewOrder\n");
      if(dataBlock->msgContent[1] == 1)
        HandleMsgNewOrder(connection, dataBlock);
      else
        HandleMsgNewOrder_V3(connection, dataBlock);
      break;
      
    case 'F':
      // printf("'F' - HandleMsgCancelOrder\n");
      HandleMsgCancelOrder(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown MsgType(%d)\n", dataBlock->msgContent[0]);
      break;
  }
  return SUCCESS;
}