#ifndef _NSX_
#define _NSX_

#include "global.h"


#define NSX_COMPID                  "NSX"
#define NSX_BEGIN_STRING            "FIX4.2"
#define NSX_MSG_BEGIN_STRING        "8=FIX4.2\001"
#define NSX_MSG_BEGIN_STRING_LEN     9
#define NSX_RESERVE_ENTIRE_MSG_LEN   2048

int HandleMsg_NSX(t_Connection* client, t_DataBlock *dataBlock);

#endif //_NSX_
