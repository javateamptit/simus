#ifndef _NETWORK_
#define _NETWORK_

#include "utility.h"
#include "global.h"

int ReceiveMsg_FIX(t_Connection *connection, t_DataBlock* dataBlock);
int ReceiveMsg_RASH(t_Connection *connection, t_DataBlock* dataBlock);
int ReceiveMsg(t_Connection *connection, t_DataBlock* dataBlock);
int ReceiveMsgFrom(int sockfd, t_DataBlock *dataBlock, struct sockaddr* from, int* fromLen);
int SendMsg(int sockfd, pthread_spinlock_t *lock, char* buff, int len);
int SendMsgTo(int sockfd, pthread_spinlock_t *lock, char* buff, int len, struct sockaddr* to, int tolen);
int CloseConnection(t_Connection *connection);

#endif
