#include "config.h"
#include "utility.h"
#include "global.h"
#include "sim.h"
#include "arca.h"

int TEST_ARCA_logon(t_Connection* connection, const char* session, const char* user, const char* password)
{
	char message[48];
	memset(message, 0, 48);	
	t_IntConverter lastSeqNum;
	t_ShortConverter tmpLength;

	// Message type is 'A'
	message[0] = 'A';

	// Variant
	message[1] = 1;

	// Length
	tmpLength.value = 48;
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];
	
	// SeqNum (4 bytes)
	
	/*
	Last sequence number
	Message last sequence number should start at 0
	*/
	lastSeqNum.value = __sync_add_and_fetch(&connection->userInfo->incomingSeq, 1);
	message[8] = lastSeqNum.c[3];
	message[9] = lastSeqNum.c[2];
	message[10] = lastSeqNum.c[1];
	message[11] = lastSeqNum.c[0];

	// Company Group ID (User name)
	memcpy(&message[12], user, 5);

	// Symbology (1 byte)
	
	// Message Version Profile
	strncpy(&message[18], ARCA_VERSION_PROFILE, 28);
	
	// Cancel On Disconnect (1 bytes binary). Should be 0 or 1
	message[46] = 0;

	// Terminator
	message[47] = '\n';
	
	// Send logon message
  return SendMsg(connection->socket, &connection->lock, message, 48);
}

int TEST_ARCA_logout(t_Connection* connection)
{
  connection->isAlive = 0;
  return 1;
}

int TEST_ARCA_heartbeat (t_Connection* connection)
{
	char message[12];
  memset(message, 0, 12);	
	t_ShortConverter tmpLength;
		
	// Message type is '0'
	message[0] = '0';

	// Variant (1 byte) = 1
	message[1] = 1;

	// Length
	tmpLength.value = 12;
	message[2] = tmpLength.c[1];
	message[3] = tmpLength.c[0];

	// SeqNum (4 bytes)
	
	// Filler (3 byte)

	// Terminator
	message[11] = '\n';
  return SendMsg(connection->socket, &connection->lock, message, 12);
	
}

int TEST_ARCA_order(t_Connection* connection, char* token, char* symbol, char* sd, char* qty, char* pr, char* tf)
{
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;
  
  char msgContent[76] ;
  memset(msgContent, 0, 76);

	// Message type is 'D'
	msgContent[0] = 'D';

	// Variant (1 byte) = 1
	msgContent[1] = 1;

	// Length
	tmpLength.value = 76;
	msgContent[2]  = tmpLength.c[1];
	msgContent[3]  = tmpLength.c[0];

	// Sequence number   (we set this later, right before sending)
	/*
	int seqNum = Get_ARCA_DIRECT_OutgoingSeqNum();
	tmpValue.value = seqNum;
	msgContent[4] = tmpValue.c[3];
	msgContent[5] = tmpValue.c[2];
	msgContent[6] = tmpValue.c[1];
	msgContent[7] = tmpValue.c[0];
	*/
	
	// Client order Id
	tmpValue.value =  atoi(token);
	msgContent[8] = tmpValue.c[3];
	msgContent[9] = tmpValue.c[2];
	msgContent[10] = tmpValue.c[1];
	msgContent[11] = tmpValue.c[0];
	
	// PCS Link ID
	msgContent[12] = 0;
	msgContent[13] = 0;
	msgContent[14] = 0;
	msgContent[15] = 0;

	// Order Quantity
	tmpValue.value =  atoi(qty);
	msgContent[16] = tmpValue.c[3];
	msgContent[17] = tmpValue.c[2];
	msgContent[18] = tmpValue.c[1];
	msgContent[19] = tmpValue.c[0];

	// Price
	tmpValue.value = (int)(atof(pr) * 100.00 + 0.500001);
	msgContent[20] = tmpValue.c[3];
	msgContent[21] = tmpValue.c[2];
	msgContent[22] = tmpValue.c[1];
	msgContent[23] = tmpValue.c[0];
	
	// ExDestination (2 bytes) = 102
	tmpLength.value = 102;
	msgContent[24]  = tmpLength.c[1];
	msgContent[25]  = tmpLength.c[0];

	// Price scale
	msgContent[26] = '2';

	// Symbol
	strncpy(&msgContent[27], symbol, 8);
	
	// Company group ID
	memcpy(&msgContent[35], "HBIWB", 5);
	
	// Deliver Comp ID

	
	// SenderSubID (5 bytes) = null


	// Execution Instructions
	msgContent[50] = '1';

	// Side
	msgContent[51] = *sd == 'B'? '1' : (*sd == 'S' ? '2' : '5')  ;

	// Order Type
	msgContent[52] = '2';

	// Time In Force
	msgContent[53] =  strcmp(tf, "DAY")==0? '0' : '3';	//IOC

	// Rule80A: A, P, E
	msgContent[54] = 'P';
	
	// Trading session Id: "1", "2", "3"
	memcpy(&msgContent[55], "123\0", 4);
	
	// Account
	strcpy(&msgContent[59], "LONG");
	memset(&msgContent[59 + 4], 0, 10 - 4);
	
	// ISO Flag
		msgContent[69] = 'N';

	
	// Extended Execution instruction
	
	// ExtendedPNP (1 byte) = null
	
	// NoSelfTrade (1 byte) = 'O'
	msgContent[72] = 'O';
	
	// ProactiveIfLocked (1 byte) = null
	
	// Filler (1 byte)
	
	//Terminator
	msgContent[75] = '\n';

  return SendMsg(connection->socket, &connection->lock, msgContent, 76);  
}

int TEST_ARCA_cancel(t_Connection* connection, char* token, char* symbol, char* sd, char* newQty)
{
	t_IntConverter tmpValue;
	t_ShortConverter tmpLength;
  
  char msgContent[72] ;
  memset(msgContent, 0, 72);

	// Message type is 'F'
	msgContent[0] = 'F';

	// Variant (1 byte) = 1
	msgContent[1] = 1;

	// Length
	tmpLength.value = 72;
	msgContent[2]  = tmpLength.c[1];
	msgContent[3]  = tmpLength.c[0];

	// Sequence number   (we set this later, right before sending)
	/*
	int seqNum = Get_ARCA_DIRECT_OutgoingSeqNum();
	tmpValue.value = seqNum;
	msgContent[4] = tmpValue.c[3];
	msgContent[5] = tmpValue.c[2];
	msgContent[6] = tmpValue.c[1];
	msgContent[7] = tmpValue.c[0];
	*/
	//Order ID
  
	// Client order Id
	
	// Original ClOrdID 
	tmpValue.value =  atoi(token);
	msgContent[16] = tmpValue.c[3];
	msgContent[17] = tmpValue.c[2];
	msgContent[18] = tmpValue.c[1];
	msgContent[19] = tmpValue.c[0];
  
	// Strike Price

  //Under Qty
  
	// ExDestination (2 bytes) = 102
	tmpLength.value = 102;
	msgContent[24]  = tmpLength.c[1];
	msgContent[25]  = tmpLength.c[0];

  // Corporate Action
  // Put or Call
  // Bulk Cancel
	// Open or Close
	// Symbol
	strncpy(&msgContent[32], symbol, 8);
  //Strike Date

	// Side
	msgContent[48] = *sd;

  //DeliverToCompID

	// Account
	strcpy(&msgContent[53], "LONG");
	memset(&msgContent[53 + 4], 0, 10 - 4);
	
  // Filler
	
	//Terminator
	msgContent[71] = '\n';

  return SendMsg(connection->socket, &connection->lock, msgContent, 72);   
}

void TEST_ARCA_printInfo(t_Connection* connection, t_DataBlock* data)
{
  char type = data->msgContent[0];
  int token;
  if (type == '2')
  {

    token = __builtin_bswap32(*(unsigned int*)(&data->msgContent[24]));
    int share = __builtin_bswap32(*(unsigned int*)(&data->msgContent[64]));
    int price = __builtin_bswap32(*(unsigned int*)(&data->msgContent[68]));
    char side = data->msgContent[74];
    
    
    TraceLog(DEBUG_LEVEL, "[ARCA  FILL] ClOrdID:%d Side:%s  Share:%d Price:%.4f\n", token, side=='1' ? "Buy" : "Sell" ,share, price/100.0);
    return;
  }
  
  if (type == '4')
  {
    token = __builtin_bswap32(*(unsigned int*)(&data->msgContent[24]));
    
    TraceLog(DEBUG_LEVEL, "[ARCA CANCEL] ClOrdID:%d\n", token);
    return;
  }
}
