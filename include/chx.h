/****************************************************************************
** Project name:    US Blink Simulator
** File name:       chx.h
** Description:     N/A
** Specification:   CHX FIX Link Specification
**                  Version 1.27
**                  November 21, 2013
**
** Author:          Thien Nguyen-Tai
** First created:   April 17, 2014
** Last updated:    May 13, 2014
****************************************************************************/

#ifndef _CHX_
#define _CHX_

#include "global.h"


#define CHX_COMPID                  "CHX"
#define CHX_BEGIN_STRING            "FIX 4.2"
#define CHX_RESERVE_ENTIRE_MSG_LEN   2048

int HandleMsg_CHX(t_Connection* client, t_DataBlock *dataBlock);

#endif //_CHX_
