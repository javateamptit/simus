/****************************************************************************
** Project name:    US Blink Simulator
** File name:       soupbintcp.h
** Description:     N/A
** Specification:   SoupBinTCP
**                  Version 3.00
**                  July 20, 2009
**
** Author:          Thien Nguyen-Tai
** First created:   April 24, 2014
** Last updated:    May 08,2014
****************************************************************************/

#ifndef __SOUPBINTCP__
#define __SOUPBINTCP__

#include "global.h"


#define SOUPBINTCP_PAYLOAD_OFFSET                     3
#define SOUPBINTCP_HEARDER_PACKET_LEN                 2

#define SOUPBINTCP_LOGIN_REQUEST_PACKET_LEN           49
#define SOUPBINTCP_LOGIN_ACCEPTED_PACKET_LEN          33
#define SOUPBINTCP_LOGIN_REJECTED_PACKET_LEN          4
#define SOUPBINTCP_HEARTBEAT_PACKET_LEN               3
#define SOUPBINTCP_END_SESSION_PACKET_LEN             3
#define SOUPBINTCP_LOGOUT_REQUEST_PACKET_LEN          3

#define SOUPBINTCP_LOGIN_REQUEST_PACKET_TYPE                'L'
#define SOUPBINTCP_LOGIN_ACCEPTED_PACKET_TYPE               'A'
#define SOUPBINTCP_LOGIN_REJECTED_PACKET_TYPE               'J'
#define SOUPBINTCP_SERVER_HEARTBEAT_PACKET_TYPE             'H'
#define SOUPBINTCP_CLIENT_HEARTBEAT_PACKET_TYPE             'R'
#define SOUPBINTCP_END_SESSION_PACKET_TYPE                  'Z'
#define SOUPBINTCP_LOGOUT_REQUEST_PACKET_TYPE               'O'
#define SOUPBINTCP_DEBUG_PACKET_TYPE                        '+'
#define SOUPBINTCP_SEQUENCED_DATA_PACKET_TYPE               'S'
#define SOUPBINTCP_UNSEQUENCED_DATA_PACKET_TYPE             'U'

#define SOUPBINTCP_LOGIN_REJECTED_NOT_AUTHORIZED     'A'
#define SOUPBINTCP_LOGIN_REJECTED_NOT_SESSION        'S'

 
/****************************************************************************
- Function name:    SOUPBINTCP_GetContent_UnsequencedPacket
- Input:            'dataBlock': the data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Get data of application message
- Usage:            N/A
****************************************************************************/
char* SOUPBINTCP_GetContent_UnsequencedPacket (t_DataBlock* dataBlock, int* len);
 
/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_ServerHeartbeatPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Server Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_ServerHeartbeatPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_LoginAcceptedPacket
- Input:            'connection': the connection; 'sessionId': The session is now logged into; 'seqNum': The sequence number
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Accepted Packet 
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_LoginAcceptedPacket (t_Connection* connection, const char* sessionId, const long seqNum);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_LoginRejectedPacket
- Input:            'connection': the connection; 'reason': reject reason
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Rejected Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_LoginRejectedPacket (t_Connection* connection, const char reason);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_EndSessionPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send End of Session Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_EndSessionPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_SequencedDataPacket
- Input:            'connection': the connection; 'msg': content message; 'msgLen': message length
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Sequenced Data Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_SequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Client Heartbeat Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_ClientHeartbeatPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_LoginRequestPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Login Request Packet  
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_LoginRequestPacket (t_Connection* connection, const char* userName, const char* password, const char* sessionId, const long seqNum);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_UnsequencedDataPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Unsequenced Data Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_UnsequencedDataPacket (t_Connection* connection, const char* msg, const int msgLen);

/****************************************************************************
- Function name:    SOUPBINTCP_BuildAndSend_LogoutRequestPacket
- Input:            'connection': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      Build and send Logout Request Packet
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_BuildAndSend_LogoutRequestPacket (t_Connection* connection);

/****************************************************************************
- Function name:    SOUPBINTCP_Handle_LogonPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_Handle_LogonPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    SOUPBINTCP_Handle_LogoutPacket
- Input:            'connection': the connection; 'dataBlock': data
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_Handle_LogoutPacket(t_Connection* connection, t_DataBlock *dataBlock);

/****************************************************************************
- Function name:    SOUPBINTCP_HeartbeatThread
- Input:            'arg': the connection
- Output:           N/A
- Return:           SUCCESS or ERROR
- Description:      N/A
- Usage:            N/A
****************************************************************************/
void* SOUPBINTCP_HeartbeatThread(void* arg);

/****************************************************************************
- Function name:    SOUPBINTCPStringFormatPrintable
- Input:            'buffer': buffer need to format; 'buffLen': length of the buffer
- Output:           'outStr': string replaced the line feed character
- Return:           length of the outStr
- Description:      Replaces all the non printable character within this buffer with hex.
- Usage:            N/A
****************************************************************************/
int SOUPBINTCP_StringFormatPrintable(char* outStr, const unsigned char* buffer, const int buffLen);

#endif //__SOUPBINTCP__
