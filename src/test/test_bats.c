#include "sim.h"
#include "global.h"
#include "network.h"
#include "bats_boe.h"


#define MPID_BATSZ_BOE "BYXT"

static int testExit = 0;

int BuildAndSend_BATS_BOE_Client_HeartbeatMsg(t_Connection* connection)
{
  unsigned short maxLength = 10;
  
	char message[maxLength];
	memset(message, 0, maxLength);

	// Start of Message: 2 bytes(0-1)
	message[0] = 0xBA;
	message[1] = 0xBA;

	// Message length: 2 bytes(2-3)
	message[2] = 0x08;

	// Message type: 1 byte
	message[4] = 0x03;

	// Matching Unit 1 byte -> Not use
	// Sequence number 4 bytes(6-9) -> Not use

	return SendMsg(connection->socket, &connection->lock, message, maxLength);
}

int BuildAndSend_BATS_BOE_LoginRequestMsg(t_Connection* connection, const char* sessionSubId, const char* userName, const char* password)
{
	/*
	 * ************Why is the 5 * BATSZ_BOE_Config.numbersOfUnits********
	 * Each pair of unit/sequence has 5 bytes in which unit has 1 byte and sequence has 4 bytes
	*/
	unsigned short maxLength = 118;

	char message[maxLength];
	memset(message, 0, maxLength);

	// Start of Message: 2 bytes(0-1])
	message[0] = 0xBA;
	message[1] = 0xBA;

	// Message length: 2 bytes(2-3)
	unsigned short mgsLength = maxLength - 2; // Not including 2 bytes of start of message
	memcpy(&message[2], &mgsLength, 2);

	// Message type: 1 byte
	message[4] = 0x01;

	// Matching Unit: 1 byte -> always 0
	// Sequence number: 4 bytes(6-9]) -> always 0

	// Session Sub ID: 4 bytes(10-13)
	strncpy(&message[10], sessionSubId, 4);

	// User name: 4 bytes(14-17)
	strncpy(&message[14], userName, 4);

	// Password: 10 bytes(18-27)
	strncpy(&message[18], password, 10);

	/*
	 * No unspecified Unit Replay: 1 byte
	 * 0x00 = False: Replay Unspecified Units
	 * 0x01 = True: Suppress Unspecified Units Replay
	 * Set this value to 1(default) in batsz_boe_conf
	*/
	message[28] = 0x00;

	/* ****************HOW ARE BITFIELDS USED *************************
	 * + Currently, EAA will trade with Time In Force = IOC. Therefore we just desire to
	 * 		receive messages of Order-ACK/Order-Rejected/Order-Cancelled/Order-Execution from BATSZ BOE
	 * + Use 7 bytes for each Bitfields. If bit in bitfield set to 1, we will use optional field
	 * + There is always 1 byte(Reserved) after each Bitfields
	*/

	/***********Order Acknowledgement Bitfield (29-35)*********/
	//Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType, 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
	message[29] = 0xBD; // 10111101 (Side, Price, ExecInst, OrdType, TimeInForce, MaxRemovePct)
	
	//Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
	message[30] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
	
	//Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
	message[31] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
	
	//Bitfield4: not use
	
	//Bitfield5 (1:OrigClOrdID, 2:LeavesQty, 3:LastShares, 4:LastPx, 5:DisplayPrice, 6:WorkingPrice, 7:BaseLiquidity, 8:ExpireTime)
	message[33] = 0x4F; // 01001111(OrigCldOrdID, LeavesQty, LastShares, LastPx, BaseLiquidity)
	
	//Bitfield6: not use
	//Bitfield7: not use
	//Reserved: 1 byte -> Not use
	

	/*************** Order Rejected Bitfields (37-43) *******************/
	//Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
	message[37] = 0xBD; // 10111101 (Side, Price, ExecInst, OrdType, TimeInForce, MaxRemovePct)
	
	//Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
	message[38] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
	
	//Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
	message[39] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
	
	//Bitfield4: not use
	//Bitfield5: not use
	//Bitfield6: not use
	//Bitfield7: not use
	// Reserved 1 byte -> Not use
	
	
	/************OrderModified Bitfields(45-51): not use**************/
	// Reserved 1 byte -> Not use
	
	
	/*************** Order Restated Bitfields (53-58): not use *******************/
	// Reserved 1 byte -> Not use
	
	
	/***********User Modify Rejected Bitfields(61-67) -> Not use**********/
	// Reserved 1 byte -> Not use
	

	/*************Order Cancelled Bitfields (69-75)***************/
	//Bitfield1 (1:Side)
	message[69] = 0x01; // 00000001 (Side)
	
	//Bitfield2 (1:Symbol)
	message[70] = 0x01; // 00000001 (Symbol)
	
	//Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
	message[71] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
	
	//Bitfield4: not use
	
	//Bitfield5 (1:OrigClOrdID, 2:LeavesQty, 3:LastShares, 4:LastPx, 5:DisplayPrice, 6:WorkingPrice, 7:BaseLiquidity, 8:ExpireTime)
	message[73] = 0x4F; // 01001111 (OrigCldOrdID, LeavesQty, LastShares, LastPx, BaseLiquidity)
	
	//Bitfield6: not use
	//Bitfield7: not use
	// Reserved 1 byte -> Not use

	// Cancel Reject Bitfields (77-83) -> Filled value to 0 in memset
	// Reserved 1 byte -> Not use

	
	/***************Order Execution Bitfields(85-91)*****************/
	//Bitfield1 (1:Side, 2:PerDifference, 3:Price, 4:ExecInst, 5:OrdType, 6:TimeInForce, 7:MinQty, 8:MaxRemovePct)
	message[85] = 0x3D; // 00111101 (Side, Price, ExecInst, OrdType, TimeInForce)
	
	//Bitfield2 (1:Symbol, 2:SymbolSfx, 3:RESERVED, 4:RESERVED, 5:RESERVED, 6:RESERVED 7:Capacity, 8:RESERVED)
	message[86] = 0x43; // 01000011 (Symbol, SymbolSfx, Capacity)
	
	//Bitfield3 (1:Account, 2:ClearingFilm, 3:ClearingAccount, 4:DisplayIndicator, 5:MaxFloor, 6:Discretion Amount, 7:OrderQty, 8:PreventMember Match)
	message[87] = 0x7B; // 01111011 (Account, ClearingFilm, DisplayIndicator, Maxfloor, Discretion Amount, OrderQty)
	
	//Bitfield4: not use
	//Bitfield5: not use
	//Bitfield6: not use
	//Bitfield7: not use
	// Reserved 1 byte -> Not use

	
	/***********Trade Cancel Or Correct Bitfields(93-99): not use***********/
	// Reserved 1 byte -> Not use

	
	// Skip message(101 -> 116)

	// Number of Units
	message[117] = 0; // just use one byte

	// Unit Number and Unit Sequence	
  
  
	return SendMsg(connection->socket, &connection->lock, message, maxLength);
}

int BuildAndSend_BATS_BOE_LogoutRequestMsg(t_Connection* connection)
{
  unsigned short maxLength = 10;
  
	char message[maxLength];
	memset(message, 0, maxLength);

	// Start of Message: 2 bytes(0-1)
	message[0] = 0xBA;
	message[1] = 0xBA;

	// Message length: 2 bytes(2-3)
	message[2] = 0x08;

	// Message type: 1 byte
	message[4] = 0x02;

	// Matching Unit 1 byte -> Not use
	// Sequence number 4 bytes(6-9) -> Not use

	return SendMsg(connection->socket, &connection->lock, message, maxLength);
}

int BuildAndSend_BATS_BOE_OrderMsg(t_Connection* connection, const char* clOrdId, 
      const char* symbol, const char side, const int quantity, const long price, const char tif)
{
  unsigned short maxLength = 256;
  
	char message[maxLength];
	memset(message, 0, maxLength);

	// Start of Message: 2 bytes(0-1)
	message[0] = 0xBA;
	message[1] = 0xBA;

	// Message length: 2 bytes: WILL BE UPDATED AFTER

	// Message type: 1 byte
	message[4] = 0x04;

	// Matching Unit: 1 byte->Not use

	//Sequence number: 4 bytes
	int seqNum = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
	memcpy(&message[6], &seqNum, 4);

	//Client Order ID: 20 bytes
	strncpy(&message[10], clOrdId, 20);

	//Side
	message[30] = side;

	//OrderQty
	memcpy(&message[31], &quantity, 4);
	
	/*
	 * New order bitfield1
	 * bit1:ClearingFilm, bit2:ClearingAccount, bit3:Price, bit4:ExecInst
	 * bit5:OrderType, bit6:TimeInForce, bit7:MinQty, bit8:MaxFloor
	*/
	message[35] = 0x35; // 00110101 (ClearingFilm, Price, OrdType, TimeInForce)
	
	/*
	 * New order bitfield2
	 * bit1:Symbol, bit2:SymbolSfx, bit3:RESERVED, bit4:RESERVED
	 * bit5:RESERVED, bit6:RESERVED, bit7:Capacity, bit8:RoutingInst
	*/
	message[36] = 0xC1; // 11000001 (Symbol, Capacity, RoutingInst)

	/*
	 * New order bitfield3
	 * bit1:Account, bit2:DisplayIndicator, bit3:MaxRemovePct, bit4:DiscretionAmount
	 * bit5:PefDifference, bit6:PreventMemberMatch, bit7:LocateReqd, bit8:ExpireTime
	*/
	message[37] = 0x03; // 00000011 (Account, DisplayIndicator)

	//bitfield4: Not use
	//bitfield5: Not use
	//bitfield6: Not use

	register int indexOptionFields = 41;

	// ClearingFilm
	strncpy(&message[indexOptionFields], MPID_BATSZ_BOE, 4);
	indexOptionFields += 4;

	// Price: 8 bytes
	memcpy(&message[indexOptionFields], &price, 8);
	indexOptionFields += 8;

	//OrdType: 1 byte
	message[indexOptionFields] = '2';
	indexOptionFields += 1;

	// TimeInForce: 1 byte
	message[indexOptionFields] = tif;
	indexOptionFields += 1;

	//Symbol: 8 bytes
	strncpy(&message[indexOptionFields], symbol, 8);
	indexOptionFields += 8;

	// Capacity: 1 byte
	message[indexOptionFields] = 'P';
	indexOptionFields += 1;

	// RoutingInst: 4 bytes
  strncpy(&message[indexOptionFields], "B", 4);
  indexOptionFields += 4;

	//Account
	strcpy(&message[indexOptionFields], "_account_");
	indexOptionFields += 16;
	
	//DisplayIndicator
	message[indexOptionFields] = 'V';
	indexOptionFields += 1;

	// Update message length
	int msgLength = indexOptionFields - 2; /*not including 2 bytes of start of message*/
	memcpy(&message[2], &msgLength, 2);

  return SendMsg(connection->socket, &connection->lock, message, indexOptionFields);
}

int BuildAndSend_BATS_BOE_CancelMsg(t_Connection* connection, const char* origClOrdId, const char* symbol)
{
   unsigned short maxLength = 36;
  
	char message[maxLength];
	memset(message, 0, maxLength);

	// Start of Message: 2 bytes(0-1)
	message[0] = 0xBA;
	message[1] = 0xBA;

	// Message length: 2 bytes:
  unsigned short mgsLength = maxLength - 2; // Not including 2 bytes of start of message
	memcpy(&message[2], &mgsLength, 2);
  
	// Message type: 1 byte
	message[4] = 0x05;

	// Matching Unit: 1 byte->Not use

	//Sequence number: 4 bytes
	int seqNum = __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1);
	memcpy(&message[6], &seqNum, 4);

	//Client Order ID: 20 bytes
	strncpy(&message[10], origClOrdId,  20);

	// bitfield 1
	message[30] = 0x01; // 0000.0001 (ClearingFilm)
  
  // bitfield 2
  
  // ClearingFilm
  strncpy(&message[32], MPID_BATSZ_BOE, 4);
  
 return SendMsg(connection->socket, &connection->lock, message, maxLength);
}

int BuildAndSend_BATS_BOE_Order(t_Connection* connection, char* clOrdId, char* symbol, char* s, char* q, char* p, char* t)
{
  char side = (strcmp(s, "b")==0 || strcmp(s, "B")==0) ? BATS_BOE_SIDE_BUY :
              (strcmp(s, "s")==0 || strcmp(s, "S")==0) ? '2' : '5';
  int quantity = atoi(q);
  long price = (long)(atof(p) * 10000.0);
  char tif = (strcmp(t, "ioc")==0 || strcmp(t, "IOC")==0) ? BATS_BOE_TIMEINFORCE_IOC: BATS_BOE_TIMEINFORCE_DAY;
  
  return BuildAndSend_BATS_BOE_OrderMsg(connection, clOrdId, symbol, side, quantity, price, tif);
}

int BuildAndSend_BATS_BOE_Cancel(t_Connection* connection, char* origClOrdId, char* symbol, char* s, char* q)
{
  return BuildAndSend_BATS_BOE_CancelMsg(connection, origClOrdId, symbol);
}

void TEST_BATS_printInfo (t_Connection* connection, t_DataBlock* data)
{
  if (data->msgContent[4] == 0x11)
  {
    char clOrdId[20];
    memcpy(clOrdId, &data->msgContent[18], 20);
    unsigned int quantity = *(unsigned int*)(&data->msgContent[46]);
    unsigned long price = *(unsigned long*)(&data->msgContent[50]);
    
    TraceLog(DEBUG_LEVEL, "BATS FILL %s: NUMBER %d OF %s IS %s AT %f\n", clOrdId, quantity, "XXX", "YYY", price / 10000.0f);
  }
  
  if (data->msgContent[4] == 0x0F)
  {
    char clOrdId[20];
    memcpy(clOrdId, &data->msgContent[18], 20);
    
    TraceLog(DEBUG_LEVEL, "BATS CANCEL %s: \n", clOrdId);
  }
}

static int HandleCommandLine(t_Connection* connection, t_Parameters* para)
{
  int i;
  
  for (i = 0; i < para->countParameters; i++)
  {
    t_Parameters argv;
    memset(&argv, 0, sizeof(t_Parameters));
    Lrc_Split(para->parameterList[i], ' ', &argv);
    // TraceLog(DEBUG_LEVEL, "*** check parameters = %s; %s; %d\n", para->parameterList[i], argv.parameterList[0], argv.countParameters);
    if (strcmp(argv.parameterList[0], "exit") == 0)
    {
      testExit = 1;
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "quit") == 0)
    {
      testExit = 1;
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "logon") == 0)
    {
      //send logon
      BuildAndSend_BATS_BOE_LoginRequestMsg(connection, "_se_", "_us_", "_pass_");
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "logout") == 0)
    {
      //send logout
      BuildAndSend_BATS_BOE_LogoutRequestMsg(connection);
      connection->isAlive = 0;
      return 1;
    }
    
    if (strcmp(argv.parameterList[0], "heartbeat") == 0)
    {
      //handle heartbeat
      BuildAndSend_BATS_BOE_Client_HeartbeatMsg(connection);
      continue;
    }

    if (strcmp(argv.parameterList[0], "order") == 0)
    {
      //handle new order
      if (argv.countParameters < 7)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -order <token> <symbol> <side> <quantity> <price> <tif>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      char *symbol =  argv.parameterList[2];
      char side = !strcmp(argv.parameterList[3], "b")  ? BATS_BOE_SIDE_BUY :
                  !strcmp(argv.parameterList[3], "s")  ? '2' : '5';
      int quantity = atoi(argv.parameterList[4]);
      double price = atof(argv.parameterList[5]);
      long longPx = (long) (price * 10000.0);
      char tif = !strcmp(argv.parameterList[6], "ioc")? BATS_BOE_TIMEINFORCE_IOC: BATS_BOE_TIMEINFORCE_DAY;
      
      BuildAndSend_BATS_BOE_OrderMsg(connection, token, symbol, side, quantity, longPx, tif);
      continue;
    }
    
    if (strcmp(argv.parameterList[0], "cancel") == 0)
    {
      //handle cancel order
      if (argv.countParameters < 5)
      {
        TraceLog(DEBUG_LEVEL, "*** Usage: -cancel <token> <symbol> <side> <quantity>\n");
        continue;
      }
      
      char *token =  argv.parameterList[1];
      char *symbol =  argv.parameterList[2];
      // char side = !strcmp(argv.parameterList[3], "b")  ? BATS_BOE_SIDE_BUY :
                  // !strcmp(argv.parameterList[3], "s")  ? '2' : '5';
      // int quantity = atoi(argv.parameterList[4]);
      
      BuildAndSend_BATS_BOE_CancelMsg(connection, token, symbol);
      continue;
    }
  }
  
  return 0;
}

static int ParseCommandLine(char* cmdLine, t_Parameters* para)
{
  Lrc_Split(cmdLine, '-', para);
  
  return para->countParameters;  
}

static int GetCommandLine(t_Parameters* para)
{
  char cmdLine[128];
  
  fgets(cmdLine, 128, stdin);
  // if (cmdLine == NULL)
  // {
    // return 0;
  // }

  return ParseCommandLine(cmdLine, para);
}

void* StartBATSTestThread (void* data)
{
  t_Parameters para;
  t_Connection* connection = (t_Connection*) data;
  
  while (1)
  {
    TraceLog(DEBUG_LEVEL, "Get command line >>\n");
    memset(&para, 0, sizeof(t_Parameters));
    GetCommandLine(&para);
    if (HandleCommandLine(connection, &para) == 1)
      break;
    usleep(1000);
  }  

  return NULL;
}

static void *StartBATSDebugThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    t_DataBlock dataBlock;
    
    if(ReceiveMsg(connection, &dataBlock) == ERROR)
    {
      TraceLog(ERROR_LEVEL, "StartBATSDebugThread: receive msg failed\n");
      break;
    }
    
    if (dataBlock.msgContent[4] == 0x11)
    {
      char clOrdId[20];
      memcpy(clOrdId, &dataBlock.msgContent[18], 20);
      unsigned int quantity = *(unsigned int*)(&dataBlock.msgContent[46]);
      unsigned long price = *(unsigned long*)(&dataBlock.msgContent[50]);
      
      TraceLog(DEBUG_LEVEL, "BATS FILL %s: NUMBER %d OF %s IS %s AT %f\n", clOrdId, quantity, "XXX", "YYY", price / 10000.0f);
    }
    
    if (dataBlock.msgContent[4] == 0x0F)
    {
      char clOrdId[20];
      memcpy(clOrdId, &dataBlock.msgContent[18], 20);
      
      TraceLog(DEBUG_LEVEL, "BATS CANCEL %s: \n", clOrdId);
    }
  }
 
  return NULL;
}

void BATSForTesting ()
{
  struct sockaddr_in server_addr;
  
  int configIdx = 0;
  for (configIdx = 0; configIdx < Configs.countPort; configIdx++)
    if (Configs.portList[configIdx].venue == VENUE_BYX)
      break;
  
  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(Configs.portList[configIdx].port);
  if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot convert net address\n");
    return;
  }
  
  while (!testExit)
  {
    int testSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (testSocket == -1)
    {
      TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
      return;
    }  

    if(connect(testSocket, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Cannot connect to server\n");
      return;
    }
    
    t_UserInfo userInfo;
    t_Connection connection;
  
    strcpy(userInfo.userName, "TEST");
    userInfo.outgoingSeq = 0;
    
    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 1;
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);
    
    pthread_t testThread;
    pthread_create(&testThread, NULL, StartBATSTestThread, &connection);
    pthread_t dbgThread;
    pthread_create(&dbgThread, NULL, StartBATSDebugThread, &connection);
    
    pthread_join(testThread, NULL);
    
    close(testSocket);
  }
}