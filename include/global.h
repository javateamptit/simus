#ifndef _GLOBAL_
#define _GLOBAL_

#include "config.h"
#include "order_token_queue.h"

#include <time.h>
#include <pthread.h>
#include <netinet/in.h>

enum { //<thiennt> added for new venues
  VENUE_ARCA = 0,
  VENUE_OUCH,
  VENUE_BX,
  VENUE_RASH,
  VENUE_NYSE,
  VENUE_BATSZ,
  VENUE_EDGX,
  VENUE_EDGA,
  VENUE_BYX,
  VENUE_CHX,
  VENUE_PSX,
  VENUE_NSX,
  VENUE_LAVA,
  MAX_VENUE_TYPE
};

#define MAX_CONNECTION_PER_PORT 10


typedef struct BITFIELDS
{
  char              ACK[7];
  char              Rejected[7];
  char              Modified[7];
  char              Restates[7];
  char              ModifyReject[7];
  char              Cancelled[7];
  char              CancelReject[7];
  char              Execution[7];
  char              Correct[7];
} t_BitFields;

typedef struct USERINFO
{
  char              userName[25];
  char              password[10];
  unsigned long     incomingSeq;
  unsigned long     outgoingSeq;
  pthread_mutex_t   outgoingLock;
} t_UserInfo;

typedef struct CONFIGITEM
{
  unsigned short    port;         // port number
  int               index;        // port index of global Configs
  int               venue;        // venue index
  int               type;         // socket type : UDP or TCP
  double            fillRate;     // fill rate
} t_ConfigItem;

typedef struct CONFIGS
{
  int               countPort;                  // number of port 
  t_ConfigItem      portList[MAX_VENUE_TYPE];   // list of port config
} t_Configs;

typedef struct CONNECTION
{
  t_UserInfo*         userInfo;      // info of user
  t_ConfigItem*       configInfo;    // info of port config
  
  int                 clientIndex;    // connection index of global Connections
  struct sockaddr_in  clientAddr;     // address of client
  int                 socket;         // client connection socket
  int                 isAlive;        // still alive or not
  pthread_spinlock_t  lock;           // spin lock
  
  t_BitFields         bitFields;
  char                noUnspecifiedUnitReplay;
  short               messageVersionProfile[16];
  int                 isCancelOnDisconnect;
  int                 disableHeartbeat;
  int                 heartbeatInterval;
  time_t              lastClientBeat;
  
  
  t_OrderTokenQueue   orderQueue;
  unsigned long       executionNumber;
  unsigned long       orderID;
} t_Connection;


extern t_Configs Configs;
extern int HeaderBytes[MAX_VENUE_TYPE];
extern char *VenueName[MAX_VENUE_TYPE];
extern t_Connection Connections[MAX_VENUE_TYPE][MAX_CONNECTION_PER_PORT];
extern t_UserInfo UserAccounts[MAX_VENUE_TYPE][MAX_CONNECTION_PER_PORT];

typedef int (*t_Fill) (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum);
typedef int (*t_SystemCancel) (t_Connection* connection, t_QueueItem* item);
typedef int (*t_UserCancel) (t_Connection* connection, t_QueueItem* item, const int quantity);
typedef int (*t_CancelReject) (t_Connection* connection, t_OrderEntry *orderEntry);

int Simulator_Handle_CancelOrder(t_Connection* connection, t_OrderEntry *cancelEntry, t_UserCancel cancel, t_CancelReject reject);
int Simulator_Handle_CrossOrder(t_Connection* connection, t_QueueItem *item, const char buySide, t_Fill fill);
int Simulator_Handle_RandomIOC(t_Connection* connection, t_QueueItem *item, t_Fill fill, t_SystemCancel cancel);
int Simulator_Handle_RandomDAY(t_Connection* connection, t_Fill fill, t_SystemCancel cancel);
int Simulator_Add_DAYOrder(t_OrderTokenQueue* queue, t_QueueItem *item);

void Simulator_Initialize_User(t_Connection* connection);

#endif
