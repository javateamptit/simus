#include "nsx.h"
#include "fix_protocol.h"
#include "network.h"


static int BuildCommonMsgHeaderField(t_Connection* connection, char* msg, const char msgType)
{
  int length = 0;
  time_t now;
  time(&now);
  
  length += FIX_AddTagValueChar(&msg[length], FIX_TAG_MSG_TYPE, msgType);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_SENDER_COMPID, NSX_COMPID);
  length += FIX_AddTagValueString(&msg[length], FIX_TAG_TARGET_COMPID, connection->userInfo->userName);
  length += FIX_AddTagValueInt(&msg[length], FIX_TAG_MSG_SEQNO, __sync_add_and_fetch(&connection->userInfo->outgoingSeq, 1));
  length += FIX_AddTagValueTime(&msg[length], FIX_TAG_SENDING_TIME, now);
  
  return length;  
}

static int BuildMsgHeaderField(char* msg, const int bodyLength)
{
  int i, length;
  
  // "8=FIX4.2<SOH>9=23<SOH>"
  sprintf(msg, "%s%d=%d\001", NSX_MSG_BEGIN_STRING, FIX_TAG_BODY_LENGTH, bodyLength);
  length = strlen(msg);
  
  if (FIX_RESERVE_HEADER_LEN > length)
    for (i = 0; i < length; i++)
      msg[FIX_RESERVE_HEADER_LEN - i - 1] = msg[length - i - 1];
      
  return length;
}

static char* BuildMsgTrailerField(char* msg, const int headerLen, const int bodyLength)
{
  int offset;
  unsigned int checksum;
  
  offset = FIX_RESERVE_HEADER_LEN - headerLen;  
  checksum = FIX_CalculateChecksum(&msg[offset], headerLen + bodyLength);
  
  // "10=023<SOH>"
  sprintf(&msg[offset + headerLen + bodyLength], "%d=%03d\001", FIX_TAG_CHECKSUM, checksum);
      
  return &msg[offset];
}

static int BuildAndSendHeartBeat(t_Connection* connection)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_HEARTBEAT);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendLogonResponse(t_Connection* connection)
{
  // NSX will NOT send the Logon message
  return SUCCESS;
}

static int BuildAndSendLogoutResponse(t_Connection* connection)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_LOGOUT);  
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendResendRequest(t_Connection* connection, const int beginSeq, const int endSeq)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_RESEND_REQUEST);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_BEGIN_SEQNO, beginSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_END_SEQNO, endSeq);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendSequenceReset(t_Connection* connection, const int newSeq)
{
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_SEQ_RESET);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_NEW_SEQNO, newSeq);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendOrderCancelReject(t_Connection* connection,
    const char* clOrdID, const char* origClOrdID, const char* clientId,
    const char cxlRejResponseTo, const int cxlRejReason, const char* text)
{
  char orderId[16];
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  sprintf(orderId, "%ld", connection->orderID);
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_CANCEL_REJECT);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 37, orderId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdID);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, origClOrdID);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 434, cxlRejResponseTo);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 102, cxlRejReason);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, text);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 109, clientId);
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

// static int BuildAndSendReject(t_Connection* connection, const int errSeq)
// {
  // char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  // int headerLen, bodyLen;
  //char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_REJECT);
  // bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_REF_SEQNO, errSeq);
  // headerLen = BuildMsgHeaderField(msg, bodyLen);
  // message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  // return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
// }

static int BuildAndSendExecutionReport(t_Connection* connection,
    const char ordStatus, const char* clOrdID, const char* clientId,
    const char execTransType, const char* symbol, const char side, const char orderType,
    const char execType, const int orderQty, const int cumQty, const int leavesQty, const double avgPx,
    const char handlInst, const int lastShares, const double lastPx,
    const double price, const char orderCapacity, const char* origClOrdID, const char* text)
{
  char orderId[16];
  char msg[NSX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  time_t now;
  time(&now);
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  sprintf(orderId, "%ld", connection->orderID);
  
  bodyLen = BuildCommonMsgHeaderField(connection, message, FIX_MSG_TYPE_EXECUTION_REPORT);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 37, orderId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdID);
  if (origClOrdID != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, origClOrdID);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 109, clientId);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 17, __sync_add_and_fetch(&connection->executionNumber, 1));
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 20, execTransType);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 150, execType);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 39, ordStatus);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, side);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 38, orderQty);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 40, orderType);
  if (price > 0)
    bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 44, price);
  if (orderCapacity != 0)
    bodyLen += FIX_AddTagValueChar(&message[bodyLen], 47, orderCapacity);
  if (lastShares > 0)
    bodyLen += FIX_AddTagValueInt(&message[bodyLen], 32, lastShares);
  if (lastPx > 0)
    bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 31, lastPx);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 151, leavesQty);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 14, cumQty);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 6, avgPx);
  bodyLen += FIX_AddTagValueTime(&message[bodyLen], 60, now);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 21, handlInst);
  if (text != NULL)
    bodyLen += FIX_AddTagValueString(&message[bodyLen], 58, text);
    
  headerLen = BuildMsgHeaderField(msg, bodyLen);
  message = BuildMsgTrailerField(msg, headerLen, bodyLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendCanceled(t_Connection* connection, t_QueueItem* item)
{
  t_OrderEntry *orderEntry = &item->orderEntry;
  double orgPx = orderEntry->price / 10000.0;
  double avgPx = (item->totalPx / 10000.0) / item->cumShare;
  
  return BuildAndSendExecutionReport(connection,
          FIX_ORDER_STATUS_CANCELLED, orderEntry->clOrdID, orderEntry->clientId,
          FIX_EXEC_TRANS_TYPE_NEW, orderEntry->symbol, orderEntry->side, orderEntry->orderType,
          FIX_EXEC_TYPE_CANCELLED, orderEntry->quantity, item->cumShare, 0, avgPx,
          FIX_HANDL_INST_AUTO_NO_BROKER, 0, 0,
          orgPx, 0, NULL, NULL);
}

static int BuildAndSendFilled(t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  t_OrderEntry *orderEntry = &item->orderEntry;
  double curPx = price / 10000.0;
  double orgPx = orderEntry->price / 10000.0;
  double avgPx = (item->totalPx / 10000.0) / item->cumShare;
  const int isFilled = item->orderEntry.quantity == item->cumShare;
  
  return BuildAndSendExecutionReport(connection,
      isFilled? FIX_ORDER_STATUS_FILLED : FIX_ORDER_STATUS_PARTIALLY, orderEntry->clOrdID, orderEntry->clientId,
      FIX_EXEC_TRANS_TYPE_NEW, orderEntry->symbol, orderEntry->side, orderEntry->orderType,
      isFilled? FIX_EXEC_TYPE_FILL : FIX_EXEC_TYPE_PARTIAL, orderEntry->quantity, item->cumShare, orderEntry->quantity - item->cumShare, avgPx,
      FIX_HANDL_INST_AUTO_NO_BROKER, quantity, curPx,
      orgPx, 0, NULL, NULL);
}

static int HandleMsgLogon(t_Connection* connection, t_DataBlock *dataBlock)
{
  Simulator_Initialize_User(connection);
  
  char strValue[16];
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_SENDER_COMPID, connection->userInfo->userName);
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_ENCRYPT_METHOD, strValue);
  if (atoi(strValue) != 0)
    return ERROR;
    
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_HEARBEAT_INT, strValue);
  if (atoi(strValue) != 30)
    return ERROR;
  
  return BuildAndSendLogonResponse(connection);
}

static int HandleMsgLogout(t_Connection* connection, t_DataBlock *dataBlock)
{
  connection->isAlive = 0;
  return BuildAndSendLogoutResponse(connection);
}

static int HandleMsgResendRequest(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  int beginSeq/* , endSeq */;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_BEGIN_SEQNO, strValue);
  beginSeq = atoi(strValue);
  
  // FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_END_SEQNO, strValue);
  // endSeq = atoi(strValue);
  
  return BuildAndSendSequenceReset(connection, beginSeq);
}

static int HandleMsgSequenceReset(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  int newSeq;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_NEW_SEQNO, strValue);
  newSeq = atoi(strValue);
  
  connection->userInfo->incomingSeq = newSeq - 1;
  
  return SUCCESS;
}

static int HandleMsgNewOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[64];
  char clientId[64];
  char orderType, orderCapacity;
  double price;
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  FIX_GetTagValue (dataBlock->msgContent, 11, strValue);
  strcpy(orderEntry.clOrdID, strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 21, strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 38, strValue);
  orderEntry.quantity = atoi(strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 40, strValue);
  orderType = strValue[0];
  
  FIX_GetTagValue (dataBlock->msgContent, 44, strValue);
  price = atof(strValue);
  orderEntry.price = (int) (price * 10000.0);
  
  FIX_GetTagValue (dataBlock->msgContent, 47, strValue);
  orderCapacity = strValue[0];
  
  FIX_GetTagValue (dataBlock->msgContent, 54, strValue);
  orderEntry.side = strValue[0];
  
  FIX_GetTagValue (dataBlock->msgContent, 55, strValue);
  strcpy(orderEntry.symbol, strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 59, strValue);
  orderEntry.tif = (int) strValue[0];
  
  FIX_GetTagValue (dataBlock->msgContent, 60, strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 109, strValue);
  strcpy(clientId, strValue);  
  
  if(orderEntry.tif != '0' && orderEntry.tif != '3')
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    BuildAndSendExecutionReport(connection,
        FIX_ORDER_STATUS_REJECTED, orderEntry.clOrdID, clientId,
        FIX_EXEC_TRANS_TYPE_NEW, orderEntry.symbol, orderEntry.side, orderType,
        FIX_EXEC_TYPE_REJECTED, orderEntry.quantity, 0, 0, 0,
        FIX_HANDL_INST_AUTO_NO_BROKER, 0, 0,
        price, orderCapacity, NULL, "Reject by TIF");
  }
  else if(orderEntry.side != '1' && orderEntry.side != '2' && orderEntry.side != '5' && orderEntry.side != '6')
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    BuildAndSendExecutionReport(connection,
        FIX_ORDER_STATUS_REJECTED, orderEntry.clOrdID, clientId,
        FIX_EXEC_TRANS_TYPE_NEW, orderEntry.symbol, orderEntry.side, orderType,
        FIX_EXEC_TYPE_REJECTED, orderEntry.quantity, 0, 0, 0,
        FIX_HANDL_INST_AUTO_NO_BROKER, 0, 0,
        price, orderCapacity, NULL, "Reject by Side");
  }
  else if((orderEntry.quantity < 1) || (orderEntry.quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    BuildAndSendExecutionReport(connection,
        FIX_ORDER_STATUS_REJECTED, orderEntry.clOrdID, clientId,
        FIX_EXEC_TRANS_TYPE_NEW, orderEntry.symbol, orderEntry.side, orderType,
        FIX_EXEC_TYPE_REJECTED, orderEntry.quantity, 0, 0, 0,
        FIX_HANDL_INST_AUTO_NO_BROKER, 0, 0,
        price, orderCapacity, NULL, "Reject by Quantity");
  }
  else
  {
    // ACK
    BuildAndSendExecutionReport(connection,
        FIX_ORDER_STATUS_ACK, orderEntry.clOrdID, clientId,
        FIX_EXEC_TRANS_TYPE_NEW, orderEntry.symbol, orderEntry.side, orderType,
        FIX_EXEC_TYPE_ACK, orderEntry.quantity, 0, orderEntry.quantity, 0,
        FIX_HANDL_INST_AUTO_NO_BROKER, 0, 0,
        price, orderCapacity, NULL, NULL);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.orderEntry.orderType = orderType;
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;

    // Detect cross
    int cross = Simulator_Handle_CrossOrder(connection, &item, FIX_SIDE_BUY, &BuildAndSendFilled);
    //IOC Random
    if (orderEntry.tif == FIX_TIMEINFORCE_IOC)
      if (cross == -1)
        Simulator_Handle_RandomIOC(connection, &item, &BuildAndSendFilled, &BuildAndSendCanceled);
      else
        BuildAndSendCanceled(connection, &item);
    else if (item.orderEntry.quantity > item.cumShare) // DAY
      AddOrderToken(&connection->orderQueue, &item);
  }
  
  return SUCCESS;
}

static int HandleMsgCancelOrder(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[32], origClOrdId[32], clOrdId[32], symbol[32], clientId[32];
  //char side;
  //int orderQty;  
  
  t_QueueItem item;
  FIX_GetTagValue (dataBlock->msgContent, 41, origClOrdId); //OrigClOrdId
  
  FIX_GetTagValue (dataBlock->msgContent, 11, clOrdId);
  
  FIX_GetTagValue (dataBlock->msgContent, 38, strValue);
  //orderQty = atoi(strValue);
  
  FIX_GetTagValue (dataBlock->msgContent, 55, symbol);
  
  FIX_GetTagValue (dataBlock->msgContent, 54, strValue);
  //side = strValue[0];
  
  FIX_GetTagValue (dataBlock->msgContent, 60, strValue); //TransactTime
  
  FIX_GetTagValue (dataBlock->msgContent, 109, clientId);
  
  strcpy(item.orderEntry.clOrdID, origClOrdId);
  
  int res = SearchAndRemoveToken(&connection->orderQueue, &item);
  if(res == SUCCESS)
  {
    BuildAndSendCanceled(connection, &item);
  }
  else//send order cancel reject
  {
    BuildAndSendOrderCancelReject(connection, clOrdId, origClOrdId, item.orderEntry.clientId, '1', 1, "not found/opened");
  }
  
  return SUCCESS;
}

static void* DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    Simulator_Handle_RandomDAY(connection, &BuildAndSendFilled, &BuildAndSendCanceled);
    
    sleep(15);
  }
  
  return NULL;
}

static void* HeartbeatThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;
  
  while(connection->isAlive)
  {
    BuildAndSendHeartBeat(connection);
    sleep(30);
  }
  
  TraceLog(DEBUG_LEVEL, "Close HeartbeatThread\n");
  return NULL;
} 

int HandleMsg_NSX(t_Connection* connection, t_DataBlock *dataBlock)
{
  char strValue[16];
  char msgType;
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_BEGIN_STRING, strValue);
  if(strcmp(strValue, NSX_BEGIN_STRING) != 0)
  {
    TraceLog(ERROR_LEVEL, "Not NSX FIX4.2 protocol (%s)\n", strValue);
    return ERROR;
  }
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_TARGET_COMPID, strValue);
  if(strcmp(strValue, NSX_COMPID) != 0)
  {
    TraceLog(ERROR_LEVEL, "Not NSX target (%s)\n", strValue);
    return ERROR;
  }
  
  FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_MSG_TYPE, strValue);
  msgType = strValue[0];
  
  if (connection->userInfo != NULL && msgType != FIX_MSG_TYPE_SEQ_RESET)
  {
    FIX_GetTagValue (dataBlock->msgContent, FIX_TAG_MSG_SEQNO, strValue);
    int incomingSeq = atoi(strValue);

    if (incomingSeq == connection->userInfo->incomingSeq + 1)
    {
      connection->userInfo->incomingSeq++;
    }
    else if (incomingSeq < connection->userInfo->incomingSeq + 1)
    {
      connection->isAlive = 0;
      return ERROR;
    }
    else
    {
      BuildAndSendResendRequest(connection, connection->userInfo->incomingSeq + 1, 0);
      return SUCCESS;
    }    
  }
  
  switch(msgType)
  {
    case FIX_MSG_TYPE_LOGON:
      TraceLog(DEBUG_LEVEL, "Received login message from connection\n");
      if(HandleMsgLogon(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, HeartbeatThread, (void*)connection);
      }
      break;
      
    case FIX_MSG_TYPE_NEW_ORDER:
      TraceLog(DEBUG_LEVEL, "Received new order message from connection\n");
      HandleMsgNewOrder(connection, dataBlock);
      break;
      
    case FIX_MSG_TYPE_CANCEL_ORDER:
      TraceLog(DEBUG_LEVEL, "Received cancel order message from connection\n");
      HandleMsgCancelOrder(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_LOGOUT:
      TraceLog(DEBUG_LEVEL, "Received logout message from connection\n");
      HandleMsgLogout(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_RESEND_REQUEST:
      TraceLog(DEBUG_LEVEL, "Received resend request message from connection\n");
      HandleMsgResendRequest(connection, dataBlock);
      break;
    
    case FIX_MSG_TYPE_SEQ_RESET:
      TraceLog(DEBUG_LEVEL, "Received sequence reset message from connection\n");
      HandleMsgSequenceReset(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown MsgType (%c)\n", msgType);
      break;
  }

  return SUCCESS;
}