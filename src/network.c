
#include "network.h"
#include "global.h"
#include "fix_protocol.h"

#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/tcp.h>

static int GetBodyBytes(t_Connection *connection, t_DataBlock* dataBlock)
{
  int venue = connection->configInfo->venue;
  
  switch(venue)
  {
    case VENUE_OUCH:
    case VENUE_BX:
    case VENUE_EDGX:
    case VENUE_EDGA:
    case VENUE_LAVA:  //<thiennt> added
    case VENUE_PSX:  //<thiennt> added
      return (ushortAt((unsigned char*)dataBlock->msgContent, 0) + 2);
      
    case VENUE_ARCA:
    case VENUE_NYSE:
      return ushortAt((unsigned char*)dataBlock->msgContent, 2);
      
    case VENUE_BATSZ:
    case VENUE_BYX:  //<thiennt> added
    {
      short len = 0;
      memcpy(&len, &dataBlock->msgContent[2], 2);
      return (len + 2);
    }
    
    default:
      TraceLog(ERROR_LEVEL, "%s: Unknown venue(%d)\n", __func__, venue);
      break;
  }
  
  return SUCCESS;
}

static int ReceivedDataBytes(t_Connection *connection, t_DataBlock* dataBlock, int startIndex, int size)
{
  int sock = connection->socket;
  int venue = connection->configInfo->venue;
  
  int receivedBytes = startIndex,
    recvBytes = 0;
  
  while(receivedBytes < size)
  {
    errno = 0;
    recvBytes = recv(sock, &dataBlock->msgContent[receivedBytes], size, 0);

    if(recvBytes > 0)
    {
      receivedBytes += recvBytes;
    }
    else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
    {
      TraceLog(ERROR_LEVEL, "%s: Socket receive-timeout\n", VenueName[venue]);
      return ERROR;
    }
    else if ( (recvBytes == 0) && (errno == 0) )  //Server Disconnected
    {
      TraceLog(ERROR_LEVEL, "%s: Connection is terminated\n", VenueName[venue]);
      return ERROR;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s: recv() returned %d, errno: %d\n", VenueName[venue], receivedBytes, errno);
      return ERROR;
    }
  }
  
  return SUCCESS;
}

static int ReceivedHeaderBytes(t_Connection *connection, t_DataBlock* dataBlock)
{
  int headerBytes = HeaderBytes[connection->configInfo->venue];
  return ReceivedDataBytes(connection, dataBlock, 0, headerBytes);
}

static int ReceivedBodyBytes(t_Connection *connection, t_DataBlock* dataBlock)
{
  int receivedBytes = HeaderBytes[connection->configInfo->venue];
  return ReceivedDataBytes(connection, dataBlock, receivedBytes, dataBlock->msgLen - receivedBytes);
}

static int GetMsgLength(char msgType)
{
  int len ;
  switch(msgType)
  {
    // for msgType = 'U'
    case 'O': len = 140; break;
    case 'Q': len = 142; break;
    case 'X': len = 21;  break;
    // for msgType = 'S'
    case 'A': len = 157; break;
    case 'E': len = 49;  break;
    case 'C': len = 30;  break;
    case 'J': len = 24;  break;
    default:
      TraceLog(ERROR_LEVEL, "%s: Unknown msg type(%c)\n", __func__, msgType);
      len = ERROR;
      break;
  }
  
  return len;
}

int ReceiveMsg_FIX(t_Connection *connection, t_DataBlock *dataBlock)
{
  // receive first 20 bytes
  if (ReceivedHeaderBytes(connection, dataBlock) == ERROR)
    return ERROR;

  // calculate msg length
  char strValue[16];
  int headerLen = FIX_GetTagValue(dataBlock->msgContent, FIX_TAG_BODY_LENGTH, strValue);
  dataBlock->msgLen = headerLen + atoi(strValue) + FIX_MSG_CHECKSUM_LEN;
  
  // receive entire msg
  if (ReceivedBodyBytes(connection, dataBlock) == ERROR)
    return ERROR;
  
  dataBlock->msgContent[dataBlock->msgLen] = '\0';
  
  // check checksum
  if (FIX_GetTagValue(dataBlock->msgContent, FIX_TAG_CHECKSUM, strValue) == -1)
    return ERROR;
  
  if (FIX_CalculateChecksum(dataBlock->msgContent, dataBlock->msgLen-FIX_MSG_CHECKSUM_LEN) != (unsigned int)atoi(strValue))
    return ERROR;
    
  return SUCCESS;
}

int ReceiveMsg_RASH(t_Connection *connection, t_DataBlock *dataBlock)
{
  int sock = connection->socket;
  int venue = connection->configInfo->venue;
  int receivedBytes = HeaderBytes[venue];

  if(ReceivedHeaderBytes(connection, dataBlock) == ERROR)
  {
    return ERROR;
  }

  char packetType = dataBlock->msgContent[0];
  switch(packetType)
  {
    case 'L':
      dataBlock->msgLen = 38;
      break;
    case 'R':
    case 'O':
      dataBlock->msgLen = 2;
      break;
      
    case 'U':
      {
        dataBlock->msgLen = GetMsgLength(dataBlock->msgContent[1]) + 2;
        if(dataBlock->msgLen == ERROR)
        {
          return ERROR;
        }
      }
      break;
      
    case 'S':
      if(ReceivedDataBytes(connection, dataBlock, HeaderBytes[venue], 10) == ERROR)
      {
        return ERROR;
      } 
      dataBlock->msgLen = GetMsgLength(dataBlock->msgContent[9]) + 2;
      if(dataBlock->msgLen == ERROR)
      {
         return ERROR;
      }
      receivedBytes += 10;
      break;
    default:
      //TraceLog(ERROR_LEVEL, "%s: Unknown packet type(%c)\n", VenueName[venue], packetType);
      dataBlock->msgLen = -1;
      break;
  }
  
    int  recvBytes = 0;
  
  while(receivedBytes < dataBlock->msgLen)
  {
    errno = 0;
    recvBytes = recv(sock, &dataBlock->msgContent[receivedBytes], dataBlock->msgLen - receivedBytes, 0);

    if(recvBytes > 0)
    {
      receivedBytes += recvBytes;
      if(dataBlock->msgContent[receivedBytes - 1] == '\n')
      {
        break;
      }
      else if(receivedBytes == dataBlock->msgLen)
      {
        TraceLog(ERROR_LEVEL, "%s: -> Len :%d\n", VenueName[venue], dataBlock->msgLen);
        dataBlock->msgLen += GetMsgLength(dataBlock->msgContent[receivedBytes - 1]);
      }
      else
      {
        continue;
      }
    }
    else if ( (recvBytes == -1) && (errno == EAGAIN) ) //Timeout
    {
      TraceLog(ERROR_LEVEL, "%s: Socket receive-timeout.\n", VenueName[venue]);
      return ERROR;
    }
    else if ( (recvBytes == 0) && (errno == 0) )  //Server Disconnected
    {
      TraceLog(ERROR_LEVEL, "%s: Connection is terminated\n", VenueName[venue]);
      return ERROR;
    }
    else
    {
      TraceLog(ERROR_LEVEL, "%s: recv() returned %d, errno: %d\n", VenueName[venue], receivedBytes, errno);
      return ERROR;
    }
  }

  return SUCCESS;
}

int ReceiveMsg(t_Connection *connection, t_DataBlock *dataBlock)
{
  if(ReceivedHeaderBytes(connection, dataBlock) == SUCCESS)
  {
    dataBlock->msgLen = GetBodyBytes(connection, dataBlock);
    return ReceivedBodyBytes(connection, dataBlock);
  }
  else
  {
    return ERROR;
  }
}

int ReceiveMsgFrom(int sockfd, t_DataBlock *dataBlock, struct sockaddr* from, int* fromLen)
{
  dataBlock->msgLen = recvfrom(sockfd, dataBlock->msgContent, DATA_BUFF_SIZE, 0, from, (socklen_t *) fromLen);
  
  if (dataBlock->msgLen < 0)
    return ERROR;

  return SUCCESS;
}

int SendMsg(int sockfd, pthread_spinlock_t *lock, char* buff, int len)
{
  pthread_spin_lock(lock);
    write(sockfd, buff, len);
  pthread_spin_unlock(lock);

  return SUCCESS;
}

int SendMsgTo(int sockfd, pthread_spinlock_t *lock, char* buff, int len, struct sockaddr* to, int tolen)
{
  pthread_spin_lock(lock);
    sendto(sockfd, buff, len, 0, to, tolen);
  pthread_spin_unlock(lock);
  
  return SUCCESS;
}

int CloseConnection(t_Connection *connection)
{
  if(connection->socket > -1)
  {
    connection->isAlive = 0;
    close(connection->socket);
  }
  
  return SUCCESS;
}