#include "sim.h"
#include "global.h"
#include "network.h"
#include "fix_protocol.h"
#include "chx.h"

static int testExit = 0;

static int BuildAndSendHeartBeat(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_HEARTBEAT, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendLogon(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_LOGON, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_ENCRYPT_METHOD, 0);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_HEARBEAT_INT, 30);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendLogout(t_Connection* connection)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_LOGOUT, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendResendRequest(t_Connection* connection, const int beginSeq, const int endSeq)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_RESEND_REQUEST, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_BEGIN_SEQNO, beginSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_END_SEQNO, endSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  //trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendSequenceReset(t_Connection* connection, const int newSeq)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_SEQ_RESET, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_NEW_SEQNO, newSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);  
}

static int BuildAndSendReject(t_Connection* connection, const int errSeq)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_REJECT, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], FIX_TAG_REF_SEQNO, errSeq);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendNewOrder(t_Connection* connection,
    const char* clOrdId, const char* symbol, const char side, const int orderQty, const double price,
    const char tif, const char orderType, const char orderCapacity,  const char* clientId)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  time_t now;
  time(&now);
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_NEW_ORDER, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdId);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 21, FIX_HANDL_INST_AUTO_NO_BROKER);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, side);  
  bodyLen += FIX_AddTagValueInt(&message[bodyLen], 38, orderQty);  
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 40, orderType);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 44, price);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 47, orderCapacity);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 59, tif);
  bodyLen += FIX_AddTagValueTime(&message[bodyLen], 60, now);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 109, clientId);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);
  
  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int BuildAndSendCancelRequest(t_Connection* connection,
    const char* origClOrdId, const char* clOrdId, const char* symbol, const char side, const double orderQty)
{
  char msg[CHX_RESERVE_ENTIRE_MSG_LEN];
  int headerLen, bodyLen;
  char* message = &msg[FIX_RESERVE_HEADER_LEN];
  time_t now;
  time(&now);
  
  // body
  bodyLen = FIX_BuildCommonTagsField(message, FIX_MSG_TYPE_CANCEL_ORDER, connection->userInfo->userName, CHX_COMPID, (int*) &connection->userInfo->outgoingSeq);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 41, origClOrdId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 11, clOrdId);
  bodyLen += FIX_AddTagValueString(&message[bodyLen], 55, symbol);
  bodyLen += FIX_AddTagValueChar(&message[bodyLen], 54, side);
  bodyLen += FIX_AddTagValueDouble(&message[bodyLen], 38, orderQty);
  bodyLen += FIX_AddTagValueTime(&message[bodyLen], 60, now);
  // header
  message = FIX_BuildMsgWithHeaderField(msg, CHX_BEGIN_STRING, bodyLen, &headerLen);
  // trailer
  message = FIX_BuildMsgWithTrailerField(message, bodyLen, headerLen);

  return SendMsg(connection->socket, &connection->lock, message, headerLen + bodyLen + FIX_MSG_CHECKSUM_LEN);
}

static int HandleCommandLine(t_Connection* connection, t_Parameters* para)
{
  int i;
  char token[][16] = { "token_1", "token_2", "token_3", "token_4", "token_5" };
  char symbol[][16] = { "symbol_1", "symbol_2", "symbol_3", "symbol_4", "symbol_5" };
  int Idx = rand() % 5;
  
  for (i = 0; i < para->countParameters; i++)
  {
    // TraceLog(DEBUG_LEVEL, "check parameters = %s\n", para->parameterList[i]);
    
    if (strcmp(para->parameterList[i], "exit") == 0)
    {
      testExit = 1;
      return 1;
    }
    
    if (strcmp(para->parameterList[i], "quit") == 0)
    {
      testExit = 1;
      return 1;
    }
    
    if (strcmp(para->parameterList[i], "logon") == 0)
    {
      //send logon
      BuildAndSendLogon(connection);
      continue;
    }
    
    if (strcmp(para->parameterList[i], "logout") == 0)
    {
      //send logout
      BuildAndSendLogout(connection);
      return 1;
    }
    
    if (strcmp(para->parameterList[i], "heartbeat") == 0)
    {
      //handle heartbeat
      BuildAndSendHeartBeat(connection);
      continue;
    }
    
    if (strcmp(para->parameterList[i], "request") == 0)
    {
      //handle resend request
      BuildAndSendResendRequest(connection, 2, 3);
      continue;
    }
    
    if (strcmp(para->parameterList[i], "reset") == 0)
    {
      //handle sequence reset
      BuildAndSendSequenceReset(connection, 2);
      connection->userInfo->outgoingSeq = 1;
      continue;
    }
    
    if (strcmp(para->parameterList[i], "reject") == 0)
    {
      //handle reject
      BuildAndSendReject(connection, 2);
      continue;
    }
    
    if (strcmp(para->parameterList[i], "new") == 0)
    {
      //handle new order
      BuildAndSendNewOrder(connection, token[Idx], symbol[Idx], FIX_SIDE_SELL, 10000, 12.34, 
          (Idx==0 || Idx==1) ? FIX_TIMEINFORCE_DAY : FIX_TIMEINFORCE_IOC, '1', 'A',  "taithien05");
      continue;
    }
    
    if (strcmp(para->parameterList[i], "cancel") == 0)
    {
      //handle cancel order
      BuildAndSendCancelRequest(connection, (Idx==0) ? token[0] : token[1], "cancel_token",
          (Idx==0) ? symbol[0] : symbol[1], FIX_SIDE_SELL, 10000);
      continue;
    }
  }
  
  return 0;
}

static int ParseCommandLine(char* cmdLine, t_Parameters* para)
{
  Lrc_Split(cmdLine, ' ', para);
  
  return para->countParameters;  
}

static int GetCommandLine(t_Parameters* para)
{
  char cmdLine[128];
  
  fgets(cmdLine, 128, stdin);
  // if (cmdLine == NULL)
  // {
    // return 0;
  // }

  return ParseCommandLine(cmdLine, para);
}

void* StartCHXTestThread (void* data)
{
  t_Parameters para;
  t_Connection* connection = (t_Connection*) data;
  
  while (1)
  {
    TraceLog(DEBUG_LEVEL, "Get command line >>\n");
    GetCommandLine(&para);
    if (HandleCommandLine(connection, &para) == 1)
      break;
    usleep(1000);
  }  

  return NULL;
}

void CHXForTesting ()
{
  struct sockaddr_in server_addr;
  
  int configIdx = 0;
  for (configIdx = 0; configIdx < Configs.countPort; configIdx++)
    if (Configs.portList[configIdx].venue == VENUE_CHX)
      break;

  server_addr.sin_family = AF_INET;
  server_addr.sin_port = htons(Configs.portList[configIdx].port);
  if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) != 1)
  {
    TraceLog(ERROR_LEVEL, "Cannot convert net address\n");
    return;
  }
  
  while (!testExit)
  {
    int testSocket = socket(AF_INET, SOCK_STREAM, 0);
    if (testSocket == -1)
    {
      TraceLog(ERROR_LEVEL, "Cannot create test socket\n");
      return;
    }  

    if(connect(testSocket, (struct sockaddr*) &server_addr, sizeof(server_addr)) < 0)
    {
      TraceLog(ERROR_LEVEL, "Cannot connect to server\n");
      return;
    }
  
    t_UserInfo userInfo;
    t_Connection connection;
  
    strcpy(userInfo.userName, "TEST");
    userInfo.outgoingSeq = 0;
    
    connection.configInfo = &Configs.portList[configIdx];
    connection.userInfo = &userInfo;
    connection.clientIndex = 1;
    connection.isAlive = 1;
    connection.socket = testSocket;
    connection.executionNumber = 0;
    connection.orderID = 1;
    InitOrderTokenQueue(&connection.orderQueue);
    pthread_spin_init(&connection.lock, 0);
    
    pthread_t testThread;
    pthread_create(&testThread, NULL, StartCHXTestThread, &connection);
    
    pthread_join(testThread, NULL);
    
    close(testSocket);
  }
}

