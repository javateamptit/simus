#include "nasdaq_rash.h"


int RASH_GetNewOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  if (msg == NULL || msgLen != RASH_ENTER_ORDER_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "RASH: NewOrder message is null or not match length\n");
    return ERROR;
  }
  
  // Order Token
  memcpy(orderEntry->clOrdID, &msg[1], 14);
  // Buy/Sell Indicator
  orderEntry->side = msg[15];
  // Shares
  orderEntry->quantity = Lrc_atoi(&msg[16], 6);
  // Stock Symbol
  memcpy(orderEntry->symbol, &msg[22], 8);
  // The price of the order
  orderEntry->price = Lrc_atoi(&msg[30], 10);
  // Time In Force
  orderEntry->tif = Lrc_atoi(&msg[40], 5);
  // Firm (Client ID)
  memcpy(orderEntry->clientId, &msg[45], 4);
  // Display
  orderEntry->displayIndicator =  msg[49];
  // Min Qty
  orderEntry->minQty = Lrc_atoi(&msg[50], 6);
  // Max Floor
  orderEntry->minQty = Lrc_atoi(&msg[56], 6);
  
  // Peg Type
  orderEntry->pegType = msg[62];
  // Peg Difference Sign
  char diffSign = msg[63];
  // Peg Difference
  orderEntry->pegDifference = Lrc_atoi(&msg[64], 10);
  /// Validation Peg Difference
  if (orderEntry->pegType == 'N' && (diffSign != '+' || orderEntry->pegDifference != 0))
  {
    TraceLog(ERROR_LEVEL, "RASH: NewOrder invalid Peg Difference specify\n");
    return ERROR;
  }
  if (diffSign == '-')
    orderEntry->pegDifference = -orderEntry->pegDifference;

  // Discretion price
  orderEntry->discretionPrice = Lrc_atoi(&msg[74], 10);
  // Discretion Peg Type
  orderEntry->discretionPegType = msg[84];
  // Discretion Peg Difference Sign
  diffSign = msg[85];
  // Discretion Peg Difference
  orderEntry->discretionPegDiff = Lrc_atoi(&msg[86], 10);
  /// Validation Discretion Peg Difference
  if (orderEntry->discretionPegType == 'N' && (diffSign != '+' || orderEntry->discretionPegDiff != 0))
  {
    TraceLog(ERROR_LEVEL, "RASH: NewOrder invalid Discretion Peg Difference specify\n");
    return ERROR;
  }
  if (diffSign == '-')
    orderEntry->discretionPegDiff = -orderEntry->discretionPegDiff;

  // Capacity
  orderEntry->capacity = msg[96];
  // Random Reserve 
  orderEntry->randomReserve = Lrc_atoi(&msg[97], 6);
  // Route Dest/Exec Broker
  memcpy(orderEntry->RoutingInst, &msg[103], 4);
  // Cust/Terminal ID/Sender SubID 
  memcpy(orderEntry->senderSubID, &msg[107], 32);
  // Customer Type
  orderEntry->customerType =  msg[139];

  return SUCCESS;
}

int RASH_GetCancelOrderEntry(t_OrderEntry* orderEntry, const char* msg, const int msgLen)
{
  if (msg == NULL || msgLen != RASH_CANCEL_MSG_LEN)
  {
    TraceLog(ERROR_LEVEL, "OUCH: CancelOrder message is null or not match length\n");
    return ERROR;
  }
  
  // Order Token
  memcpy(orderEntry->clOrdID, &msg[1], 14);
  // Shares
  orderEntry->quantity = Lrc_atoi(&msg[15], 6);
  
  return SUCCESS;
}

int RASH_BuildAndSend_MsgReject(t_Connection* connection, t_OrderEntry* orderEntry, const char reason)
{
  char message[RASH_REJECTED_MSG_LEN];

  // Timestamp
  __lToStrWithLeftPad(GetLocalTime_msec(), '0', 8, &message[0]);
  // Message Type "J"
  message[8] = RASH_REJECTED_MSG_TYPE;
  // The order Token field as entered.
  memcpy(&message[9], orderEntry->clOrdID, 14);
  // Reject reason
  message[23] = reason;
  
  return RASH_BuildAndSend_SequencedDataPacket(connection, message, RASH_REJECTED_MSG_LEN);
}

int RASH_BuildAndSend_MsgAccepted(t_Connection* connection, t_OrderEntry* orderEntry)
{
  char message[RASH_ACCEPTED_MSG_LEN];

  // Timestamp
  __lToStrWithLeftPad(GetLocalTime_msec(), '0', 8, &message[0]);
  // Message Type "A" Accepted message
  message[8] = RASH_ACCEPTED_MSG_TYPE;
  // The order Token field as entered.
  memcpy(&message[9], orderEntry->clOrdID, 14);
  // Buy/Sell Indicator as entered.
  message[23] = orderEntry->side;
  // Total number of shares accepted.
  __iToStrWithLeftPad(orderEntry->quantity, '0', 6, &message[24]);
  // Stock Symbol as entered.
  memcpy(&message[30], orderEntry->symbol, 8);
  // Price
  __lToStrWithLeftPad(orderEntry->price, '0', 10, &message[38]);
  // Time In Force
  __iToStrWithLeftPad(orderEntry->tif, '0', 5, &message[48]);
  // Firm (Account)
  memcpy(&message[53], orderEntry->clientId, 4);
  // Display
  message[57] = orderEntry->displayIndicator;
  // Order Reference Number
  orderEntry->orderID = __sync_add_and_fetch(&connection->orderID, 1);
  __lToStrWithLeftPad(orderEntry->orderID, '0', 9, &message[58]);
  // Minimum number of shares to execute on the replacement
  __iToStrWithLeftPad(orderEntry->minQty, '0', 6, &message[67]);
  // Max Floor
  __iToStrWithLeftPad(orderEntry->maxFloor, '0', 6, &message[73]);
  // Peg Type
  message[79] = orderEntry->pegType;
  // Peg Difference Sign & Peg Difference
  if (orderEntry->pegDifference >= 0)
  {
    message[80] = '+';
    __lToStrWithLeftPad(orderEntry->pegDifference, '0', 10, &message[81]);
  }
  else
  {
    message[80] = '-';
    __lToStrWithLeftPad(-orderEntry->pegDifference, '0', 10, &message[81]);
  }
  // Discretion Price 
  __lToStrWithLeftPad(orderEntry->discretionPrice, '0', 10, &message[91]);
  // Discretion Peg Type
  message[101] = orderEntry->discretionPegType;
  // Discretion Peg Difference Sign & Peg Difference
  if (orderEntry->discretionPegDiff >= 0)
  {
    message[102] = '+';
    __lToStrWithLeftPad(orderEntry->discretionPegDiff, '0', 10, &message[103]);
  }
  else
  {
    message[102] = '-';
    __lToStrWithLeftPad(-orderEntry->discretionPegDiff, '0', 10, &message[103]);
  }
  // The capacity specified on the order
  message[113] = orderEntry->capacity;
  // Random Reserve
  __iToStrWithLeftPad(orderEntry->randomReserve, '0', 6, &message[114]);
  // Route Dest/Exec Broker
  memcpy(&message[120], orderEntry->RoutingInst, 4);
  // Cust/Terminal ID/Sender SubID 
  memcpy(&message[124], orderEntry->senderSubID, 32);
  // Customer Type
  message[156] = orderEntry->customerType;

  return RASH_BuildAndSend_SequencedDataPacket(connection, message, RASH_ACCEPTED_MSG_LEN);
}

int RASH_BuildAndSend_MsgExecuted(t_Connection* connection, t_QueueItem* item,
      const int quantity, const long price, const char liqidFlag, const long matchNum)
{
  // Update executed shares and total price
  item->cumShare += quantity;
  item->totalPx += quantity * price;
  
  char message[RASH_EXECUTED_MSG_LEN];

  // Timestamp
  __lToStrWithLeftPad(GetLocalTime_msec(), '0', 8, &message[0]);
  // Message Type "E"
  message[8] = RASH_EXECUTED_MSG_TYPE;
  // The order Token field as entered.
  memcpy(&message[9], item->orderEntry.clOrdID, 14);
  // The number of shares executed 
  __iToStrWithLeftPad(quantity, '0', 6, &message[23]);
  // The price at which these shares were executed
  __lToStrWithLeftPad(price, '0', 10, &message[29]);
  // Liquidity Flag
  message[39] = liqidFlag;
  // Match Number
  __lToStrWithLeftPad(matchNum, '0', 9, &message[40]);

  return RASH_BuildAndSend_SequencedDataPacket(connection, message, RASH_EXECUTED_MSG_LEN);
}

int RASH_BuildAndSend_MsgCancelled(t_Connection* connection, t_QueueItem* item, const int quantity, const char reason)
{
  // Update new quantity of stock
  item->orderEntry.quantity -= quantity;
  
  char message[RASH_CANCELLED_MSG_LEN];

  // Timestamp
  __lToStrWithLeftPad(GetLocalTime_msec(), '0', 8, &message[0]);
  // Message Type "C"
  message[8] = RASH_CANCELLED_MSG_TYPE;
  // The order Token field as entered.
  memcpy(&message[9], item->orderEntry.clOrdID, 14);
  // Shares
  __iToStrWithLeftPad(quantity, '0', 6, &message[23]);
  // Cancel reason
  message[29] = reason;

  return RASH_BuildAndSend_SequencedDataPacket(connection, message, RASH_CANCELLED_MSG_LEN);
}

int RASH_CheckNewOrderEntry(t_Connection* connection, t_OrderEntry *orderEntry)
{
  if(orderEntry->tif != RASH_TIF_IOC && orderEntry->tif != RASH_TIF_DAY)
  {
    TraceLog(DEBUG_LEVEL, "Reject by TIF\n");
    RASH_BuildAndSend_MsgReject(connection, orderEntry, 'O'); // Invalid time in force
    return ERROR;
  }
  
  if(orderEntry->side != RASH_SIDE_BUY && orderEntry->side != RASH_SIDE_SELL && orderEntry->side != RASH_SIDE_SELL_SHORT)
  {
    TraceLog(DEBUG_LEVEL, "Reject by Side\n");
    RASH_BuildAndSend_MsgReject(connection, orderEntry, 'O'); // Invalid Side
    return ERROR;
  }
  
  if((orderEntry->quantity < 1) || (orderEntry->quantity > 1000000))
  {
    TraceLog(DEBUG_LEVEL, "Reject by Quantity\n");
    RASH_BuildAndSend_MsgReject(connection, orderEntry, 'O'); // Order quantity missing/invalid
    return ERROR;
  }
  
  return SUCCESS;
}

int RASH_Callback_Filled (t_Connection* connection, t_QueueItem* item, const int quantity, const long price, const long matchNum)
{
  return RASH_BuildAndSend_MsgExecuted(connection, item, quantity, price, RASH_LIQUIDITY_FLAG_REMOVED, matchNum);
}

int RASH_Callback_SystemCancel (t_Connection* connection, t_QueueItem* item)
{
  int remain = item->orderEntry.quantity - item->cumShare;
  
  return RASH_BuildAndSend_MsgCancelled(connection, item, remain, RASH_CANCEL_REASON_SYSTEM);
}

int RASH_Callback_UserCancel (t_Connection* connection, t_QueueItem* item, const int quantity)
{
  return RASH_BuildAndSend_MsgCancelled(connection, item, quantity, RASH_CANCEL_REASON_USER);
}

int RASH_Callback_CancelReject (t_Connection* connection, t_OrderEntry *orderEntry)
{
  // NOT USE
  return SUCCESS;
}

static int RASH_Handle_MsgCancelOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  if (RASH_GetCancelOrderEntry(&orderEntry, msg, msgLen) == ERROR)
    return ERROR; // RASH_BuildAndSend_MsgCancelReject(connection, &orderEntry);
  
  return Simulator_Handle_CancelOrder(connection, &orderEntry, &RASH_Callback_UserCancel, &RASH_Callback_CancelReject);
}

static int RASH_Handle_MsgNewOrder(t_Connection* connection, const char* msg, const int msgLen)
{
  t_OrderEntry orderEntry;
  memset(&orderEntry, 0, sizeof(t_OrderEntry));
  
  if (RASH_GetNewOrderEntry(&orderEntry, msg, msgLen) == ERROR)
    return RASH_BuildAndSend_MsgReject(connection, &orderEntry, 'O');

  if (RASH_CheckNewOrderEntry(connection, &orderEntry) == SUCCESS)
  {
/*    TraceLog(DEBUG_LEVEL, "RASH New order: symbol(%.8s), clOrdID(%s), quantity(%d), price(%lf) side(%c) tif(%d)\n", 
                    orderEntry.symbol, orderEntry.clOrdID, orderEntry.quantity, 
                    orderEntry.price * 0.0001, orderEntry.side, orderEntry.tif);*/
  
    RASH_BuildAndSend_MsgAccepted(connection, &orderEntry);
    
    // Token
    t_QueueItem item;
    memcpy(&item.orderEntry, &orderEntry, sizeof(t_OrderEntry));
    item.priceScale = '4';
    item.cumShare = 0;
    item.totalPx = 0;

    // Detect cross
    int hasCross = Simulator_Handle_CrossOrder(connection, &item, RASH_SIDE_BUY, &RASH_Callback_Filled);
    
    if (orderEntry.tif == RASH_TIF_IOC)
      if (hasCross)
        RASH_BuildAndSend_MsgCancelled(connection, &item, item.orderEntry.quantity - item.cumShare, RASH_CANCEL_REASON_IOC);
      else //IOC Random
        Simulator_Handle_RandomIOC(connection, &item, &RASH_Callback_Filled, &RASH_Callback_SystemCancel);
    else // DAY
      Simulator_Add_DAYOrder(&connection->orderQueue, &item);
  }

  return SUCCESS;
}

int RASH_Handle_UnsequencedDataPacket(t_Connection* connection, t_DataBlock *dataBlock)
{
  char* msg = &dataBlock->msgContent[RASH_PAYLOAD_OFFSET];
  int msgLen = dataBlock->msgLen - RASH_ADDITIONAL_DATA_PACKET_LEN;

  switch(msg[0])
  {
    case RASH_ENTER_ORDER_MSG_TYPE:
      return RASH_Handle_MsgNewOrder(connection, msg, msgLen);
      break;
    
    case RASH_CANCEL_MSG_TYPE:
      return RASH_Handle_MsgCancelOrder(connection, msg, msgLen);
      break;
      
    default:
      TraceLog(DEBUG_LEVEL, "Unknown message type '%c'\n", msg[0]);
      break;
  }
  
  return SUCCESS;
}

static void* RASH_DAYThread(void* arg)
{
  t_Connection *connection = (t_Connection *)arg;

  while(connection->isAlive)
  {    
    Simulator_Handle_RandomDAY(connection, &RASH_Callback_Filled, &RASH_Callback_SystemCancel);
    
    sleep(15);
  }
  
  return NULL;
}

int HandleMsg_RASH(t_Connection* connection, t_DataBlock *dataBlock)
{
  switch(dataBlock->msgContent[0])
  {
    case RASH_LOGIN_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received login packet from client\n");
      if(RASH_Handle_LogonPacket(connection, dataBlock) == SUCCESS)
      {
        pthread_t tDAY;
        pthread_create(&tDAY, NULL, RASH_DAYThread, (void*)connection);
        
        pthread_t tHeartbeat;
        pthread_create(&tHeartbeat, NULL, RASH_HeartbeatThread, (void*)connection);
      }
      break;
    
    case RASH_CLIENT_HEARTBEAT_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received heartbeat packet from client\n");
      break;
    
    case RASH_LOGOUT_REQUEST_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received logout packet from client\n");
      RASH_Handle_LogoutPacket(connection, dataBlock);
      break;
    
    case RASH_UNSEQUENCED_DATA_PACKET_TYPE:
      TraceLog(DEBUG_LEVEL, "Received unsequenced data packet from client\n");
      RASH_Handle_UnsequencedDataPacket(connection, dataBlock);
      break;
      
    default:
      TraceLog(ERROR_LEVEL, "Unknown packet type '%c'\n", dataBlock->msgContent[2]);
      break;
  }
  return SUCCESS;
}

